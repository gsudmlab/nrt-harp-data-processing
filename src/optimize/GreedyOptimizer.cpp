/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * @file GreedyOptimizer.cpp
 *
 * @author Dustin Kempton, GSU DMLab
 * Contact: dkempton1@gsu.edu
 *
 * Created on March 22, 2021
 */

#ifndef MCT_GREEDY_OPTIMIZER_CPP
#define MCT_GREEDY_OPTIMIZER_CPP

#include <tuple>
#include <queue>
#include <vector>
#include <cmath>
#include <limits>
#include <algorithm>

#include "../include/optimize/IOptimizer.cpp"

class GreedyOptimizer : public IOptimizer {

private:
    struct DistContainer {
        double distance;
        int positiveIdx;
        int negativeIdx;
    };

    class DistComparison {
    public:
        bool operator()(const DistContainer *lhs, const DistContainer *rhs) {
            return (lhs->distance > rhs->distance);
        }
    };

    class ReverseDistComparison{
    public:
        bool operator()(const DistContainer *lhs, const DistContainer *rhs) {
            return (lhs->distance < rhs->distance);
        }
    };

    typedef std::priority_queue<DistContainer *, std::vector < DistContainer * >, DistComparison>
    dist_queue_type;

    typedef std::priority_queue<DistContainer *, std::vector < DistContainer * >, ReverseDistComparison>
    rev_dist_queue_type;

public:

    GreedyOptimizer() {

    }

    PathMap *process(std::vector<std::vector<int>*> *positiveFluxPartitions,
                     std::vector<std::vector<int>*> *negativeFluxPartitions, int ncols, int nrows ) {

        //Todo:  The algorithm needs to be reconfigured to limit memory usage by limiting the points in the queue at any point in time
        //Get the minimum bounding rectangles for each of the positive and negative partitions.
        std::vector < PartMBR * > *positivePartitionMBRs = this->getMBRs(positiveFluxPartitions, ncols);
        std::vector < PartMBR * > *negativePartitionMBRs = this->getMBRs(negativeFluxPartitions, ncols);

        /*
         * Construct a queue that contains the minimum distance from each positive cluster to each negative cluster.
         * This will be used to know when we need to start calculating distances between points in two clusters. We will
         * only process all of the point distances between points in two clusters once the distance we are currently
         * working on meets the minimum distance between two said clusters.
         */
        dist_queue_type * minClusterDistanceQueue = this->getMBRMinDistanceQueue(positivePartitionMBRs,
                                                                                 negativePartitionMBRs);

        for (std::vector<PartMBR *>::iterator it = positivePartitionMBRs->begin();
             it != positivePartitionMBRs->end(); it++) {
            PartMBR *mbr = (*it);
            delete mbr;
        }
        delete positivePartitionMBRs;

        for (std::vector<PartMBR *>::iterator it = negativePartitionMBRs->begin();
             it != negativePartitionMBRs->end(); it++) {
            PartMBR *mbr = (*it);
            delete mbr;
        }
        delete negativePartitionMBRs;

        /*
         * Get the information about the positive and negative clusters that have the smallest minimum distance between
         * them. This will be the first set of partitions we will map points between.
         */
        DistContainer *currentMinCluster = minClusterDistanceQueue->top();
        minClusterDistanceQueue->pop();


        /*
         * Construct a number of arrays that are used to keep track of the current state.
         * 1.) inuseMap indicates which points have already been used  to map flux between two clusters.
         * 2.) positivePartsInactive is used to indicate when all the points in a positive partition have been mapped
         * to negative partition points, that way we can ignore processing it any further.
         * 3.) negativePartsInactive is used to indicate when all the points in a negative partition have been mapped
         * to positive partition points, that way we can ignore processing it any further.
         */
        int *inUseMap = new int[ncols * nrows]();
        int *positivePartsInactive = new int[positiveFluxPartitions->size()]();
        int *negativePartsInactive = new int[negativeFluxPartitions->size()]();

        /*
         * Populate the priority queue used to process mapping the closest/lowest cost pixel mappings first.
         */
        dist_queue_type * pixelDistanceQueue = new dist_queue_type();
        std::vector<int> *currentPositivePartition = positiveFluxPartitions->at(currentMinCluster->positiveIdx);
        std::vector<int> *currentNegativePartition = negativeFluxPartitions->at(currentMinCluster->negativeIdx);
        this->calculatePartitionPointDistances(pixelDistanceQueue, currentPositivePartition,
                                               currentNegativePartition, inUseMap, ncols);
        delete currentMinCluster;

        /*
         * Set the distance between the next closest pair of positive and negative partitions. This is used to know
         * when we need to add points between additional partitions to the priority queue used to process mapping the
         * closest/lowest cost pixel mappings. If the next pixel mapping in the queue is greater than the distance
         * between the next pair of partitions, then we need to add the points between these partitions to the queue for
         * consideration.
         *
         * If there are no partition pairs remaining, then the distance will be set to the maximum possible double value.
         */
        double distToNextPart = std::numeric_limits<double>::max();
        if (!minClusterDistanceQueue->empty()) {
            DistContainer *nextMinCluster = minClusterDistanceQueue->top();
            distToNextPart = nextMinCluster->distance;
        }



        /*
         * Process until we have no more to process.
         */
        std::vector <std::tuple<int, int>> *mappings = new std::vector <std::tuple<int, int>>();
        while (!pixelDistanceQueue->empty() || !minClusterDistanceQueue->empty()) {
            /*
             * If the pixel queue has points in it, we can proceed to process them.
             */
            if (!pixelDistanceQueue->empty()) {
                /*
                 * First check if we need to add the points from the next partition pair to the processing queue. This
                 * can occur in two cases:
                 * 1.) The distance to the next closest pixel in the queue is larger than the distance to the next
                 * closest partition pair. In which case, we need to add those points to the queue before we proceed.
                 * 2.) The pixel queue is empty, in which case, we need to add the points in next closest partition pair to
                 * the queue for processing.
                 */
                DistContainer *currentPixDistCont = pixelDistanceQueue->top();
                double nextPixDistance = currentPixDistCont->distance;
                if (nextPixDistance > distToNextPart) {
                    /*
                     * If yes, then we make sure that both of the partitions are still active and add
                     * the distances between the points in each partition to the queue if they are.
                     */
                    if (!minClusterDistanceQueue->empty()) {
                        DistContainer *currentMinCluster = minClusterDistanceQueue->top();
                        minClusterDistanceQueue->pop();
                        int posIdx = currentMinCluster->positiveIdx;
                        int negIdx = currentMinCluster->negativeIdx;
                        delete currentMinCluster;
                        if (this->checkPartitionsActive(positiveFluxPartitions, negativeFluxPartitions,
                                                        positivePartsInactive, negativePartsInactive, inUseMap, posIdx,
                                                        negIdx)) {
                            std::vector<int> *currentPositivePartition = positiveFluxPartitions->at(posIdx);
                            std::vector<int> *currentNegativePartition = negativeFluxPartitions->at(negIdx);
                            this->calculatePartitionPointDistances(pixelDistanceQueue, currentPositivePartition,
                                                                   currentNegativePartition, inUseMap, ncols);
                        }
                    }

                    /*
                     * Now we update the distance to the next closest/lowest cost cluster pair.
                     */
                    if (!minClusterDistanceQueue->empty()) {
                        DistContainer *nextMinCluster = minClusterDistanceQueue->top();
                        distToNextPart = nextMinCluster->distance;
                    } else {
                        //If there were no more pairs, then we set the next pair distance to maximum.
                        distToNextPart = std::numeric_limits<double>::max();
                    }
                    //Since we updated the queue, we should start the loop over.
                    continue;
                }
                //Only reach here if we don't update the pixel distance queue
                pixelDistanceQueue->pop();
                int posIdx = currentPixDistCont->positiveIdx;
                int negIdx = currentPixDistCont->negativeIdx;
                delete currentPixDistCont;

                /*
                 * Now that we have our pixel information, let's check if the mapping is open. If it is, then we add it
                 * to the result mappings vector, and update the inUseMap to indicate that these two are no longer
                 * available to be used in mapping.
                 */
                if (inUseMap[posIdx] == 0 && inUseMap[negIdx] == 0) {
                    std::tuple<int, int> map = std::make_tuple(posIdx, negIdx);
                    mappings->push_back(map);
                    inUseMap[posIdx] = 1;
                    inUseMap[negIdx] = 1;
                }
            } else {
                /*
                 * If the pixel queue is empty, then we make sure that both of the next partitions are still active and
                 * add the distances between the points in each partition to the queue if they are.
                 */
                if (!minClusterDistanceQueue->empty()) {
                    DistContainer *currentMinCluster = minClusterDistanceQueue->top();
                    minClusterDistanceQueue->pop();
                    int posIdx = currentMinCluster->positiveIdx;
                    int negIdx = currentMinCluster->negativeIdx;
                    delete currentMinCluster;
                    if (this->checkPartitionsActive(positiveFluxPartitions, negativeFluxPartitions,
                                                    positivePartsInactive, negativePartsInactive, inUseMap, posIdx,
                                                    negIdx)) {
                        std::vector<int> *currentPositivePartition = positiveFluxPartitions->at(posIdx);
                        std::vector<int> *currentNegativePartition = negativeFluxPartitions->at(negIdx);
                        this->calculatePartitionPointDistances(pixelDistanceQueue, currentPositivePartition,
                                                               currentNegativePartition, inUseMap, ncols);
                    }
                }

                /*
                 * Now we update the distance to the next closest/lowest cost cluster pair.
                 */
                if (!minClusterDistanceQueue->empty()) {
                    DistContainer *nextMinCluster = minClusterDistanceQueue->top();
                    distToNextPart = nextMinCluster->distance;
                } else {
                    //If there were no more pairs, then we set the next pair distance to maximum.
                    distToNextPart = std::numeric_limits<double>::max();
                }
            }
        }

        delete pixelDistanceQueue;
        delete minClusterDistanceQueue;

        delete[] inUseMap;
        delete[] positivePartsInactive;
        delete[] negativePartsInactive;

        PathMap *result = new PathMap();
        result->mappings = mappings;
        return result;
    }

private:

    struct PartMBR {
        int X[2] = {0, 0};
        int Y[2] = {0, 0};
    };

    /**
     * Method that checks if the positive and negative partitions both still have unmapped pixels. If one or both
     * partitions are found to not have any unused pixels, the inactive marker arrays are updated to to indicate
     * which partition is now considered inactive for further consideration.
     *
     * @param positiveFluxPartitions A list of lists, where the sub-lists contain indexes of the pixels in the partition
     * that the sub-list represents.
     *
     * @param negativeFluxPartitions A list of lists, where the sub-lists contain indexes of the pixels in the partition
     * that the sub-list represents.
     *
     * @param positivePartsInactive An array of the same length as the positive flux partition list, where the element
     * is 1 when that corresponding partition is not to be considered for use any longer, and 0 otherwise. The method
     * will edit the array if the partition is found to be inactive.
     *
     * @param negativePartsInactive An array of the same lenght as the negative flux partition list, where the element
     * is 1 when that corresponding partition is not to be considered for use any longer, and 0 otherwise. The method
     * will edit the array if the partition is found to be inactive.
     *
     * @param inUseMap A map of pixels that have already been used. The element is 1 if the pixel is used and 0 otherwise
     *
     * @param posIdx The index of the positive partition to consider for this call.
     *
     * @param negIdx The index of the negative partition to consider for this call.
     *
     * @return True if both partitions were found to be active, False otherwise.
     */
    bool checkPartitionsActive(std::vector<std::vector<int>*>* positiveFluxPartitions,
                               std::vector<std::vector<int>*>* negativeFluxPartitions, int* positivePartsInactive,
                               int* negativePartsInactive, int* inUseMap, int posIdx, int negIdx) {
        bool posActive = false;
        bool negActive = false;

        //If the positive partition is not marked as inactive, then check the points.
        if (positivePartsInactive[posIdx] == 0) {
            std::vector<int> *positivePartition = positiveFluxPartitions->at(posIdx);
            for (std::vector<int>::iterator it = positivePartition->begin(); it != positivePartition->end(); it++) {
                int idx = (*it);
                //If we find one pixel that is not already used by a mapping, then it is active and we can stop.
                if (inUseMap[idx] == 0) {
                    posActive = true;
                    break;
                }
            }
            //If no pixels were found unused, then mark this partition inactive so we don't process it anymore.
            if (!posActive) {
                positivePartsInactive[posIdx] = 1;
            }
        }

        //If the negative partition is not marked as inactive, then check the points.
        if (negativePartsInactive[negIdx] == 0) {
            std::vector<int> *negativePartition = negativeFluxPartitions->at(negIdx);
            for (std::vector<int>::iterator it = negativePartition->begin(); it != negativePartition->end(); it++) {
                int idx = (*it);
                //If we find one pixel that is not already used by a mapping, then it is active and we can stop.
                if (inUseMap[idx] == 0) {
                    negActive = true;
                    break;
                }
            }
            //If no pixels were found unused, then mark this partition as inactive so we don't process it anymore.
            if (!negActive) {
                negativePartsInactive[negIdx] = 1;
            }
        }

        return posActive && negActive;
    }

    /**
     * Method calculates the distance from each point in the positive partition to each point in the negative partition.
     * The distances and the indexes of the points the distance represents are placed into the input priority queue.
     *
     * @param pixelDistanceQueue The priority queue used to store the distances and point information in some sort of
     * ordering.
     *
     * @param positiveFluxPartition The positive flux partition to calculate point distances from.
     *
     * @param negativeFluxPartition The negative flux partition to calculate point distances to.
     *
     * @param inUseMap A map of pixels that have already been used. The element is 1 if the pixel is used and 0 otherwise
     *
     * @param ncols The number of columns in the original input image, used to convert indexes to Euclidean coordinates
     */
    void calculatePartitionPointDistances(dist_queue_type *pixelDistanceQueue, std::vector<int> *positiveFluxPartition,
                                          std::vector<int> *negativeFluxPartition, int *inUseMap, int ncols) {

        for (std::vector<int>::iterator it = positiveFluxPartition->begin(); it != positiveFluxPartition->end(); it++) {
            int posIdx = (*it);
            if (inUseMap[posIdx] == 0) {
                int posX = posIdx % ncols;
                int posY = posIdx / ncols;
                for (std::vector<int>::iterator it2 = negativeFluxPartition->begin();
                    it2 != negativeFluxPartition->end(); it2++) {

                    int negIdx = (*it2);
                    if (inUseMap[negIdx] == 0) {
                        int negX = negIdx % ncols;
                        int negY = negIdx / ncols;
                        double distance = this->getDist(posX, negX, posY, negY);
                        DistContainer *container = new DistContainer();
                        container->distance = distance;
                        container->positiveIdx = posIdx;
                        container->negativeIdx = negIdx;
                        pixelDistanceQueue->push(container);
                    }
                }
            }

        }

    }


    /**
     * Method construct a queue that contains the minimum distance from each positive cluster to each negative cluster.
     *
     * @param positivePartitionMBRs A list of minimum bounding rectangles for the positive flux clusters
     *
     * @param negativePartitionMBRs A list of minimum bounding rectangles for the negative flux clusters
     *
     * @return A priority queue that contains the minimum distance from each positive partition to each negative.
     */
    dist_queue_type *getMBRMinDistanceQueue(std::vector<PartMBR *> *positivePartitionMBRs,
                                            std::vector<PartMBR *> *negativePartitionMBRs) {

        dist_queue_type * minClusterDistanceQueue = new dist_queue_type();
        for (int i = 0; i < int(positivePartitionMBRs->size()); i++) {
            PartMBR *posMBR = positivePartitionMBRs->at(i);
            for (int j = 0; j < int(negativePartitionMBRs->size()); j++) {
                PartMBR *negMBR = negativePartitionMBRs->at(j);
                double minDist = this->getDist(posMBR->X[0], negMBR->X[0], posMBR->Y[0], negMBR->Y[0]);
                for (int px = 0; px < 2; px++) {
                    for (int py = 0; py < 2; py++) {
                        for (int nx = 0; nx < 2; nx++) {
                            for (int ny = 0; ny < 2; ny++) {
                                double dist = this->getDist(posMBR->X[px], negMBR->X[nx], posMBR->Y[py], negMBR->Y[ny]);
                                if (dist < minDist)minDist = dist;
                            }
                        }
                    }
                }
                DistContainer *distContainer = new DistContainer();
                distContainer->distance = minDist;
                distContainer->positiveIdx = i;
                distContainer->negativeIdx = j;
                minClusterDistanceQueue->push(distContainer);
            }
        }

        return minClusterDistanceQueue;
    }

    /**
     * Method calculates the Euclidean distance between two points.
     *
     * @param x1 The X position of the first point
     *
     * @param x2 The X position of the second point
     *
     * @param y1 The Y position of the first point
     *
     * @param y2 The Y position of the second point
     *
     * @return The Euclidean distance between the two input points
     */
    double getDist(int x1, int x2, int y1, int y2) {
        double dist = sqrt(pow((x1 - x2), 2.0) + pow((y1 - y2), 2.0));
        return dist;
    }

    /**
     * Method that constructs a list of minimum bounding rectangles for the input partitions.
     *
     * @param partitions List of lists where the sub-lists are index locations in the original input image.
     *
     * @param ncols The number of columns in the original input image.
     *
     * @return A list of minimum bounding rectangles for the input partitions
     */
    std::vector<PartMBR *> *getMBRs(std::vector<std::vector < int> * > *partitions,
                                    int ncols ) {

        std::vector < PartMBR * > *partitionMBRs = new std::vector<PartMBR *>();
        for (std::vector < std::vector < int > * > ::iterator it = partitions->begin();
                it != partitions->end(); it++) {

            std::vector<int> *part = (*it);
            int idx = part->front();
            int minX, maxX;
            minX = maxX = idx % ncols;
            int minY, maxY;
            minY = maxY = idx / ncols;
            for (std::vector<int>::iterator it2 = part->begin(); it2 != part->end(); it2++) {
                idx = (*it2);
                int x = idx % ncols;
                int y = idx / ncols;

                if (x < minX)minX = x;
                if (x > maxX)maxX = x;

                if (y < minY)minY = y;
                if (y > maxY)maxY = y;
            }
            PartMBR* mbr = new PartMBR();
            mbr->X[0] = maxX;
            mbr->X[1] = minX;
            mbr->Y[0] = maxY;
            mbr->Y[1] = minY;
            partitionMBRs->push_back(mbr);
        }
        return partitionMBRs;
    }

};

#endif