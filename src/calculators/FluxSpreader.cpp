/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * @file FluxSpreader.cpp
 *
 * @author Dustin Kempton, GSU DMLab
 * Contact: dkempton1@gsu.edu
 *
 * Created on March 21, 2021
 */
#ifndef FLUX_SPREADER_CPP
#define FLUX_SPREADER_CPP

#include <math.h>
#include <vector>
#include <cstdio>

#include "../include/calculators/IFluxSpreader.cpp"


/**
 * Class that spreads the flux over regions who's sizes and locations are based upon the flux weighted centroid of
 * the input partitions, the total uncanceled flux within each of the partitions. The area is a rectangle who's
 * size is calculated as the number of pixels needed to spread the total flux in units of the input min_flux variable.
 */
class FluxSpreader : public IFluxSpreader {

    double minFlux;
public:
    FluxSpreader(double min_flux) {
        this->minFlux = min_flux; //1e20 I believe is default.
    }


    FluxDistributions *process(std::vector<std::vector<int> *> *partitions, std::vector<int> *centroidIdxs,
                               std::vector<double> *uncanceledFluxValues, int ncols, int nrows) {

        //Normalize the flux and spread it in units of minFlx
        int nParts = partitions->size();
        double flx_norm[nParts];
        for (int i = 0; i < nParts; i++) {
            flx_norm[i] = round(uncanceledFluxValues->at(i) / this->minFlux);
        }

        std::vector<std::vector<int> *> *positiveFluxPartitions = new std::vector<std::vector<int> *>();
        std::vector<std::vector<int> *> *negativeFluxPartitions = new std::vector<std::vector<int> *>();
        int *indexToPartitionMap = new int[ncols * nrows]();

        for (int i = 0; i < nParts; i++) {
            int partNum = i + 1;
            int recSize = 0;
            //Size of the rectangle to be used for filling
            if (abs(flx_norm[i]) != 1.0) {
                recSize = ceil(0.5 * (-1.0 + sqrt(abs(flx_norm[i]))));
            }

            //If not big enough to hold the flux then expand by one
            if (pow(recSize, 2) < abs(flx_norm[i])) {
                recSize = recSize + 1;
            }


            int centroidIdx = centroidIdxs->at(i);
            int xc = centroidIdx % ncols;
            int yc = centroidIdx / ncols;

            //Verify we are not beyond edges of original image with the rectangle
            int xcMin = xc - recSize;
            int xcMax = xc + recSize;
            int ycMin = yc - recSize;
            int ycMax = yc + recSize;
            if (xcMin < 0) {
                xcMin = 0;
            }
            if (xcMax > ncols) {
                xcMax = ncols;
            }
            if (ycMin < 0) {
                ycMin = 0;
            }
            if (ycMax > nrows) {
                ycMax = nrows;
            }


            /*
             * This roughly spreads the flux in the target area over the rectangle calculated above.
             */
            int ip = 0;
            double absFluxNormVal = abs(flx_norm[i]);
            bool negFluxPart = signbit(flx_norm[i]);
            std::vector<int> *flxIdxs = new std::vector<int>();
            if (ip < absFluxNormVal && indexToPartitionMap[centroidIdx] == 0) {
                //Start with the centroid pixel
                ip++;
                flxIdxs->push_back(centroidIdx);
                indexToPartitionMap[centroidIdx] = partNum;
            }
            /*
             * Spread flux as concentric rings around the supplied centroid of the partition
             */
            for (int ring = 1; ring <= recSize; ring++) {
                for (int i = 0; i <= ring; i++) {

                    /*
                     * Calculate the col and row values for the current ring around the centroid
                     */
                    int colPlus = (xc + ring);
                    int colMinus = (xc - ring);
                    int rowPlus = (yc + ring);
                    int rowMinus = (yc - ring);
                    int ym = yc - i;
                    int yp = yc + i;
                    int xm = xc - i;
                    int xp = xc + i;

                    /*
                     * If col to the left is greater than or equal to the minimum location within the
                     * rectangle calculated for spreading flux, then proceed.
                     */
                    int idx;
                    if (colMinus >= xcMin) {
                        /*
                         * If the row above is greater than or equal to the minimum location within the
                         * rectangle calculated for spreading flux.
                         */
                        if (ym >= ycMin) {
                            idx = ym * ncols + colMinus;
                            if (ip < absFluxNormVal && indexToPartitionMap[idx] == 0) {
                                ip++;
                                flxIdxs->push_back(idx);
                                indexToPartitionMap[idx] = partNum;
                            }
                        }

                        /*
                         *  If the row below is greater than or equal to the maximum location within the
                         *  rectangle calculated for spreading flux.
                         */
                        if (yp < ycMax) {
                            idx = yp * ncols + colMinus;
                            if (ip < absFluxNormVal && indexToPartitionMap[idx] == 0) {
                                ip++;
                                flxIdxs->push_back(idx);
                                indexToPartitionMap[idx] = partNum;
                            }
                        }
                    }

                    /*
                     *  If col to the right is less than or equal to the maximum location within the
                     *  rectangle calculated for spreading flux, then proceed.
                     */
                    if (colPlus < xcMax) {
                        /*
                         * If the row above is greater than or equal to the minimum location within the
                         * rectangle calculated for spreading flux.
                         */
                        if (ym >= ycMin) {
                            idx = ym * ncols + colPlus;
                            if (ip < absFluxNormVal && indexToPartitionMap[idx] == 0) {
                                ip++;
                                flxIdxs->push_back(idx);
                                indexToPartitionMap[idx] = partNum;
                            }
                        }

                        /*
                         *  If the row below is greater than or equal to the maximum location within the
                         *  rectangle calculated for spreading flux.
                         */
                        if (yp < ycMax) {
                            idx = yp * ncols + colPlus;
                            if (ip < absFluxNormVal && indexToPartitionMap[idx] == 0) {
                                ip++;
                                flxIdxs->push_back(idx);
                                indexToPartitionMap[idx] = partNum;
                            }
                        }
                    }

                    /*
                     * If the row above is greater than or equal to the minimum location withing the
                     * rectangle calculated for spreading flux, then proceed.
                     */
                    if (rowMinus >= ycMin) {
                        /*
                         * If the column to the left is greater than or equal to the minimum location within the
                         * rectangle calculated for spreading flux.
                         */
                        if (xm >= xcMin) {
                            idx = rowMinus * ncols + xm;
                            if (ip < absFluxNormVal && indexToPartitionMap[idx] == 0) {
                                ip++;
                                flxIdxs->push_back(idx);
                                indexToPartitionMap[idx] = partNum;
                            }
                        }

                        /*
                         * If the column to the right is less than or equal to the maximum location within the
                         * rectangle calculated for spreading flux.
                         */
                        if (xp < xcMax) {
                            idx = rowMinus * ncols + xp;
                            if (ip < absFluxNormVal && indexToPartitionMap[idx] == 0) {
                                ip++;
                                flxIdxs->push_back(idx);
                                indexToPartitionMap[idx] = partNum;
                            }
                        }
                    }

                    /*
                     * If the row below is less than or equal to the maximum location within the
                     * rectangle calculated for spreading flux, then proceed.
                     */
                    if (rowPlus < ycMax) {
                        /*
                         * If the column to the left is greater than or equal to the minimum location within the
                         * rectangle calculated for spreading flux.
                         */
                        if (xm >= xcMin) {
                            idx = rowPlus * ncols + xm;
                            if (ip < absFluxNormVal && indexToPartitionMap[idx] == 0) {
                                ip++;
                                flxIdxs->push_back(idx);
                                indexToPartitionMap[idx] = partNum;
                            }
                        }

                        /*
                         * If the column to the right is less than or equal to the maximum location within the
                         * rectangle calculated for spreading flux.
                         */
                        if (xp < xcMax) {
                            idx = rowPlus * ncols + xp;
                            if (ip < absFluxNormVal && indexToPartitionMap[idx] == 0) {
                                ip++;
                                flxIdxs->push_back(idx);
                                indexToPartitionMap[idx] = partNum;
                            }
                        }
                    }
                }
            }

            if (!flxIdxs->empty()) {
                if (!negFluxPart) {
                    positiveFluxPartitions->push_back(flxIdxs);
                } else {
                    negativeFluxPartitions->push_back(flxIdxs);
                }
            } else {
                delete flxIdxs;
            }
        }

        FluxDistributions *distributions = new FluxDistributions();
        distributions->indexToPartitionMap = indexToPartitionMap;
        distributions->positiveFluxPartitions = positiveFluxPartitions;
        distributions->negativeFluxPartitions = negativeFluxPartitions;
        return distributions;
    }

};

#endif