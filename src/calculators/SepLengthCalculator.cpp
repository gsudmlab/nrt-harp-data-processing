/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * @file SepLengthCalculator.cpp
 *
 * @author Dustin Kempton, GSU DMLab
 * Contact: dkempton1@gsu.edu
 *
 * Created on July 28, 2020
 */

#ifndef SEP_LEN_CALCULATOR_CPP
#define SEP_LEN_CALCULATOR_CPP

#include <tuple>
#include <math.h>
#include <algorithm>
#include <unordered_map>

#include "../include/calculators/ISepLengthCalculator.cpp"
#include "../include/calculators/IFluxSpreader.cpp"
#include "../include/models/IModelCreator.cpp"
#include "../include/optimize/IOptimizer.cpp"

/**
 * This class finds an average flux-weighted separation length between positive and negative polarities using the Magnetic
 * Charge Topology model. The connectivity matrices are constructed using a some minimization technique to minimize
 * length and flux imbalance over the FOV.
 */
class SepLengthCalculator : public ISepLengthCalculator {

    IModelCreator *modelCreator;
    IFluxSpreader *fluxSpreader;
    IOptimizer *optimizer;

    double minFlux;

public:

    /**
     * The Constructor
     *
     * @param modelCreator Object that constructs the magnetic charge topology (MCT) model for an image input
     *
     * @param optimizer Object that finds the least cost mapping between the field values found by the MCT model
     *
     * @param fluxSpreader Object that spreads the field values found within a partitions around its flux weighted
     * centroid based on some criteria
     *
     * @param min_flux The minimum amount of flux that is considered a unit of flux to be mapped.
     */
    SepLengthCalculator(IModelCreator *modelCreator, IOptimizer *optimizer, IFluxSpreader *fluxSpreader,
                        double min_flux) {
        this->modelCreator = modelCreator;
        this->fluxSpreader = fluxSpreader;
        this->optimizer = optimizer;
        this->minFlux = min_flux; //1e20 I believe is default.
    }

    /**
     * From a vertical field distribution, finds an average flux-weighted
     * separation length between positive and negative polarities.
     *
     * @param bz Vertical field distribution (G)'
     *
     * @param ncols Size of bz in number of columns
     *
     * @param nrows Size of bz in number of rows
     *
     * @param cdelt1 Pixel size in arcsec
     */
    SepLength *findSepLength(double *bz, int ncols, int nrows, double cdelt1) {

        /*
         * We need to construct the magnetic charge topology (MCT) model before we can proceed.
         */
        MCTModel *mctModel = this->modelCreator->process(bz, ncols, nrows, cdelt1);
        int nElem = mctModel->NElem;
        std::vector<int> *centroidIdxs = mctModel->centroidIdxs;
        std::vector<std::vector<int>*>* partitions = mctModel->partitions;
        std::vector<double> *uncanceledFluxValues = mctModel->uncanceledFluxValues;
        delete mctModel;

        /*
         * If only one magnetic charge of a particular polarity is found there is no need to apply mapping.
         */
        if (nElem == 1) {
            /*
            * Done with the partitions, so delete them.
            */
            this->cleanupPartitionsVector(partitions);
            delete centroidIdxs;
            delete uncanceledFluxValues;


            SepLength *result = new SepLength();
            std::vector <std::tuple<int, int, double>> *mappings = new std::vector <std::tuple<int, int, double>>();
            result->mappings = mappings;
            result->errFlux = 0;
            result->meanFlux = 0;
            result->sepLen = 0;
            return result;
        }

        /*
         * If only two magnetic charges of opposite polarities are found there is no need to apply mapping.
         */
        if (nElem == 2 && uncanceledFluxValues->at(0) * uncanceledFluxValues->at(1) < 0.0) {
            SepLength *result = this->handleOnlyTwoFound(centroidIdxs, uncanceledFluxValues, ncols);

            /*
             * Done with the partitions, so delete them.
             */
            this->cleanupPartitionsVector(partitions);
            delete centroidIdxs;
            delete uncanceledFluxValues;
            return result;
        }

        /*
         * Split into regions of positive and negative flux centered around the centroid for each of the input
         * partitions. The area is determined by the total uncanceled flux within each of the partitions.
         */
        FluxDistributions *fluxDistributions = this->fluxSpreader->process(partitions, centroidIdxs,
                                                                           uncanceledFluxValues, ncols, nrows);

        std::vector <std::vector<int>*> *positiveFluxPartitions = fluxDistributions->positiveFluxPartitions;
        std::vector <std::vector<int>*> *negativeFluxPartitions = fluxDistributions->negativeFluxPartitions;
        int *indexToPartitionMap = fluxDistributions->indexToPartitionMap;
        delete fluxDistributions;

        /*
         * Done with the partitions, let's just delete them now.
         */
        this->cleanupPartitionsVector(partitions);

        /*
         *  If all positive or all negative clusters are found there is no need to apply mapping.
         */
        int nPFlux = positiveFluxPartitions->size();
        int nNFlux = negativeFluxPartitions->size();
        if (nPFlux == 0 || nNFlux == 0) {
            /*
             * We are now done with the positive and negative flux partitions, so delete them.
             */
            this->cleanupPartitionsVector(positiveFluxPartitions);
            this->cleanupPartitionsVector(negativeFluxPartitions);


            delete[] indexToPartitionMap;
            delete centroidIdxs;
            delete uncanceledFluxValues;

            SepLength *result = new SepLength();
            std::vector <std::tuple<int, int, double>> *mappings = new std::vector <std::tuple<int, int, double>>();
            result->mappings = mappings;
            result->errFlux = 0;
            result->meanFlux = 0;
            result->sepLen = 0;

            return result;
        }


        /*
         * Call the optimizer to find the least cost mapping between pixels in the positive and negative clusters.
         */
        PathMap *map = this->optimizer->process(positiveFluxPartitions, negativeFluxPartitions, ncols, nrows);
        std::vector <std::tuple<int, int>> *pixelMappings = map->mappings;
        delete map;

        /*
         * We are now done with the positive and negative flux partitions, so let's delete them.
         */
        this->cleanupPartitionsVector(positiveFluxPartitions);
        this->cleanupPartitionsVector(negativeFluxPartitions);



        /*
         * Fill the connectivity matrices:
         * First we define the connectivity matrices
         */
        //Matrix with minimum flux committed to a connection
        double *flx_mat = new double[nElem * nElem]();
        //Matrix with separation lengths
        double *len_mat = new double[nElem * nElem]();
        //Then fill them.
        std::vector <std::tuple<int, int, double>> *partition_flux_mappings = this->fillConnectivityMatricies(flx_mat,
                                                                                                              len_mat,
                                                                                                              pixelMappings,
                                                                                                              centroidIdxs,
                                                                                                              indexToPartitionMap,
                                                                                                              ncols);
        delete pixelMappings;
        delete[] indexToPartitionMap;
        //Done with the centroid idx list, so let's just delete it now.
        delete centroidIdxs;

        /*
         * Flux mappings are in multiples of minFlux after the flux spreader, so multiply each connection by
         * minFlux to get the actual flux value.
         */
        for (int i = 0; i < nElem * nElem; i++) {
            flx_mat[i] = flx_mat[i] * this->minFlux;
        }

        /*
         * Calculate the total available positive and negative flux in the system.
         */
        double pfl_tot = 0;
        double nfl_tot = 0;
        for(std::vector<double>::iterator it = uncanceledFluxValues->begin(); it!=uncanceledFluxValues->end(); it++){
            double val = (*it);
            bool negVal = signbit(val);
            if(negVal){
                nfl_tot += round(abs(val / this->minFlux)) * this->minFlux;
            }else{
                pfl_tot += round(val / this->minFlux) * this->minFlux;
            }
        }
        delete uncanceledFluxValues;


        /*
         * Calculate the total flux mapped and the total flux weighted by the distance between the mapped points.
         */
        double tot1 = 0;
        double tot2 = 0;
        for (int i = 0; i < nElem; i++) {
            for (int j = 0; j < nElem; j++) {
                int idx = i * nElem + j;
                tot1 += flx_mat[idx] * len_mat[idx];
                tot2 += flx_mat[idx];
            }
        }
        delete[] flx_mat;
        delete[] len_mat;


        double sep_len = tot1 / tot2;
        double mean_flux = 0.5 * (pfl_tot + nfl_tot);
        double err_flux = abs(mean_flux - pfl_tot);

        SepLength *result = new SepLength();
        result->sepLen = sep_len;
        result->errFlux = err_flux;
        result->meanFlux = mean_flux;
        result->mappings = partition_flux_mappings;

        return result;
    }

private:

    void cleanupPartitionsVector(std::vector<std::vector<int>*>* partitions){
        if(!partitions->empty()){
            for (std::vector<std::vector<int>*> ::iterator it = partitions->begin(); it != partitions->end(); it++) {
                std::vector<int> *part = (*it);
                delete part;
            }
            partitions->clear();
        }
        delete partitions;
    }


    /**
     * Fills the connectivity matrices flx_mat and len_mat.
     *
     * @param flx_mat A 2D array with nploc columns and nnloc rows that is updated with the sum of flux mapped between
     * each of the clusters indicated by that location in either ploc or nloc.
     *
     * @param len_mat A 2D array with nploc columns and nnloc rows that is updated with the distance between each
     * cluster that has flux mapped between them. The locations of which are indicated by either ploc or nloc.
     *
     * @param mappings A list of mappings from points in one list to points in another list. Each value in the
     * individual map entry represents the index within the positive and negative partition lists.  If item index 1
     * from the positive flux list is mapped to item index 2 in the negative flux list, then the map item is a tuple
     * of value (1,2).
     *
     * @param centroidIdxs A list of centroid indexes for each cluster found by the magnetic field topology model. The
     * index is the index in the original field map
     *
     * @param indexToPartitionMap A map that indicates which partition each of the positive and negative pixels map
     * back to, because their position may not overlap with the original position of the partition they are meant to
     * be a representation of.
     *
     * @param ncols the number of columns in the original field map
     *
     */
    std::vector <std::tuple<int, int, double>> *fillConnectivityMatricies(double *flx_mat, double *len_mat,
                                                                          std::vector <std::tuple<int, int>> *mappings,
                                                                          std::vector<int> *centroidIdxs,
                                                                          int *indexToPartitionMap, int ncols) {

        /**
         * Map where row is negative partition and col is positive partition, used to show how much flux flows from
         * a positive to negative partition.
         */
        int nElem = centroidIdxs->size();
        double *partMap = new double[nElem * nElem]();

        for (std::vector <std::tuple<int,int>>::iterator it = mappings->begin(); it != mappings->end();
        it++) {
            std::tuple<int, int> map = (*it);
            int posIdx = std::get<0>(map);
            int negIdx = std::get<1>(map);


            int posPartIdx = indexToPartitionMap[posIdx] - 1;
            int negPartIdx = indexToPartitionMap[negIdx] - 1;
            int idx = negPartIdx * nElem + posPartIdx;
            partMap[idx] += 1.0;

            int posCent = centroidIdxs->at(posPartIdx);
            int negCent = centroidIdxs->at(negPartIdx);

            int x1, y1, x2, y2 = 0;
            this->indToCart(posCent, x1, y1, ncols);
            this->indToCart(negCent, x2, y2, ncols);

            flx_mat[idx] += 1.0;
            len_mat[idx] += sqrt(pow((x2 - x1), 2.0) + pow((y2 - y1), 2.0));
        }

        std::vector <std::tuple<int, int, double>> *resultMapping = new std::vector <std::tuple<int, int, double>>();
        for (int negPart = 0; negPart < nElem; negPart++) {
            for (int posPart = 0; posPart < nElem; posPart++) {
                int idx = negPart * nElem + posPart;
                double val = partMap[idx] * this->minFlux;
                if (val > 0) {
                    int posCent = centroidIdxs->at(posPart);
                    int negCent = centroidIdxs->at(negPart);
                    std::tuple<int, int, double> tup = std::make_tuple(posCent, negCent, val);
                    resultMapping->push_back(tup);
                }
            }
        }
        delete[] partMap;

        return resultMapping;
    }

    /**
     * If only two magnetic charges of oposite polarities are found
     * there is no need to apply annealing.
     *
     * @param centroidIdxs A list of centroid indexes for each cluster found by the magnetic field topology model. The
     * index is the index in the original field map
     *
     * @param uncanceledFluxValues The uncanceled magnetic fluxes within each of the identified magnetic sources
     *
     * @param ncols The number of columns in the original field map
     *
     */
    SepLength *handleOnlyTwoFound(std::vector<int> *centroidIdxs, std::vector<double> *uncanceledFluxValues,
                                  int ncols) {
        int x1, y1, x2, y2 = 0;

        this->indToCart(centroidIdxs->at(0), x1, y1, ncols);
        this->indToCart(centroidIdxs->at(1), x2, y2, ncols);

        double sep_len = sqrt(pow((x2 - x1), 2.0) + pow((y2 - y1), 2.0));

        double abs_flux_tot = 0;
        int nElem = int(uncanceledFluxValues->size());
        for (int i = 0; i < nElem; i++) {
            abs_flux_tot += abs(uncanceledFluxValues->at(i));
        }

        double mean_flux = 0.5 * abs_flux_tot;
        double err_flux = 0;
        for (int i = 0; i < nElem; i++) {
            err_flux += abs(mean_flux - abs(uncanceledFluxValues->at(i)));
        }

        double flx = std::min(abs(uncanceledFluxValues->at(0)), abs(uncanceledFluxValues->at(1)));


        SepLength *result = new SepLength();
        result->sepLen = sep_len;
        result->meanFlux = mean_flux;
        result->errFlux = err_flux;
        result->mappings = new std::vector <std::tuple<int, int, double>>();
        std::tuple<int, int, double> tup = std::make_tuple(centroidIdxs->at(0), centroidIdxs->at(1), flx);
        result->mappings->push_back(tup);

        return result;
    }

    /**
     * Transforms the array index position into a Cartesian coordinate
     * position on a grid of specified linear size along x and y
     *
     * The array is expected to be in row major order.
     */
    void indToCart(int idx, int &xi, int &yi, int nCols) {
        yi = idx / nCols;
        xi = idx - (yi * nCols);
    }
};

#endif
