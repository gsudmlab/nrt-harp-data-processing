/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * @file IFluxSpreader.cpp
 *
 * @author Dustin Kempton, GSU DMLab
 * Contact: dkempton1@gsu.edu
 *
 * Created on March 21, 2021
 */

#ifndef IFLUX_SPREADER_CPP
#define IFLUX_SPREADER_CPP

#include <vector>

struct FluxDistributions {

    /**
     * The partitions with positive flux, processed to spread the flux over some area.
     */
    std::vector<std::vector<int> *> *positiveFluxPartitions;

    /**
     *  The partitions with negaive flux, processed to spread the flux over some area.
     */
    std::vector<std::vector<int> *> *negativeFluxPartitions;

    /**
     *  A map that indicates which partition each of the positive and negative pixels map back to, because their
     *  position may not overlap with the original position of the partition they are meant to be a representation of.
     */
    int *indexToPartitionMap;

};

/**
 * Interface for spreading flux over regions who's sizes and locations are based upon the centroid of
 * the input partitions, the total uncanceled flux within each of the partitions, and some other criteria set by
 * the implementation class.
 */
class IFluxSpreader {

public:
    virtual ~IFluxSpreader() {
    };

    /**
     * Method that takes in a list of partitions, a list of their flux weighted centroids, and a list of the
     * uncanceled flux within each partition. These are then processed to produce regions of positive and negative
     * flux based upon the centroid of the input partitions, the total uncanceled flux within each of
     * the partitions, and some other criteria set by the implementation class.
     *
     * @param partitions A list of lists, where each of the lists contain the index in the input image that belongs to
     * the partition that the list represents.
     *
     * @param centroidIdxs The index of the centroid locations' for each of identified magnetic sources
     *
     * @param uncanceledFluxValues The uncanceled magnetic fluxes within each of the identified magnetic sources
     *
     * @param ncols The number of columns in the input image
     *
     * @param nrows The number of rows in the input image
     */
    virtual FluxDistributions *process(std::vector<std::vector<int> *> *partitions, std::vector<int> *centroidIdxs,
                                       std::vector<double> *uncanceledFluxValues, int ncols, int nrows) = 0;
};

#endif
