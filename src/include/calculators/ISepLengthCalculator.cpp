/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * @file ISepLengthCalculator.cpp
 *
 * @author Dustin Kempton, GSU DMLab
 * Contact: dkempton1@gsu.edu
 *
 * Created on July 28, 2020
 */

#ifndef ISEP_LEN_CALCULATOR_CPP
#define ISEP_LEN_CALCULATOR_CPP

#include <vector>
#include <tuple>

struct SepLength {

    /**
     * @param sepLen The resulting flux-weighted separation length
     */
    double sepLen;

    /**
     * @param meanFlux The mean flux used for the calculation (Mx)'
     */
    double meanFlux;

    /**
     * @param errFlux The uncertainty of meanFlux (Mx)'
     */
    double errFlux;

    /**
     * @param mappings A list of tuples, where the first value of the tuple is the index location of the centroid of
     * a positive partition in the input image. The second value of the tuple is the index location of the centroid
     * of a negative partition in the input image. The third value of the tuple is the amount of flux mapped between
     * the two partitions. The tuple represents a pair of partitions that have some connection between them in the flux
     * mapping constructed by the implementing class.
     */
    std::vector<std::tuple<int, int, double>> *mappings;

};

/**
 * Interface for finding separation length between positive and
 * negative polarity clusters of magnetic field distributions.
 */
class ISepLengthCalculator {

public:
    virtual ~ISepLengthCalculator() {
    };

    /**
     * From a vertical field distribution, finds an average flux-weighted
     * separation length between positive and negative polarities.
     *
     * @param bz Vertical field distribution (G)'
     *
     * @param ncols Size of bz in number of columns
     *
     * @param nrows Size of bz in number of rows
     *
     * @param cdelt1 Pixel size in arcsec
     */
    virtual SepLength *findSepLength(double *bz, int ncols, int nrows, double cdelt1) = 0;
};

#endif
