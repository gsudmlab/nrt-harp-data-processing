/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * @file IModelCreator.cpp
 *
 * @author Dustin Kempton, GSU DMLab
 * Contact: dkempton1@gsu.edu
 *
 * Created on June 19, 2020
 */

#ifndef IMODELCREATOR_CPP
#define IMODELCREATOR_CPP

#include <vector>


struct MCTModel {
    /**
     * The number of magnetic elements found when constructing the model.
     * (Note: NElem will be 0 if no sources are found)
     */
    int NElem;

    /**
     * The uncanceled magnetic fluxes within each of the identified magnetic sources
     */
    std::vector<double> *uncanceledFluxValues;

    /**
     * The index of the centroid locations' for each of identified magnetic sources
     */
    std::vector<int> *centroidIdxs;

    /**
     * @param partitions A list of lists, where each of the lists contain the indexes in the input image that belongs to
     * the partition that the list represents.
     */
    std::vector<std::vector<int> *> *partitions;


    /**
     * @param nrows The number of rows in the input image that the partitions were extracted from.
     */
    int nrows;

    /**
     * @param ncols The number of columns in the input image that the partitions were extracted from.
     */
    int ncols;
};

/**
 * Interface for classes that calculate a magnetic charge topology (MCT) model
 * in a distribution of vertical field. The model returned provides the centroids,
 * radii, and magnetic fluxes of the identified boundary sources.
 */
class IModelCreator {

public:
    virtual ~IModelCreator() {
    };

    /**
     * Method that processes the input distribution of vertical field and returns the model.
     *
     *
     * @param bz The distribution of the vertical magnetic field (G)'
     *
     * @param ncols Size of bz in number of columns
     *
     * @param nrows Size of bz in number of rows
     *
     * @param cdelt1 Pixel size in arcsec
     */
    virtual MCTModel *process(double *bz, int ncols, int nrows, double cdelt1) = 0;

};

#endif
