/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * @file IImagePartitioner.cpp
 *
 * @author Dustin Kempton, GSU DMLab
 * Contact: dkempton1@gsu.edu
 *
 * Created on June 18, 2020
 */

#ifndef IIMAGE_PARTITIONER_CPP
#define IIMAGE_PARTITIONER_CPP

#include <vector>

struct PartitionMap {

    /**
     * @param partitions A list of lists, where each of the lists contain the indexes in the input image that belongs to
     * the partition that the list represents.
     */
    std::vector<std::vector<int> *> *partitions;

    /**
     * @param nrows The number of rows in the input image that the partitions were extracted from.
     */
    int nrows;

    /**
     * @param ncols The number of columns in the input image that the partitions were extracted from.
     */
    int ncols;

};

/**
 * Interface for image partitioning classes that identifies various topological features
 * in an image with a surface distribution of magnetic fields
 */
class IImagePartitioner {
public:
    virtual ~IImagePartitioner() {
    };

    /**
     * Method that takes an input image with a given surface distribution and
     * identifies various topological features within the image.
     *
     *
     * @param img The input image that pixel values are pulled from.
     *
     * @param ncols The number of columns in the input image
     *
     * @param nrows The number of rows in the input image
     *
     * @param cdelt1 Pixel size in arcsec
     */
    virtual PartitionMap *process(double *img, int ncols, int nrows, double cdelt1) = 0;

};

#endif
