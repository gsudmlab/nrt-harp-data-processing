/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * @file IClusterProcessor.cpp
 *
 * @author Dustin Kempton, GSU DMLab
 * Contact: dkempton1@gsu.edu
 *
 * Created on March 4, 2021
 */

#ifndef ICLUSTER_PROCESSOR_CPP
#define ICLUSTER_PROCESSOR_CPP

#include <vector>

/**
 * Interface for classes that perform some sort of processing of clusters that have already been identified.
 */
class IClusterProcessor {
public:

    virtual ~IClusterProcessor() {
    }
    ;

    /**
     * Method that takes a set of partitions, and performs some processing task on those partitions, editing the set
     * in place.
     *
     * @param partitions A list of lists, where each of the lists contain the index in the input image that belongs to
     * the partition that the list represents.
     *
     * @param ncols The number of columns in the input image
     *
     * @param nrows The number of rows in the input image
     *
     */
    virtual void process(std::vector<std::vector<int> *> *partitions, int ncols, int nrows)=0;
};

#endif