/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * @file IImageFilter.cpp
 *
 * @author Dustin Kempton, GSU DMLab
 * Contact: dkempton1@gsu.edu
 *
 * Created on September 17, 2018
 */

#ifndef IIMAGEFILTER_CPP
#define IIMAGEFILTER_CPP

/**
 * Interface for image filtering classes used for mdi-mvts4swa
 *
 * This interface defines the minimum guaranteed behavior of classes
 * used to apply filters to images
 *
 */
class IImageFilter {
public:
	virtual ~IImageFilter() {
	}
	;

	/**
	 * Method that takes an input image and applies a filter of some type to
	 * the image while copying it into the output image. The size of the two
	 * images are expected to be related in such a way that the calculation
	 * of each value of the output image can be mapped to a pixel in the input
	 * image by use of the ncols and nrows of the output image.
	 *
	 * @param in_img The input image
	 *
	 * @param out_img The ouput image
	 *
	 * @param ncols The number of columns in the output image
	 *
	 * @param nrows The number of rows in the ouput image
	 */
	virtual int process(double *in_img, double *out_img, int ncols,
			int nrows)=0;

};

#endif /* OPENACC_IIMAGEFILTER_HPP */
