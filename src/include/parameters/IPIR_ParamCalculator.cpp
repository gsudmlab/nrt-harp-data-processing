/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * File IPIR_ParamCalculator.cpp
 * Author: Dustin Kempton, GSU DMLab
 *
 * Created on July 26, 2021
 */

#ifndef IPIR_PARAMCALCULATOR_CPP
#define IPIR_PARAMCALCULATOR_CPP

class IPIR_ParamCalculator {
public:
	virtual ~IPIR_ParamCalculator() {
	}
	;

	virtual int calculateParameter(double *los, double *pir,  double *mask, double *bitmap,
			const double cdelt1, const double rsun_ref, const double rsun_obs,
			const int nx, const int ny, double *results) = 0;

};

#endif /* IPIR_PARAMCALCULATOR_CPP */
