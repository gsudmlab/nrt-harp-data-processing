/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * @file IOptimizer.cpp
 *
 * @author Dustin Kempton, GSU DMLab
 * Contact: dkempton1@gsu.edu
 *
 * Created on August 11, 2020
 */

#ifndef IOPTIMIZER_CPP
#define IOPTIMIZER_CPP

#include <vector>
#include <tuple>

struct PathMap {

    /**
     * A list of mappings from points in one list to points in another list. Each value in the individual map entry
     * represents the index within the input lists.  If item index 1 from the positive flux list is mapped to item
     * index 2 in the negative flux list, then the map item is a tuple of value (1,2).
     */
    std::vector<std::tuple<int, int>> *mappings;

};

/**
 * Interface for algorithms to find a least cost mapping of points in one list to points in another the input points
 * represent different polarity points of interest, who's x,y coordinates are calculated from the value in the lists
 * where the values represent the index in a 2D matrix of size ncols by nrows. The output specifies the mapping of the
 * points of interest where each value in the individual map entry represents the index within the input lists.  If
 * item index 1 from the positive flux list is mapped to item index 2 in the negative flux list, then the map item is a
 * tuple of value (1,2).
 */
class IOptimizer {
public:
    virtual ~IOptimizer() {
    };

    /**
     * Method that takes a two lists of points of interest and attempts to find an optimal map between the points in
     * one list to the points in the other list based on some criteria.
     *
     * @param positiveFluxPartitions A list of partitions that are positive polarity 'cities'
     *
     * @param negativeFluxPartitions A list of partitions that are negative polarity 'cities'
     *
     * @param ncols The number of columns in the original input image
     *
     * @param nrows The number of rows in the original input image
     *
     */
    virtual PathMap *process(std::vector<std::vector<int> *> *positiveFluxPartitions,
                             std::vector<std::vector<int> *> *negativeFluxPartitions, int ncols,
                             int nrows) = 0;

};

#endif
