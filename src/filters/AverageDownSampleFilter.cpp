/*
 * File: BoxCarSmoothingFilter_omp.cpp
 * Author: Dustin Kempton
 *
 *
 * Source of this filter has been adapted from:
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 *
 *
 * Created on October 01, 2018
 */

#ifndef AVERAGEFILTER_OMP_CPP
#define AVERAGEFILTER_OMP_CPP

#include <math.h>

#include "../include/filters/IImageFilter.cpp"

class AverageDownSampleFilter : public IImageFilter {

    int minsupport = 1;
    double scaleFactor = 1.0;
    double *kerx;
    double *kery;
    int fwidth;

public:

    /**
     * Scale is percent where 100.0 is 100%
     */
    AverageDownSampleFilter(double scale) {

        this->scaleFactor = scale * 0.01;

        // compute support size = how many source pixels for 1 sampled pixel ?
        int support = (int) (1 + 1.0 / this->scaleFactor);
        // minimum support
        if (support < this->minsupport)
            support = this->minsupport;
        // support size must be odd
        if (support % 2 == 0)
            support++;

        int fwidth = support;

        this->kerx = new double[fwidth];
        this->kery = new double[fwidth];

        this->fwidth = fwidth;

        for (int i = 0; i < fwidth; i++) {
            this->kerx[i] = 1.0;
            this->kery[i] = 1.0;
        }
    }

    ~AverageDownSampleFilter() {
        delete[] kerx;
        delete[] kery;

    }

    int process(double *img, double *out_img, int nx, int ny) {

        int newwidth = (int) ((nx * this->scaleFactor) + 0.5);
        int newheight = (int) ((ny * this->scaleFactor) + 0.5);

        //for each pixel in output image
#pragma omp parallel for collapse(2)
        for (int y = 0; y < newheight; y++) {
            for (int x = 0; x < newwidth; x++) {

                // ideal sample point in the source image
                double xo = x / this->scaleFactor;
                double yo = y / this->scaleFactor;

                // separate integer part and fractional part
                int x_int = (int) xo;
                int y_int = (int) yo;

                // compute resampled value
                double val = this->convolve(img, nx, ny, x_int, y_int,
                                            this->kerx, this->kery, this->fwidth);
                out_img[y * newwidth + x] = val;
            }
        }

        return 0;
    }

private:

    double convolve(double *img, const int nx, const int ny, const int x,
                    const int y, double *kerX, double *kerY, const int kernSize) {

        const int halfwindow = kernSize / 2;

        int count = 0;
        double outValue = 0;
        //Loop over the row and column offset from the pixel we are to
        //put the output into.

        for (int dy = -halfwindow; dy <= halfwindow; dy++) {
            int row = y + dy;
            if ((row < 0 || row >= ny))
                continue;

            int idxY = (row) * nx;

            double gx = 0;
            //pass 1: horizontal convolution of values
            for (int i = -halfwindow; i <= halfwindow; i++) {
                int idx = (x + i);
                if (idx < 0 || idx >= nx)
                    continue;

                gx += img[idxY + idx] * kerX[halfwindow - i];
                count++;
            }

            outValue += gx * kerY[halfwindow - dy];
        }

        //normalization;
        outValue = outValue / count;

        return outValue;
    }

};

#endif /* AVERAGEFILTER_ACC_CPP */
