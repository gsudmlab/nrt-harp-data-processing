/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * File: AverageCleaningFilter.cpp
 * Author: Dustin Kempton
 *
 *
 * Source of this filter has been adapted from:
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 *
 *
 * Created on Jul 29, 2021
 */

#ifndef AVERAGECLEANINGFILTER_CPP
#define AVERAGECLEANINGFILTER_CPP

#include <cmath>

#include "../include/filters/IImageFilter.cpp"

class AverageCleaningFilter : public IImageFilter {

    int minsupport = 1;
    double threshold;
    double *kerx;
    double *kery;
    int fwidth;

public:

    /**
     * Threshold is the value used to trigger removal of a pixel
     */
    AverageCleaningFilter(double threshold) {

        this->threshold = threshold;

        int fwidth = 3;

        this->kerx = new double[fwidth];
        this->kery = new double[fwidth];

        this->fwidth = fwidth;

        for (int i = 0; i < fwidth; i++) {
            this->kerx[i] = 1.0 / fwidth;
            this->kery[i] = 1.0 / fwidth;
        }
    }

    ~AverageCleaningFilter() {
        delete[] kerx;
        delete[] kery;

    }

    int process(double *img, double *out_img, int nx, int ny) {

        //for each pixel in output image
#pragma omp parallel for
        for (int y = 0; y < ny; y++) {
            int rowIdx = y * nx;
            for (int x = 0; x < nx; x++) {
                int idx = rowIdx + x;
                double val = std::fabs(img[idx]);
                if (val > this->threshold) {
                    // compute resampled value
                    val = this->convolve(img, nx, ny, x, y, this->fwidth);
                    out_img[idx] = val;
                } else {
                    out_img[idx] = img[idx];
                }
            }
        }

        return 0;
    }

private:

    double convolve(double *img, const int nx, const int ny, const int x,
                    const int y, const int kernSize) {

        const int halfwindow = kernSize / 2;

        int count = 0;
        double outValue = 0;
        //Loop over the row and column offset from the pixel we are to
        //put the output into.

        for (int dy = -halfwindow; dy <= halfwindow; dy++) {
            int row = y + dy;
            if (row < 0 || row >= ny)
                continue;

            int idxY = (row) * nx;

            double gx = 0;
            //pass 1: horizontal convolution of values
            for (int i = -halfwindow; i <= halfwindow; i++) {
                int idx = (x + i);
                if (idx < 0 || idx >= nx || (idxY == y && idx == x))
                    continue;

                gx += img[idxY + idx];
                count++;
            }

            outValue += gx;
        }

        outValue /= count;

        return outValue;
    }

};

#endif /* AVERAGEFILTER_ACC_CPP */
