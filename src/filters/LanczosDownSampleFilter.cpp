/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * File: LanczosFilter_omp.cpp
 * Author: Dustin Kempton
 *
 * Source of this filter has been adapted from:
 * http://subversion.developpez.com/projets/Millie/trunk/MillieCoreFilter/src/millie/plugins/core/transform/LanczosResamplePlugin.java
 *
 * Created on July 31, 2018
 */

#ifndef LANFILTER_CPP
#define LANFILTER_CPP

#include <math.h>

#include "../include/filters/IImageFilter.cpp"

using namespace std;

class LanczosDownSampleFilter : public IImageFilter {
private:
    double pi = (3.14159265358979323846264338327950288);
    int minsupport = 3;
    double scaleFactor = 1.0;
    struct Kernel1D {
    public:
        int *coeffs = nullptr;
        int size = 0;
        int normalizer = 0;
    };

public:

    /**
     * Scale is percent where 100.0 is 100%
     */
    LanczosDownSampleFilter(double scale) {
        this->scaleFactor = scale * 0.01;
    }

    ~LanczosDownSampleFilter() {

    }

    int process(double *img, double *out_img, int nx, int ny) {

        int numKern = 100;
        Kernel1D *kernelsCacheX = new Kernel1D[numKern];
        Kernel1D *kernelsCacheY = new Kernel1D[numKern];

        int newwidth = (int) ((nx * this->scaleFactor) + 0.5);
        int newheight = (int) ((ny * this->scaleFactor) + 0.5);

        //Initialize the kernel y
        for (int y = 0; y < newheight; y++) {
            // ideal sample point in the source image
            double yo = y / this->scaleFactor;

            // separate integer part and fractional part
            int y_int = (int) yo;
            double y_frac = yo - y_int;
            int kid = (int) (y_frac * 100);

            if (kernelsCacheY[kid].coeffs == nullptr) {
                Kernel1D ky = precompute(this->scaleFactor, y_frac);
                kernelsCacheY[kid] = ky;
            }
        }

        //Initialize the kernel x
        for (int x = 0; x < newwidth; x++) {
            // ideal sample point in the source image
            double xo = x / this->scaleFactor;

            // separate integer part and fractional part
            int x_int = (int) xo;
            double x_frac = xo - x_int;
            int kid = (int) (x_frac * 100);

            if (kernelsCacheX[kid].coeffs == nullptr) {
                Kernel1D kx = precompute(this->scaleFactor, x_frac);
                kernelsCacheX[kid] = kx;
            }
        }

        Image *inImgWrapper = new Image();
        inImgWrapper->values = img;
        inImgWrapper->nX = nx;
        inImgWrapper->nY = ny;

        //for each pixel in output image
#pragma omp parallel for collapse(2)
        for (int y = 0; y < newheight; y++) {
            for (int x = 0; x < newwidth; x++) {

                // ideal sample point in the source image
                double xo = x / this->scaleFactor;
                double yo = y / this->scaleFactor;

                // separate integer part and fractional part
                int x_int = (int) xo;
                double x_frac = xo - x_int;
                int y_int = (int) yo;
                double y_frac = yo - y_int;

                // get/compute resampling Kernels
                int kidX = (int) (x_frac * numKern);
                Kernel1D kx = kernelsCacheX[kidX];

                int kidY = (int) (y_frac * numKern);
                Kernel1D ky = kernelsCacheY[kidY];

                // compute resampled value
                double val = this->convolve(inImgWrapper, x_int, y_int,
                                            kx.coeffs, ky.coeffs, kx.normalizer, ky.normalizer,
                                            kx.size);
                out_img[y * newwidth + x] = val;
            }
        }

        delete inImgWrapper;

        for (int i = 0; i < numKern; i++) {
            if (kernelsCacheX[i].coeffs != nullptr) {
                delete[] kernelsCacheX[i].coeffs;
            }
            if (kernelsCacheY[i].coeffs != nullptr) {
                delete[] kernelsCacheY[i].coeffs;
            }
        }
        delete[] kernelsCacheX;
        delete[] kernelsCacheY;

        return 0;
    }

private:

    struct Image {
    public:
        double *values;
        int nX;
        int nY;
    };

    /**
     *  Lanczos function: sinc(d.pi)*sinc(d.pi/support)
     *
     * @param support,d input parameters
     * @return value of the function
     */
    double lanczos(double support, double d) {
        if (d == 0)
            return 1.0;
        if (d >= support)
            return 0.0;
        double t = d * this->pi;
        return support * sin(t) * sin(t / support) / (t * t);
    }

    /**
     * Compute a Lanczos resampling kernel
     *
     * @param scale scale to apply on the original image
     * @param frac fractionnal part of ideal sample point
     * @return the Lanczos resampling kernel
     */
    Kernel1D precompute(double scaleVal, double frac) {

        // compute support size = how many source pixels for 1 sampled pixel ?
        int support = (int) (1 + 1.0 / scaleVal);
        // minimum support
        if (support < this->minsupport)
            support = this->minsupport;
        // support size must be odd
        if (support % 2 == 0)
            support++;

        // scale limiter (minimum unit = 1 pixel)
        scaleVal = scaleVal < 1.0 ? scaleVal : 1.0;

        // construct an empty kernel
        Kernel1D kernel = Kernel1D();
        kernel.size = support;
        kernel.coeffs = new int[support];

        int i = 0;
        int halfwindow = support / 2;
        for (int dx = -halfwindow; dx <= halfwindow; dx++) {

            // ideal sample points (in the source image)
            double x = scaleVal * (dx + frac);

            // corresponding weight (=contribution) of closest source pixel
            double coef = this->lanczos(halfwindow, x);

            // store to kernel
            int c = (int) (1000 * coef + 0.5);
            kernel.coeffs[i++] = c;
            kernel.normalizer += c;
        }

        return kernel;
    }

    double convolve(Image *img, int x, int y, int *kernelX, int *kernelY,
                    int kXNorm, int kYNorm, int kSize) {

        int nx = img->nX;
        int ny = img->nY;
        int halfwindow = kSize / 2;

        double outValue = 0;
        //Loop over the row and column offset from the pixel we are to
        //put the output into.
        for (int dy = -halfwindow; dy <= halfwindow; dy++) {
            int row = y + dy;
            if ((row < 0 || row >= ny))
                continue;

            int idxY = (row) * nx;

            double gx = 0;
            //pass 1: horizontal convolution of values
            for (int i = -halfwindow; i <= halfwindow; i++) {
                int idx = (x + i);
                if (idx < 0 || idx >= nx)
                    continue;

                gx += img->values[idxY + idx] * kernelX[halfwindow - i];
            }

            outValue += gx * kernelY[halfwindow - dy];

        }

        //normalization;
        double norm = kXNorm * kYNorm;
        outValue = outValue / norm;

        return outValue;
    }

};

#endif /* LANFILTER_OMP_CPP */
