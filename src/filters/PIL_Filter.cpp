/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * File: PIL_Filter.cpp
 * Author: Dustin Kempton
 *
 *
 * Source of this filter has been adapted from:
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 *
 *
 * Created on October 03, 2018
 */

#ifndef PILFILTER_OMP_CPP
#define PILFILTER_OMP_CPP


#include "../include/filters/IImageFilter.cpp"

class PIL_Filter : public IImageFilter {

    IImageFilter *boxcarFilter;
    int threshold;

public:

    PIL_Filter(IImageFilter *boxcarFilter, int threshold) {
        this->threshold = threshold;
        this->boxcarFilter = boxcarFilter;
    }

    int process(double *img, double *out_img, int nx, int ny) {

        double *p1p0 = new double[nx * ny];
        double *p1n0 = new double[nx * ny];
        double *p1p = new double[nx * ny];
        double *p1n = new double[nx * ny];

        //................[Step 1]..................
        //Identify positive and negative pixels greater than +/- 150 gauss
        //Label those pixels with a 1.0 in arrays p1p0 and p1n0 respectively
#pragma omp parallel for
        for (int j = 0; j < ny; j++) {
            for (int i = 0; i < nx; i++) {
                int index = j * nx + i;
                if (img[index] > this->threshold)
                    p1p0[index] = 1.0;
                else
                    p1p0[index] = 0.0;

                if (img[index] < -this->threshold)
                    p1n0[index] = 1.0;
                else
                    p1n0[index] = 0.0;
            }
        }

        //................[Step 2]..................
        // smooth each of the negative and positive pixel bitmaps
        this->boxcarFilter->process(p1p0, p1p, nx, ny);
        this->boxcarFilter->process(p1n0, p1n, nx, ny);

        // =============== [STEP 3] ===============
        // find the pixels for which p1p and p1n are both equal to 1.
        // this defines the polarity inversion line
#pragma omp parallel for
        for (int j = 0; j < ny; j++) {
            for (int i = 0; i < nx; i++) {
                int index = j * nx + i;
                if ((p1p[index] > 0.0) && (p1n[index] > 0.0))
                    out_img[index] = 1.0;
                else
                    out_img[index] = 0.0;
            }
        }

        delete[] p1p0;
        delete[] p1n0;
        delete[] p1p;
        delete[] p1n;

        return 0;
    }
};

#endif
