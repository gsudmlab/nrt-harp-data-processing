/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * @file MCTModelCreator.cpp
 *
 * @author Dustin Kempton, GSU DMLab
 * Contact: dkempton1@gsu.edu
 *
 * Created on June 19, 2020
 */

#ifndef MCT_MODELCREATOR_CPP
#define MCT_MODELCREATOR_CPP

#include <math.h>
#include <vector>
#include <algorithm>

#include "../include/partition/IImagePartitioner.cpp"
#include "../include/models/IModelCreator.cpp"
#include "../include/filters/IImageFilter.cpp"

/**
 * This class calculates the magnetic charge topology (MCT) model first proposed by Barnes et al. (ApJ, 629, 561, 2005).
 *
 * The calculations assume observations are at 1 AU
 */
class MCTModelCreator : public IModelCreator {

    IImageFilter *cleaningFilter;
    IImagePartitioner *partitioner;

    double lenPerArcSec;

public:

    MCTModelCreator(IImagePartitioner *partitioner, IImageFilter *cleaningFilter, double lenPerArcSec) {
        this->partitioner = partitioner;
        this->cleaningFilter = cleaningFilter;
        this->lenPerArcSec = lenPerArcSec;
    }

    MCTModel *process(double *bz, int ncols, int nrows, double cdelt1) {


        /*
         * Cleanup any pixels that may be out of range by giving it the average value of the pixels surrounding it
         */
        double *bzFiltered = new double[ncols * nrows];
        this->cleaningFilter->process(bz, bzFiltered, ncols, nrows);

        /*
         * Process the cleaned up radial magnetic field flux map to partition into clusters of positive and negative
         * magnetic flux.
         */
        PartitionMap *partMap = this->partitioner->process(bzFiltered, ncols, nrows, cdelt1);

        //Nothing was found return nothing
        if (partMap->partitions->empty()) {
            MCTModel *result = new MCTModel();
            result->NElem = 0;
            result->uncanceledFluxValues = new std::vector<double>();
            result->centroidIdxs = new std::vector<int>();
            result->partitions = partMap->partitions;
            result->nrows = nrows;
            result->ncols = ncols;
            delete partMap;

            delete[] bzFiltered;

            return result;
        }

        std::vector<std::vector<int> *> *partitions = partMap->partitions;
        delete partMap;

        int partNum = partitions->size();

        std::vector<int> *centroidIdxs = new std::vector<int>();
        std::vector<double> *uncanceledFluxValues = new std::vector<double>();


        //Process each segment found by the partition call
        for (std::vector<std::vector<int> *>::iterator it = partitions->begin(); it != partitions->end(); it++) {
            std::vector<int> *part = (*it);

            int weightedCenterIdx = 0;
            double uncanceledFlux = 0.0;
            this->getFluxWeightedCenter(bzFiltered, part, ncols, nrows, weightedCenterIdx, uncanceledFlux, cdelt1);

            uncanceledFluxValues->push_back(uncanceledFlux);
            centroidIdxs->push_back(weightedCenterIdx);
        }
        delete[] bzFiltered;

        MCTModel *result = new MCTModel();
        result->NElem = partNum;
        result->uncanceledFluxValues = uncanceledFluxValues;
        result->centroidIdxs = centroidIdxs;
        result->partitions = partitions;
        result->nrows = nrows;
        result->ncols = ncols;


        return result;
    }

    /**
     * Finds the flux-weighted centroid, the flux-weighted radius of the radial magnetic field, and total flux within
     * the partition that is passed in.
     *
     * @param bz The distribution of radial magnetic field (G)
     *
     * @param partition A list of index locations for the partition being processed
     *
     * @param ncols Number of columns in the input image
     *
     * @param nrows Number of rows in the input image
     *
     * @param weightedCenterIdx Index in bz where the centroid is located for the partition being processed
     *
     * @param uncanceledFluxTotal The total uncanceled flux within the partition being processed
     *
     * @param cdelt1 Pixel size in arcsec per pixel
     */
    void getFluxWeightedCenter(double *bz, std::vector<int> *partition, int ncols, int nrows, int &weightedCenterIdx,
                               double &uncanceledFluxTotal, double cdelt1) {

        double lambda = this->lenPerArcSec * (cdelt1 / 3600);
        double lambda2 = pow(lambda, 2.0);


        double totXFlux = 0;
        double totYFlux = 0;
        double totFlux = 0;
        double sumUncanceledFlux = 0;

        /*
        * count non-zero pixel values and add up the flux and pixel locations weighted by flux at each location
        */
        for (std::vector<int>::iterator it = partition->begin(); it != partition->end(); it++) {
            int idx = (*it);
            int row = idx / ncols;
            int col = idx % ncols;

            double val = lambda2 * abs(bz[idx]);
            totFlux += val;
            totXFlux += val * col;
            totYFlux += val * row;
            sumUncanceledFlux += lambda2 * bz[idx];
        }

        //Calculate the weighted center of the flux partition
        int xc = int(totXFlux / totFlux + 0.5);
        if (xc >= ncols) xc = ncols - 1;
        int yc = int(totYFlux / totFlux + 0.5);
        if (yc >= nrows) yc = nrows - 1;

        /*
         * Verify that the weighted center is associated with a nonzero value of BZ and if it doesn't, find the closest
         * nonzero value to assign the center to.
         */
        if (bz[yc * ncols + xc] == 0.0) {
            int idxMin = 0;
            double minDist = std::numeric_limits<double>::max();
            for (std::vector<int>::iterator it = partition->begin(); it != partition->end(); it++) {
                int idx = (*it);
                int row = idx / ncols;
                int col = idx % ncols;

                double val = bz[idx];
                if (val != 0.0) {
                    double dist = sqrt(pow(col - xc, 2.0) + pow(row - yc, 2.0));
                    if (dist < minDist) {
                        idxMin = idx;
                        minDist = dist;
                    }
                }
            }

            xc = idxMin % ncols;
            yc = idxMin / ncols;
        }


        weightedCenterIdx = yc * ncols + xc;
        uncanceledFluxTotal = sumUncanceledFlux;

    }

};

#endif
