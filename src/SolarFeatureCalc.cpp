/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * File: SolarFeatureCalc_omp.cpp
 * Author: Dustin Kempton
 *
 * Created on July 30, 2020
 */

#ifndef FEATURE_CALC_CPP
#define FEATURE_CALC_CPP

#include <iostream>
#include <math.h>

#include "include/partition/IClusterProcessor.cpp"
#include "include/partition/IClusterPartitioner.cpp"

#include "include/partition/ICompositeClusterProcessor.cpp"
#include "include/calculators/ISepLengthCalculator.cpp"
#include "include/calculators/IFluxSpreader.cpp"

#include "include/parameters/IParamCalculator.cpp"
#include "include/parameters/IPIR_ParamCalculator.cpp"
#include "include/filters/IImageFilter.cpp"
#include "include/models/IModelCreator.cpp"
#include "include/optimize/IOptimizer.cpp"


#include "filters/GaussianSmoothingFilter.cpp"
#include "filters/AverageDownSampleFilter.cpp"
#include "filters/LanczosDownSampleFilter.cpp"
#include "filters/AverageCleaningFilter.cpp"
#include "filters/PIL_Filter_W_Gauss.cpp"
#include "filters/AverageFilter.cpp"
#include "filters/PIL_Filter.cpp"

#include "parameters/AbsoluteFluxCalculator.cpp"
#include "parameters/PIR_AbsoluteFluxCalculator.cpp"
#include "parameters/BZ_DerivativeCalculator.cpp"
#include "parameters/PIR_BZ_DerivativeCalculator.cpp"
#include "parameters/FractalDimCalculator.cpp"
#include "parameters/R_ValueCalculator.cpp"
#include "parameters/BeffCalculator.cpp"
#include "models/MCTModelCreator.cpp"

#include "optimize/GreedyOptimizer.cpp"
#include "transform/LOStoBZTransform.cpp"
#include "partition/MCTPartitioner.cpp"

#include "calculators/FluxSpreader.cpp"
#include "partition/IslandSwallower.cpp"
#include "partition/ClusterConnector.cpp"
#include "partition/ClusterCorrector.cpp"
#include "partition/ClusterPartitioner.cpp"
#include "calculators/SepLengthCalculator.cpp"
#include "partition/SmallFluxStructureRemover.cpp"
#include "partition/DownhillClusterPartitioner.cpp"

using namespace std;

double threshold = 4e3;
double min_flux = 1e18;
double mct_Bz_thres = 50;
int min_size = 40;
double rsun = 6.9598e10;
double radsindeg = (3.14159265358979323846264338327950288) / 180.0;
int pilThreshold = 50;
double sigma = 10.0 / 2.3548;
int hwidthGauss = 20;
int pir_pilThreshold = 150;
double lenPerArcSec = 0;
double scale = 25;

IParamCalculator **calculators = 0;
IPIR_ParamCalculator **pirCalculators = 0;
IImageFilter *PIR_PILFilter = 0;
LOStoBZTransform *losTransformer = 0;


extern "C" void init_lib();

extern "C" void computeSolarFeatures(double *los, double *mask, double *bitmap, double cdelt1,
                                     double crpix1, double crpix2, double crval1, double crval2,
                                     double rsun_ref, double rsun_obs, double dsun_obs, double crln_obs,
                                     int nx, int ny, double *out_solar_features);

void init_lib() {
    if (!calculators) {
        double rSunFrac = 1.0 / 215;
        double apRad = atan(rSunFrac);
        double apArcSec = apRad * radsindeg * 3600.0;
        lenPerArcSec = rsun / apArcSec;

        IImageFilter *gaussianFilter = new GaussianSmoothingFilter(hwidthGauss,
                                                                   sigma);
        IImageFilter *averageFilter = new AverageFilter(5);
        IImageFilter *averageFilter2 = new AverageFilter(3);
        IImageFilter *lanzDownFilter = new LanczosDownSampleFilter(scale);
        IImageFilter *averDownFilter = new AverageDownSampleFilter(scale);
        IImageFilter *PILFilter = new PIL_Filter(averageFilter, pilThreshold);


        IClusterProcessor *clusterConnector = new ClusterConnector();
        IClusterPartitioner *clusterPartitioner = new ClusterPartitioner(clusterConnector, averageFilter, mct_Bz_thres,
                                                                         min_size);
        IClusterPartitioner *downhillClusterPartitioner = new DownhillClusterPartitioner();
        ICompositeClusterProcessor *islandSwallower = new IslandSwallower();
        ICompositeClusterProcessor *smallFluxStructureRemover = new SmallFluxStructureRemover(min_flux, lenPerArcSec);
        ICompositeClusterProcessor *clusterCorrector = new ClusterCorrector(clusterConnector, smallFluxStructureRemover,
                                                                            min_size);


        IImagePartitioner *partitioner = new MCTPartitioner(clusterPartitioner, downhillClusterPartitioner,
                                                            islandSwallower, smallFluxStructureRemover,
                                                            clusterCorrector, averageFilter, min_size);

        IImageFilter *cleaningFilter = new AverageCleaningFilter(threshold);
        IOptimizer *optimizer = new GreedyOptimizer();
        IModelCreator *modelCreator = new MCTModelCreator(partitioner, cleaningFilter, lenPerArcSec);
        IFluxSpreader *fluxSpreader = new FluxSpreader(min_flux);

        ISepLengthCalculator *sepCalculator = new SepLengthCalculator(modelCreator, optimizer, fluxSpreader, min_flux);


        calculators = new IParamCalculator *[5];
        calculators[0] = new AbsoluteFluxCalculator();
        calculators[1] = new BZ_DerivativeCalculator();
        calculators[2] = new FractalDimCalculator(PILFilter);
        calculators[3] = new R_ValueCalculator(averDownFilter, lanzDownFilter, averageFilter2, gaussianFilter, scale);
        calculators[4] = new BeffCalculator(averDownFilter, sepCalculator, lenPerArcSec, scale);

        PIR_PILFilter = new PIL_Filter_W_Gauss_omp(averageFilter2, gaussianFilter,
                                                   pir_pilThreshold, hwidthGauss);

        losTransformer = new LOStoBZTransform();


        pirCalculators = new IPIR_ParamCalculator *[2];
        pirCalculators[0] = new PIR_AbsoluteFluxCalculator();
        pirCalculators[1] = new PIR_BZ_DerivativeCalculator();
    }
}

/**
 * Computes and returns the solar features in this order:
 * 	[USFLUX, FLUXIMB, MEANGBZ, FDIM, R_VALUE, BEFF, BEFF_MAX, SEP_LEN, MEAN_FLUX, ERR_FLUX, PIL_LEN
 * 	PIR_USFLUX, PIR_FLUXIMB, PIR_MEANGBZ]
 */
void computeSolarFeatures(double *los, double *mask, double *bitmap, double cdelt1, double crpix1, double crpix2,
                          double crval1, double crval2, double rsun_ref, double rsun_obs, double dsun_obs,
                          double crln_obs, int nx, int ny, double *out_solar_features) {

    double *correctedImg = losTransformer->process(los, crln_obs, cdelt1, dsun_obs, nx, ny, crval1, crval2);
    double *results = new double[6];

    //Calculate USFLUX, FLUXIMB,
    calculators[0]->calculateParameter(correctedImg, mask, bitmap, cdelt1, rsun_ref, rsun_obs, nx, ny, results);
    out_solar_features[0] = results[0];
    out_solar_features[1] = results[1];

    //Calculate MEANGBZ
    calculators[1]->calculateParameter(correctedImg, mask, bitmap, cdelt1, rsun_ref, rsun_obs, nx, ny, results);
    out_solar_features[2] = results[0];

    //Calculate FDIM
    calculators[2]->calculateParameter(correctedImg, mask, bitmap, cdelt1, rsun_ref, rsun_obs, nx, ny, results);
    out_solar_features[3] = results[0];

    //Calculate R_VALUE
    calculators[3]->calculateParameter(correctedImg, mask, bitmap, cdelt1, rsun_ref, rsun_obs, nx, ny, results);
    out_solar_features[4] = results[0];

    //Todo: Fix Beff Optimizer to use less RAM
    //calculators[4]->calculateParameter(correctedImg, mask, bitmap, cdelt1, rsun_ref, rsun_obs, nx, ny, results);
    out_solar_features[5] = 0.0; //results[0];//beff
    out_solar_features[6] = 0.0; //results[1];//beffMax
    out_solar_features[7] = 0.0; //results[2];//sepLen
    out_solar_features[8] = 0.0; //results[3];//meanFlux
    out_solar_features[9] = 0.0; //results[4];//errorFlux

    //Calculate PIL_LEN
    double *pil_img = new double[nx * ny];
    int pilLen = PIR_PILFilter->process(correctedImg, pil_img, nx, ny);
    double lambda = lenPerArcSec * (cdelt1 / 3600.0);
    out_solar_features[10] = pilLen * lambda;

    //Calculate PIR_USFLUX, PIR_FLUXIMB,
    pirCalculators[0]->calculateParameter(correctedImg, pil_img, mask, bitmap, cdelt1, rsun_ref, rsun_obs, nx, ny,
                                          results);
    out_solar_features[11] = results[0];
    out_solar_features[12] = results[1];

    //Calculate PIR_MEANGBZ
    pirCalculators[1]->calculateParameter(correctedImg, pil_img, mask, bitmap, cdelt1, rsun_ref, rsun_obs, nx, ny,
                                          results);
    out_solar_features[13] = results[0];

    delete[] correctedImg;
    delete[] pil_img;
    delete[] results;

}

#endif
