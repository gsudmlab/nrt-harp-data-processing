/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * File: FractalDimCalculator_omp.cpp
 * Author: Dustin Kempton
 *
 *
 * Created on October 03, 2018
 */
#ifndef FRACTALCALC_OMP_CPP
#define FRACTALCALC_OMP_CPP

#include <algorithm>
#include <math.h>

#include "../include/parameters/IParamCalculator.cpp"
#include "../include/filters/IImageFilter.cpp"

/**
 * This class computes FDIM, BZ_FDIM, BT_FDIM, BP_FDIM
 */
class FractalDimCalculator: public IParamCalculator {

	IImageFilter* PILFilter;
public:

	FractalDimCalculator(IImageFilter* PILFilter) {
		this->PILFilter = PILFilter;
	}

	/**
	 * calculateParameter: returns FDIM in results[0]
	 */
	int calculateParameter(double *los, double *mask, double *bitmap, const double cdelt1, const double rsun_ref,
                           const double rsun_obs, const int nx, const int ny, double *results) {


		int maxSize = std::min(nx, ny);
		double* p1 = new double[nx * ny];

		//Compute FDIM
		this->PILFilter->process(los, p1, nx, ny);
		double fractDim = this->computeFDimension(p1, nx, ny, maxSize);
		results[0] = fractDim;

		delete[] p1;

		return 1;
	}

private:

	double computeFDimension(double* image, int nx, int ny, int n) {

		int maxBoxSize = ((int) (2 * round(sqrt(n))));
		int numBoxes = (int) log2(maxBoxSize * 1.0);

		int boxSizes[numBoxes];

		boxSizes[0] = maxBoxSize;
		int i = 1;
		while ((maxBoxSize /= 2) > 1) {
			boxSizes[i++] = maxBoxSize;
		}

		/*
		 * Do counting for boxes of different sizes.
		 */
		double counts[numBoxes];
#pragma omp parallel for
		for (int i = 0; i < numBoxes; i++) {
			counts[i] = this->countBoxes(image, nx, ny, boxSizes[i]);
		}

		//do Simple linear regression.
		double sumX = 0;
		double sumY = 0;
		double sumXX = 0;
		double sumXY = 0;
		for (int i = 0; i < numBoxes; i++) {
			double x = counts[i];
			double y = log(counts[i]);
			sumX += x;
			sumY += y;
			sumXX += x * x;
			sumXY += x * y;
		}

		double slope = (numBoxes * sumXY - sumX * sumY)
				/ (numBoxes * sumXX - sumX * sumX);

		if (std::isfinite(slope))
			return slope;
		else
			return 0;
	}
	/**
	 * This method counts the number of boxes which intersect with at least one pixel
	 * of the foreground (i.e., detected edges)
	 *
	 * <br>
	 * <b>Note:</b> This method assumes that the given image (represented by the 1D
	 * matrix) is already <i>edge-detected</i>, meaning that the array <code>image</code>
	 * is a binary array containing only <code>colors[0]</code> as the background and
	 * <code>colors[1]</code> as the foreground.
	 * <br>
	 * <b>Note:</b> The box counting algorithm is inspired from: <a href =
	 * "https://imagej.nih.gov/ij/developer/source/index.html">FractalBoxCounter</a>
	 *
	 * @param image
	 *            The given image in the form of a 1D matrix of binary values; background
	 *            and foreground.
	 * @param imageW
	 *            The width of the image
	 * @param imageH
	 *            The height of the image
	 * @param boxSize
	 *            The size of the box using for applying the box-counting method
	 *            on the edges.
	 * @param colors
	 * 			  An array of length two. <code>colors[0]</code> represents the background color
	 *            (0:black) and color[1] the foreground color (255: white).
	 * @return The number of boxes needed to cover all the edges on the given
	 *         image.
	 */
	int countBoxes(double* image, int imageW, int imageH, int boxSize) {

		int x = 0;
		int y = 0;
		int boxW = boxSize;
		int boxH = boxSize;
		bool done = false;
		int boxCounter = 0;

		double subMatrix[boxSize * boxSize];

		do {

			int k = 0;
			for (int dy = 0; dy < boxH; dy++) {
				for (int dx = 0; dx < boxW; dx++) {
					int idxX = (x + dx);
					int idxY = (y + dy) * imageW;
					subMatrix[k] = image[idxX + idxY];
					k++;
				}
			}

			int n = boxW * boxH;
			for (int i = 0; i < n; i++) {
				if (subMatrix[i] > 0) { // If subPatch has any foreground color in it
					/*
					 * this subPatch spans over a segment of an edge, so
					 * it should be counted and there is no need to proceed any further.
					 */
					boxCounter++;
					break;
				}
			}

			//Move the box
			x += boxSize;
			if (x + boxSize > imageW) { // If the remaining horizontal space is
										// less than a box
				boxW = imageW % boxSize; // shrink the box horizontally to fit
										 // the remaining space
				if (x >= imageW) {
					// Reset w
					boxW = boxSize;
					// Reset x
					x = 0;
					// shift y
					y += boxSize;
					if (y + boxSize > imageH) { // If the remaining vertical
												// space is less than a box
						boxH = imageH % boxSize; // shrink the box vertically to
												 // fit the remaining space
						done = (y >= imageH); // done if the entire image is
											  // covered
					}
				}
			}
		} while (!done);

		return boxCounter;
	}
};

#endif
