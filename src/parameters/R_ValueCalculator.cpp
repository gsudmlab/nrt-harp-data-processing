/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * File: R_ValueCalculator_omp.cpp
 * Author: Dustin Kempton
 *
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of parameter modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 *
 *  R parameter as defined in Schrijver, 2007
 *
 * Created on October 01, 2018
 */

#ifndef RVALUE_OMP_CPP
#define RVALUE_OMP_CPP

#include <math.h>

#include "../include/filters/IImageFilter.cpp"
#include "../include/parameters/IParamCalculator.cpp"

class R_ValueCalculator : public IParamCalculator {

    IImageFilter *averageDownFilter;
    IImageFilter *downFilter;
    IImageFilter *boxcarFilter;
    IImageFilter *gaussianFilter;
    double scaleFactor = 0.25;

public:

    R_ValueCalculator(IImageFilter *averageDownFilter, IImageFilter *downFilter, IImageFilter *boxcarFilter,
                      IImageFilter *gaussianFilter, int scale) {

        this->averageDownFilter = averageDownFilter;
        this->downFilter = downFilter;
        this->boxcarFilter = boxcarFilter;
        this->gaussianFilter = gaussianFilter;
        this->scaleFactor = (scale * 0.01);
    }

    /**
     * Example function 15: R parameter as defined in Schrijver, 2007
     * Computes R_VALUE
     */
    int calculateParameter(double *los, double *mask, double *bitmap, const double cdelt1,
                           const double rsun_ref, const double rsun_obs, const int nx,
                           const int ny, double *results) {


        int newwidth = (int) ((nx * this->scaleFactor) + 0.5);
        int newheight = (int) ((ny * this->scaleFactor) + 0.5);

        double *rimage = new double[newwidth * newheight];
        double *p1 = new double[newwidth * newheight];

        //Compute R_VALUE
        this->computePIL(los, p1, nx, ny);
        this->getDownSizeRim(los, rimage, mask, bitmap, nx, ny);
        double Rparam = this->computeRValue(p1, rimage, nx, ny);
        results[0] = Rparam;

        delete[] rimage;
        delete[] p1;

        return 1;
    }

private:

    double computeRValue(double *pil_image, double *rim, int nx, int ny) {

        int newwidth = (int) ((nx * this->scaleFactor) + 0.5);
        int newheight = (int) ((ny * this->scaleFactor) + 0.5);

        int nxp = newwidth + 40;
        int nyp = newheight + 40;

        double *pmap = new double[nxp * nyp];
        double *p1pad = new double[nxp * nyp];
        double *pmapn = new double[newwidth * newheight];

        // pad p1 with zeroes so that the gaussian colvolution in step 5
        // does not cut off data within hwidth of the edge
#pragma omp parallel
        {
            // step i: zero p1pad
#pragma omp for collapse(2)
            for (int j = 0; j < nyp; j++) {
                for (int i = 0; i < nxp; i++) {
                    int index = j * nxp + i;
                    p1pad[index] = 0.0;
                }
            }

            // step ii: place p1 at the center of p1pad
#pragma omp for collapse(2)
            for (int j = 0; j < newheight; j++) {
                for (int i = 0; i < newwidth; i++) {
                    int index = j * newwidth + i;
                    int index1 = (j + 20) * nxp + (i + 20);
                    p1pad[index1] = pil_image[index];
                }
            }
        }

        this->gaussianFilter->process(p1pad, pmap, nxp, nyp);

        double sum = 0;
#pragma omp parallel
        {
            // select out the nx1 x ny1 non-padded array  within pmap
#pragma omp for collapse(2)
            for (int j = 0; j < newheight; j++) {
                for (int i = 0; i < newwidth; i++) {
                    int index = j * newwidth + i;
                    int index1 = (j + 20) * nxp + (i + 20);
                    pmapn[index] = pmap[index1];
                }
            }

            // =============== [STEP 6] ===============
            // the R parameter is calculated
#pragma omp for collapse(2), reduction(+:sum)
            for (int j = 0; j < newheight; j++) {
                for (int i = 0; i < newwidth; i++) {
                    int index = j * newwidth + i;
                    if (isnan(pmapn[index]))
                        continue;
                    if (isnan(rim[index]))
                        continue;
                    sum += pmapn[index] * fabs(rim[index]);
                }
            }
        }

        double Rparam;
        if (sum < 1.0)
            Rparam = 0.0;
        else
            Rparam = log10(sum);

        delete[] pmap;
        delete[] p1pad;
        delete[] pmapn;

        return Rparam;
    }

    void getDownSizeRim(double *in_image, double *processed_image, double *mask,
                        double *bitmask, int nx, int ny) {

        double *tmpImg = new double[nx * ny];

#pragma omp parallel for collapse(2)
        for (int j = 0; j < ny; j++) {
            for (int i = 0; i < nx; i++) {
                int idx = j * nx + i;
                double val = in_image[idx];
                if (mask[idx] < 55 || bitmask[idx] < 30)
                    val = 0;
                tmpImg[idx] = val;
            }
        }

        //................[Step 1]..................
        //Down sample the image to 1k resolution
        this->averageDownFilter->process(tmpImg, processed_image, nx, ny);
        delete[] tmpImg;
    }

    void computePIL(double *in_image, double *pil_image, int nx, int ny) {

        int newwidth = (int) ((nx * this->scaleFactor) + 0.5);
        int newheight = (int) ((ny * this->scaleFactor) + 0.5);

        double *processed_image = new double[newwidth * newheight];
        double *p1p0 = new double[newwidth * newheight];
        double *p1n0 = new double[newwidth * newheight];
        double *p1p = new double[newwidth * newheight];
        double *p1n = new double[newwidth * newheight];

        //................[Step 1]..................
        //Down sample the image to 1k resolution
        this->downFilter->process(in_image, processed_image, nx, ny);

        //................[Step 2]..................
        //Identify positive and negative pixels greater than +/- 150 gauss
        //Label those pixels with a 1.0 in arrays p1p0 and p1n0 respectively
#pragma omp parallel for collapse(2)
        for (int j = 0; j < newheight; j++) {
            for (int i = 0; i < newwidth; i++) {
                int index = j * newwidth + i;
                if (processed_image[index] > 150)
                    p1p0[index] = 1.0;
                else
                    p1p0[index] = 0.0;

                if (processed_image[index] < -150)
                    p1n0[index] = 1.0;
                else
                    p1n0[index] = 0.0;
            }
        }

        // =============== [STEP 3] ===============
        // smooth each of the negative and positive pixel bitmaps
        this->boxcarFilter->process(p1p0, p1p, newwidth, newheight);
        this->boxcarFilter->process(p1n0, p1n, newwidth, newheight);

        // =============== [STEP 4] ===============
        // find the pixels for which p1p and p1n are both equal to 1.
        // this defines the polarity inversion line
#pragma omp parallel for collapse(2)
        for (int j = 0; j < newheight; j++) {
            for (int i = 0; i < newwidth; i++) {
                int index = j * newwidth + i;
                if ((p1p[index] > 0.0) && (p1n[index] > 0.0))
                    pil_image[index] = 1.0;
                else
                    pil_image[index] = 0.0;
            }
        }

        delete[] p1p0;
        delete[] p1n0;
        delete[] p1p;
        delete[] p1n;
        delete[] processed_image;
    }

};

#endif
