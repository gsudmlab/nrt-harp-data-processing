/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * File: BZ_DerivativeCalculator_omp.cpp
 * Author: Dustin Kempton
 *
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of parameter modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 *
 * Created on July 31, 2018
 */

#ifndef BZ_DERIVATIVE_CPP
#define BZ_DERIVATIVE_CPP

#include <math.h>

#include "../include/parameters/IParamCalculator.cpp"

/**
 * This class calculates the MEANGBZ parameter
 */
class BZ_DerivativeCalculator: public IParamCalculator {

public:

	/**
	 * calculateParameter: returns the MEANGBZ parameter in results[0]
	 */
	int calculateParameter(double *los, double *mask, double *bitmap, const double cdelt1, const double rsun_ref,
                           const double rsun_obs, const int nx, const int ny, double *results) {


		if (nx < 3 || ny < 3)
			return 0;

		double *derx_bz = new double[nx * ny];
		double *dery_bz = new double[nx * ny];

		int count_mask = 0;
		double sum = 0.0;

		int n_rows = ny;
		int n_cols = nx;

#pragma omp parallel
		{
			/* brute force method of calculating the derivative (no consideration for edges) */
#pragma omp  for nowait collapse(2)
			for (int row = 0; row < n_rows; row++) {
				for (int col = 1; col < n_cols - 1; col++) {
					derx_bz[row * n_cols + col] = (los[row * n_cols + col + 1]
							- los[row * n_cols + col - 1]) * 0.5;

				}
			}

			/* brute force method of calculating the derivative (no consideration for edges) */
#pragma omp  for nowait collapse(2)
			for (int row = 1; row < n_rows - 1; row++) {
				for (int col = 0; col < n_cols; col++) {
					dery_bz[row * n_cols + col] = (los[(row + 1) * n_cols + col]
							- los[(row - 1) * n_cols + col]) * 0.5;

				}
			}

			/* consider the edges for the arrays that contribute to the variable "sum" in the computation below.
			 ignore the edges for the error terms as those arrays have been initialized to zero.
			 this is okay because the error term will ultimately not include the edge pixels as they are selected out by the mask and bitmask arrays.*/
			//i = 0;
#pragma omp  for
			for (int row = 0; row < n_rows; row++) {
				derx_bz[row * n_cols] = ((-3 * los[row * n_cols]) + (4 * los[row * n_cols + 1])
						- (los[row * n_cols + 2])) * 0.5;
			}

			const int edgI = n_cols - 1;
#pragma omp  for
			for (int row = 0; row < n_rows; row++) {
				derx_bz[row * n_cols + edgI] = ((3 * los[row * n_cols + edgI])
						- (4 * los[row * n_cols + (edgI - 1)])
						+ (los[row * n_cols + (edgI - 2)])) * 0.5;
			}

			//j = 0;
#pragma omp  for
			for (int col = 0; col < n_cols; col++) {
				dery_bz[col] = ((-3 * los[col]) + (4 * los[n_cols + col])
						- (los[2 * n_cols + col])) * 0.5;
			}

			const int edgJ = n_rows - 1;
#pragma omp for
			for (int col = 0; col < n_cols; col++) {
				dery_bz[edgJ * n_cols + col] = ((3 * los[edgJ * n_cols + col])
						- (4 * los[(edgJ - 1) * n_cols + col])
						+ (los[(edgJ - 2) * n_cols + col])) * 0.5;
			}

#pragma omp  for collapse(2),reduction(+:sum,count_mask)
			for (int row = 0; row < n_rows; row++) {
				for (int col = 0; col < n_cols; col++) {
				    if (mask[row * n_cols + col] < 70 || bitmap[row * n_cols + col] < 30)
						continue;
					if ((derx_bz[row * n_cols + col] + dery_bz[row * n_cols + col]) == 0)
						continue;
					if (isnan(los[row * n_cols + col]))
						continue;
					if (isnan(derx_bz[row * n_cols + col]))
						continue;
					if (isnan(dery_bz[row * n_cols + col]))
						continue;

					sum += sqrt(
							derx_bz[row * n_cols + col] * derx_bz[row * n_cols + col]
									+ dery_bz[row * n_cols + col]
											* dery_bz[row * n_cols + col]); /* Units of Gauss */

					count_mask++;
				}
			}
		}

		delete[] derx_bz;
		delete[] dery_bz;

		if (count_mask > 0){
		    double mean_derivative_bz = (sum) / (count_mask); // would be divided by ((nx-2)*(ny-2)) if shape of count_mask = shape of magnetogram
		    results[0] = mean_derivative_bz;
		}
		else
		    results[0] = 0;

		return 1;

	}

};

#endif
