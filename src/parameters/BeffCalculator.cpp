/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * File: BeffCalculation.cpp
 * Author: Dustin Kempton
 *
 * C++ parameter computation implemented from IDL code for calculating B effective
 * described in "Quantitative Forecasting of Major Solar Flares", by Georgoulis and Rust,
 * in The Astrophysical Journal, vol. 661, year 2007, doi:10.1086/518718,
 * url: https://doi.org/10.1086/518718
 *
 * Created on May 03, 2020
 */
#ifndef BEFF_CALCULATOR_CPP
#define BEFF_CALCULATOR_CPP

#include <math.h>
#include <algorithm>

#include "../include/filters/IImageFilter.cpp"
#include "../include/parameters/IParamCalculator.cpp"
#include "../include/calculators/ISepLengthCalculator.cpp"

/**
 * This class calculates the B effective (B_EFF) parameter of a given
 * magnetogram using simulated annealing. It uses mct_Bz_thres : to partition
 * the vector magnetogram we use the magnetic charge topology (MCT) model
 * first proposed by Barnes et al. (ApJ, 629, 561, 2005). The technique is also
 * described by Georgoulis et al. (ApJ, 761, 61, 2012). This keyword gives
 * the minimum tolerated |Bz|-value that will belong to a given partition.
 *
 * The calculations assume observations are at 1 AU
 */
class BeffCalculator : public IParamCalculator {

    IImageFilter *averageDownFilter;
    ISepLengthCalculator *sepCalculator;
    double lenPerArcSec;
    double scaleFactor = 1.0;

public:

    /**
     * Constructor
     */
    BeffCalculator(IImageFilter *averageDownFilter, ISepLengthCalculator *sepCalculator, double lenPerArcSec, double scale) {

        this->averageDownFilter = averageDownFilter;
        this->lenPerArcSec = lenPerArcSec;
        this->sepCalculator = sepCalculator;
        this->scaleFactor = scale * 0.01;
    }

    int calculateParameter(double *los, double *mask, double *bitmap, const double cdelt1, const double rsun_ref,
                           const double rsun_obs, const int nx, const int ny, double *results) {

        int newwidth = (int) ((nx * this->scaleFactor) + 0.5);
        int newheight = (int) ((ny * this->scaleFactor) + 0.5);
        double newCdelt = cdelt1 / this->scaleFactor;
        double lambda = this->lenPerArcSec * (newCdelt/3600.0);
        double lambda2 = pow(lambda, 2.0);

        double *ds_los = new double[newwidth * newheight];
        this->averageDownFilter->process(los, ds_los, nx, ny);

        SepLength *sepLen = this->sepCalculator->findSepLength(ds_los, newwidth, newheight, newCdelt);
        double len = sepLen->sepLen * lambda;
        results[2] = len;
        results[3] = sepLen->meanFlux;
        results[4] = sepLen->errFlux;

        std::vector <std::tuple<int, int, double>> *mappings = sepLen->mappings;
        delete sepLen;


        //Has any result been found
        if (len == 0) {
            results[0] = 0;
            results[1] = 0;
        } else {

            double beff = 0;
            double beff_max = 0;
            for (std::vector < std::tuple < int, int, double >> ::iterator it = mappings->begin();
                it != mappings->end(); it++){

                int posX, negX, posY, negY = 0;

                std::tuple < int, int, double > map = (*it);
                int posIdx = std::get<0>(map);
                int negIdx = std::get<1>(map);
                double flx = std::get<2>(map) ;

                this->indToCart(posIdx, posX, posY, newwidth);
                this->indToCart(negIdx, negX, negY, newwidth);

                double len = sqrt(pow(posX - negX, 2.0) + pow(posY - negY, 2.0));
                if (len != 0) {
                    double Bval = flx / pow((len * lambda), 2.0);
                    double Bval1 = flx / lambda2;
                    beff += Bval;
                    beff_max += Bval1;
                }
            }

            results[0] = beff;
            results[1] = beff_max;
        }

        delete mappings;
        delete[] ds_los;

        return 0;
    }

private:

    /**
     * Transforms the array index position into a Cartesian coordinate
     * position on a grid of specified linear size along x and y
     *
     * The array is expected to be in row major order.
     */
    void indToCart(int idx, int &xi, int &yi, int nCols) {
        yi = idx / nCols;
        xi = idx - (yi * nCols);
    }

};

#endif
