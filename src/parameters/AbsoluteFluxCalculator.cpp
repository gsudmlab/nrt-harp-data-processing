/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * File: AbsoluteFlux.cpp
 * Author: Dustin Kempton
 *
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of parameter modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 *
 * Created on July 27, 2018
 */

#ifndef ABSOLUTEFLUX_CPP
#define ABSOLUTEFLUX_CPP

#include <math.h>

#include "../include/parameters/IParamCalculator.cpp"

/**
 * This class calculates the USFLUX parameter.
 */
class AbsoluteFluxCalculator : public IParamCalculator {

public:

    /**
     * calculateParameter: returns the USFLUX and FLUXIMB parameters
     */
    int calculateParameter(double *los, double *mask, double *bitmap, const double cdelt1, const double rsun_ref,
                           const double rsun_obs, const int nx, const int ny, double *results) {

        if (nx <= 0 || ny <= 0)
            return 0;

        //Calculate how many elements to sum
        const int n = nx * ny;
        //var for holding sum
        double sum = 0.0;
        double sum2 = 0.0;

#pragma omp parallel for reduction(+:sum, sum2)
        for (int i = 0; i < n; i++) {
            if (mask[i] < 70 || bitmap[i] < 30 || isnan(los[i]))
                continue;

            sum += (fabs(los[i]));
            sum2 += los[i];
        }

        double mean_vf = sum * cdelt1 * cdelt1 * (rsun_ref / rsun_obs)
                         * (rsun_ref / rsun_obs) * 100.0 * 100.0;

        results[0] = mean_vf;
        if (sum > 0)
            results[1] = sum2 / sum;
        else
            results[1] = 0;

        return 1;

    }

};

#endif
