/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * @file DownhillClusterPartitioner.cpp
 *
 * @author Dustin Kempton, GSU DMLab
 * Contact: dkempton1@gsu.edu
 *
 * Created on March 4, 2021
 */

#ifndef DOWNHILL_CLUSTER_PARTITIONER_CPP
#define DOWNHILL_CLUSTER_PARTITIONER_CPP

#include <list>
#include <cstdio>
#include <vector>


#include "../include/partition/IClusterPartitioner.cpp"

/**
 * Class that identifies islands of topological features in an image that are distributed around a local maxima.
 * The input image is assumed to have had all values set to positive magnitude values.
 */
class DownhillClusterPartitioner : public IClusterPartitioner {

public:

    DownhillClusterPartitioner() {

    }

    ClusterPartitionMap *process(double *img, int ncols, int nrows) {
        ClusterPartitionMap *result = new ClusterPartitionMap();
        std::vector<std::vector<int> *> *partitions = new std::vector<std::vector<int> *>();
        result->ncols = ncols;
        result->nrows = nrows;
        result->paritions = partitions;

        bool processAgainFlag = false;
        int *takenMask = new int[ncols * nrows]();
        std::list<int> toProcess;
        for (int i = 0; i < ncols * nrows; i++) {
            double val = img[i];
            if (val > 0.0)
                toProcess.push_back(i);
        }

        /**
        * Loop while we keep finding new local maxima to process
        */
        while (true) {
            if (!processAgainFlag) {
                /**
                 * First we find a local maximum value pixel that is not already part of a previous partition, and add
                 * it to a new partition.
                 */
                int flag = this->findWhereValEqualMax(partitions, toProcess, img, takenMask, ncols, nrows);

                /**
                 * If nothing with value greater than zero was found, then quit and return.
                 */
                if (flag == 0) {
                    break;
                }
            }

            /**
             * Group the immediate neighbors of each pixel in the partition if they are smaller
             * values than the value at the pixel of interest and not already part of a partitioned group.
             *
             * The partition at the back of the partitions vector is the one that was most recently constructed
             * and therefore is the one that needs to be expanded.
             */
            std::vector<int> *partition = partitions->back();
            int flag = this->groupNeighborsDownhill(partition, img, takenMask, ncols, nrows);

            /**
             * If values were added to the group, they were also appended to the partition vector and most likely
             * processed as well, but we should give these new values a second pass just in case.
             */
            if (flag == 0) {
                processAgainFlag = true;
            } else {
                /**
                 * If no values were added, then we attempt to find a new local maximum, by setting the flag to false.
                 */
                processAgainFlag = false;
            }

        }
        delete[] takenMask;

        return result;
    }

private:

    /**
     * Groups the neighbors of a value at a specific location in idxList if they are
     * downhill from the value of the magnetic field value at the indexed location.
     * Those that pass the filter are added to the idxList after those that were
     * already present.  Note: img is expected to be positive values as an absolute value
     * mapping should have already been applied to the partition.
     *
     * @param partition A list containing the index in the input image that belongs to the partition.
     *
     * @param img The image of field values
     *
     * @param takenMask A map of pixels that have already been claimed by a cluster.
     *
     * @param nx The number of columns in img.
     *
     * @param ny The number of rows in img.
     *
     * @return Returns zero if neighboring pixel values were found to be added, -1 otherwise.
     */
    int groupNeighborsDownhill(std::vector<int> *partition, double *img, int *takenMask, int nx, int ny) {

        bool found = false;
        /**
         * Loop over all the points that were found be downhill from the original partition values.
         * A maximum of 8 neighbors of the point of interest will be considered for each pixel.
         */
        std::list<int> toProcess(partition->begin(), partition->end());
        int idxNeighbors[8] = {0};

        while (!toProcess.empty()) {
            int idx = toProcess.front();
            toProcess.pop_front();

            /**
             * Check to see if any of the neighbors of the current pixel have not already been claimed
             * by another partition and place them into the neighbors array if they have not.
             */
            int neighbors = this->findUnclaimedNeighbors(takenMask, idxNeighbors, idx, nx, ny);

            /**
             *  If we found some unclaimed neighbors, then check if they are downhill from the current point and add
             *  them to the partition, mark them as taken, and add to list of locations for further processing.
             */
            if (neighbors != 0) {
                double localMaximaVal = img[idx];
                if (localMaximaVal > 0.0) {
                    //Process for positive values
                    for (int j = 0; j < neighbors; j++) {
                        int neighborIdx = idxNeighbors[j];
                        double val = img[neighborIdx];
                        if (val > 0.0 && val <= localMaximaVal) {
                            toProcess.push_back(neighborIdx);
                            partition->push_back(neighborIdx);
                            takenMask[neighborIdx] = 1;
                            found = true;
                        }
                    }
                }
            }
        }

        if (found) {
            return 0;
        }
        else {
            return -1;
        }
    }

    /**
     * Function finds the neighbors of a given index that both the partition mask
     * and the image values are not zero, and returns the indexes of those values and the
     * number found.
     *
     * @param takenMask A map of pixels that have already been claimed by a cluster.
     *
     * @param idxes Array to place index values of the results
     *
     * @param idx The index of the pixel to test the neighbors of
     *
     * @param nx The number of columns in img and takenMask arrays
     *
     * @param ny The number of rows in img and takenMask arrays
     *
     * @return The number of pixels that were found to meet the filter
     */
    int findUnclaimedNeighbors(int *takenMask, int *idxes, int idx, int nx, int ny) {
        int count = 0;

        int col = idx % nx;
        int row = idx / nx;

        //If not the first column, try the pixel to the left
        if (col > 0) {
            int locIdx = idx - 1;
            if (takenMask[locIdx] == 0) {
                idxes[count++] = locIdx;
            }

        }

        //If not the last column, try the pixel to the right
        if (col < nx - 1) {
            int locIdx = idx + 1;
            if (takenMask[locIdx] == 0) {
                idxes[count++] = locIdx;
            }

        }

        //If not the first row
        if (row > 0) {
            //First try the pixel above
            int locIdx = idx - nx;
            if (takenMask[locIdx] == 0) {
                idxes[count++] = locIdx;
            }

            //If not the first column
            if (col > 0) {
                //Then try the pixel above and to the left
                locIdx = idx - nx - 1;
                if (takenMask[locIdx] == 0) {
                    idxes[count++] = locIdx;
                }
            }

            //If not the last column
            if (col < nx - 1) {
                //Then try the pixel above to the right
                locIdx = idx - nx + 1;
                if (takenMask[locIdx] == 0) {
                    idxes[count++] = locIdx;
                }
            }
        }

        //If not the last row
        if (row < ny - 1) {
            //Then try the pixel below
            int locIdx = idx + nx;
            if (takenMask[locIdx] == 0) {
                idxes[count++] = locIdx;
            }

            //If not the first column
            if (col > 0) {
                //Then try the pixel below and to the left
                locIdx = idx + nx - 1;
                if (takenMask[locIdx] == 0) {
                    idxes[count++] = locIdx;
                }
            }

            //If not the last column
            if (col < nx - 1) {
                //Then try the pixel below and to the right
                locIdx = idx + nx + 1;
                if (takenMask[locIdx] == 0) {
                    idxes[count++] = locIdx;
                }
            }
        }

        return count;
    }

    /**
     * Function that searches the entire input image to find a local maxima value that is equal to the maximum value
     * that is not yet in a partition of the image.
     *
     * @param partitions A list of lists, where each of the lists contain the index in the input image that belongs to
     * the partition that the list represents. A new partition will be added to this if an unused local maxima pixel is
     * found.
     *
     * @param toProcess A list of pixel locations that can be considered for processing. They may already be taken, and
     * they shall be removed from the list if this is the case.
     *
     * @param img The image to search for local maxima values on
     *
     * @param takenMask A map of pixels that have already been claimed by a cluster, the positions match that in img.
     *
     * @param nx The size of each row in img and takenMask arrays
     *
     * @param ny The size of each column in img and takenMask arrays
     *
     * @return The number of pixel locations found to meet the criteria
     */
    int findWhereValEqualMax(std::vector<std::vector<int> *> *partitions, std::list<int> &toProcess, double *img,
                             int *takenMask, int nx, int ny) {

        double maxVal = 0.0;
        int idxMaxVal = 0;
        std::list<int>::iterator it = toProcess.begin();
        while (it != toProcess.end()) {
            int idx = (*it);
            //Test if the pixel has been taken by some cluster in another operation.
            if (takenMask[idx] == 0) {
                double val = img[idx];
                if (val > maxVal) {
                    maxVal = val;
                    idxMaxVal = idx;
                }
                it++;
            } else {
                //If the pixel has been used by another partition, there is no need to consider it again.
                it = toProcess.erase(it);
            }
        }

        if (maxVal > 0.0) {
            //If we found a value that wasn't already used, then we add it as a new partition.
            std::vector<int> *initPart = new std::vector<int>();
            initPart->push_back(idxMaxVal);
            partitions->push_back(initPart);
            //Then mark it as taken.
            takenMask[idxMaxVal] = 1;
            return 1;
        }
        return 0;
    }

};

#endif