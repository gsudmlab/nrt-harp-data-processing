/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * @file ClusterPartitioner.cpp
 *
 * @author Dustin Kempton, GSU DMLab
 * Contact: dkempton1@gsu.edu
 *
 * Created on March 4, 2021
 */

#ifndef CLUSTER_PARTITIONER_CPP
#define CLUSTER_PARTITIONER_CPP

#include <vector>
#include <cstdio>
#include <algorithm>

#include "../include/filters/IImageFilter.cpp"
#include "../include/partition/IClusterProcessor.cpp"
#include "../include/partition/IClusterPartitioner.cpp"

/**
 * Class that identifies islands of enhanced signals in an image that are above some threshold. Signals sign is
 * considered when identifying clusters and calculates positive and negative areas separately.
 */
class ClusterPartitioner : public IClusterPartitioner {
    IImageFilter *averageFilter;
    IClusterProcessor *clusterConnector;

    int min_size;

    double posThreshold;
    double negThreshold;

public:
    ClusterPartitioner(IClusterProcessor *clusterConnector, IImageFilter *averageFilter, double bzThreshold,
                       int minSize) {
        this->averageFilter = averageFilter;
        this->clusterConnector = clusterConnector;
        this->posThreshold = bzThreshold;
        this->negThreshold = bzThreshold * (-1);
        this->min_size = minSize;
    }


    ClusterPartitionMap *process(double *img, int ncols, int nrows) {
        ClusterPartitionMap *result = new ClusterPartitionMap();

        //bzFiltered is assigned values for each pixel in the filter, so no need to initialize the values in the array
        double *bzFiltered = new double[ncols * nrows];

        //Cleanup perform an average smoothing operation on the input image.
        this->averageFilter->process(img, bzFiltered, ncols, nrows);

        std::vector<std::vector<int> *> *posPartitions = new std::vector<std::vector<int> *>();
        std::vector<int> *initPosPart = new std::vector<int>();
        posPartitions->push_back(initPosPart);

        std::vector<std::vector<int> *> *negPartitions = new std::vector<std::vector<int> *>();
        std::vector<int> *initNegPart = new std::vector<int>();
        negPartitions->push_back(initNegPart);


        /*
         * Determine which pixels are positive above a threshold value, and which are negative below a threshold value,
         * then place them in their respective initial partitions.
         */
        int n = ncols * nrows;
        for (int idx = 0; idx < n; idx++) {
            double value = bzFiltered[idx];
            if (value > this->posThreshold) {
                initPosPart->push_back(idx);
            } else if (value < this->negThreshold) {
                initNegPart->push_back(idx);
            }
        }
        delete[] bzFiltered;

        if (!initPosPart->empty()) {
            this->clusterConnector->process(posPartitions, ncols, nrows);
        } else {
            posPartitions->pop_back();
            delete initPosPart;
        }


        if (!initNegPart->empty()) {
            this->clusterConnector->process(negPartitions, ncols, nrows);
        } else {
            negPartitions->pop_back();
            delete initNegPart;
        }

        int totalClusters = int(posPartitions->size() + negPartitions->size());

        /**
         * If there are clusters, combine the negative and positive cluster maps into one map. If there are positive
         * clusters then start adding negative clusters at the end of positive count.
         */
        std::vector<std::vector<int> *> *combinedPartitions = new std::vector<std::vector<int> *>();
        if (totalClusters > 0) {
            combinedPartitions->insert(combinedPartitions->end(), posPartitions->begin(), posPartitions->end());
            combinedPartitions->insert(combinedPartitions->end(), negPartitions->begin(), negPartitions->end());
            posPartitions->clear();
            negPartitions->clear();
        }
        delete posPartitions;
        delete negPartitions;

        //If there were no clusters, then return, no need to keep processing.
        if (totalClusters == 0) {
            result->nrows = nrows;
            result->ncols = ncols;
            result->paritions = combinedPartitions;
            return result;
        }



        //Filter to only those clusters that have at least the minimum number of points in them
        std::vector<std::vector<int> *>::iterator it = combinedPartitions->begin();
        while (it != combinedPartitions->end()) {
            std::vector<int> *part = (*it);
            if (int(part->size()) < this->min_size) {
                it = combinedPartitions->erase(it);
                delete part;
            } else {
                it++;
            }
        }

        if (!combinedPartitions->empty())
            this->sortClustersDecending(combinedPartitions);

        result->paritions = combinedPartitions;
        result->ncols = ncols;
        result->ncols = nrows;
        return result;
    }

private:

    /**
     * Method sorts the clusters in order of size, from largest to smallest.
     *
     * @param partitions vector of partitions to be sorted by their size
     *

     */
    void sortClustersDecending(std::vector<std::vector<int> *> *partitions) {
        int numClusts = partitions->size();
        this->quickSortClusters(partitions, 0, numClusts - 1);
        std::reverse(partitions->begin(), partitions->end());
    }

    /**
     * Function implements QuickSort sorts in ascending order
     *
     * @param partitions vector of partitions to be sorted by their size
     *
     * @param low Starting index
     *
     * @param high Ending index
     */
    void quickSortClusters(std::vector<std::vector<int> *> *partitions, int low, int high) {
        if (low < high) {
            int partIdx = this->qsPartition(partitions, low, high);

            this->quickSortClusters(partitions, low, partIdx - 1);
            this->quickSortClusters(partitions, partIdx + 1, high);
        }
    }

    /**
     * This function takes the first element as pivot, places
     * the pivot element at its correct position in sorted
     * array, and places all smaller (smaller than the pivot)
     * to the left of pivot and all greater to the right of pivot.
     *
     * @param partitions A vector of partitions to be sorted by their size
     *
     * @param low The index that this partition starts.
     *
     * @param high The index that this partition ends.
     */
    int qsPartition(std::vector<std::vector<int> *> *partitions, int low, int high) {
        //pivot elements to be placed at correct position
        int pivotCount = partitions->at(high)->size();

        int i = (low - 1); //Index of smaller element
        for (int j = low; j < high; j++) {
            //If current element is smaller than or equal to pivot
            if (int(partitions->at(j)->size()) <= pivotCount) {
                i++; //increment index of smaller element
                //Swap element at i and j
                std::vector<int> *tmpPart = partitions->at(i);
                (*partitions)[i] = partitions->at(j);
                (*partitions)[j] = tmpPart;
            }
        }

        //swap i+1 and high
        std::vector<int> *tmpPart = partitions->at(i + 1);
        (*partitions)[i + 1] = partitions->at(high);
        (*partitions)[high] = tmpPart;

        return (i + 1);
    }

};

#endif