/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * @file ClusterConnector.cpp
 *
 * @author Dustin Kempton, GSU DMLab
 * Contact: dkempton1@gsu.edu
 *
 * Created on March 4, 2021
 */

#ifndef CLUSTER_CONNECTOR_CPP
#define CLUSTER_CONNECTOR_CPP

#include <list>
#include <cstdio>
#include <vector>

#include "../include/partition/IClusterProcessor.cpp"

/**
 * Class that determines if clusters form a continuous structure. If the pixels are
 * not connected, they are treated as yet another cluster to expand to all connected pixels to that
 * found unconnected pixel.
 */
class ClusterConnector : public IClusterProcessor {

public:
    ClusterConnector() {

    }

    void process(std::vector<std::vector < int> * > *partitions,  int ncols,  int nrows  ) {

        if (!partitions->empty()) {
            /**
             * Add all of the pixels from all of the partitions to one list of values.
             */
            std::vector<int> *allPix = new std::vector<int>();
            for (std::vector < std::vector < int > * > ::iterator it = partitions->begin();
                it != partitions->end(); it++) {
                std::vector<int> *part = (*it);
                allPix->insert(allPix->end(), part->begin(), part->end());
                delete part;
            }
            partitions->clear();


            /**
             * Construct a mask of all the pixels that are to be considered for partition membership. Also an empty
             * mask that will be populated as the pixels are added to partitions.
             */
            int *activeMask = new int[ncols * nrows]();
            int *takenMask = new int[ncols * nrows]();
            for (std::vector<int>::iterator it = allPix->begin(); it != allPix->end(); it++) {
                int idx = (*it);
                activeMask[idx] = 1;
            }


            int cls = 0;
            int flag = 0;
            while (true) {
                if (flag == 0) {
                    int clsCode = this->clusterMaker(allPix, partitions, activeMask, takenMask, cls, ncols, nrows);
                    if (clsCode != 0) {
                        break;
                    }
                }
                if (cls > 0) {
                    flag = this->clusterExpander(partitions, activeMask, takenMask, cls, ncols, nrows);
                } else {
                    break;
                }
            }

            delete allPix;
            delete[] activeMask;
            delete[] takenMask;
        }
    }

private:

    /**
     * Method that expands a cluster into unclaimed areas if the active mask indicates the pixel is a cluster candidate,
     * and it is touching a pixel that is claimed by the cluster already.
     *
     * @param partitions A list of lists, where each of the lists contain the index in the input image that belongs to
     * the partition that the list represents. A new partition will be added to this if an unused pixel is found from
     * the allPix list, and will contain the found pixel.
     *
     * @param activeMask The mask that indicates which pixels are allowed to be candidates for being part of a cluster.
     *
     * @param takenMask A map of pixels that have already been claimed by a cluster.
     *
     * @param cls The current cluster number being expanded into unclaimed areas
     *
     * @param nx The number of columns in the input image
     *
     * @param ny The number of rows in the input image
     *
     * @return Zero if no pixels were found to add to the cluster being processed, One if pixels were found.
     */
    int clusterExpander(std::vector<std::vector < int> * > *partitions, int *activeMask, int *takenMask,
                        int cls, int nx, int ny ) {

        bool found = false;
        std::vector<int> *curCluster = partitions->at(cls - 1);

        /**
         * Iterate over all of the pixels within the cluster, check if their neighbors are:
         * 1.) Candidates for adding to the a cluster.
         * 2.) Not already in a cluster.
         * If they meet these criteria, then add them to the cluster for the next iteration.
         */
        std::list<int> toProcess(curCluster->begin(), curCluster->end());
        while (!toProcess.empty()) {
            int idx = toProcess.front();
            toProcess.pop_front();

            int row = idx / nx;
            int col = idx % nx;

            //If the pixel to the right isn't beyond the edge of the image
            if (col < nx - 1 ) {
                int idx1 = idx + 1;
                if (takenMask[idx1] == 0 && activeMask[idx1] != 0) {
                    toProcess.push_back(idx1);
                    curCluster->push_back(idx1);
                    takenMask[idx1] = cls;
                    found = true;
                }
            }

            //If the pixel to the left isn't beyond the edge of the image
            if (col > 0) {
                int idx2 = idx - 1;
                if (takenMask[idx2] == 0 && activeMask[idx2] != 0) {
                    toProcess.push_back(idx2);
                    curCluster->push_back(idx2);
                    takenMask[idx2] = cls;
                    found = true;
                }
            }

            //If the pixel above isn't beyond the edge of the image.
            if (row > 0) {
                int idx3 = idx - nx;
                if (takenMask[idx3] == 0 && activeMask[idx3] != 0) {
                    toProcess.push_back(idx3);
                    curCluster->push_back(idx3);
                    takenMask[idx3] = cls;
                    found = true;
                }
            }

            //If the pixel below isn't beyond the edge of the image.
            if (row < ny - 1) {
                int idx4 = idx + nx;
                if (takenMask[idx4] == 0 && activeMask[idx4] != 0) {
                    toProcess.push_back(idx4);
                    curCluster->push_back(idx4);
                    takenMask[idx4] = cls;
                    found = true;
                }
            }

        }

        if (found == true)
            return 1;

        return 0;
    }

    /**
     * Method that identifies a pixel as being a part of a cluster if the active mask indicates it is a candidate, it
     * hasn't already been claimed as part of a cluster as indicated by the taken mask, and at least one pixel around
     * it is also considered active.
     *
     * @param allPix A list of pixels that are to be tested.
     *
     * @param partitions A list of lists, where each of the lists contain the index in the input image that belongs to
     * the partition that the list represents. A new partition will be added to this if an unused pixel is found from
     * the allPix list, and will contain the found pixel.
     *
     * @param activeMask The mask that indicates which pixels are allowed to be candidates for being part of a cluster.
     *
     * @param takenMask A map of pixels that have already been claimed by a cluster.
     *
     * @param cls The current cluster number, this is by reference so the method increments the count if pixels are
     * found that were not already claimed by another cluster.
     *
     * @param nx The number of columns in the input image
     *
     * @param ny The number of rows in the input image
     *
     * @return Zero if pixels were claimed in this call, -1 if none were found to be unclaimed.
     */
    int
    clusterMaker(std::vector<int> *allPix, std::vector<std::vector < int> * > *partitions, int *activeMask,
                 int *takenMask, int &cls, int nx, int ny ) {

        bool found = false;
        int pixLoc = 0;
        while (!allPix->empty() && !found) {
            int idx = allPix->back();
            allPix->pop_back();
            int row = idx / nx;
            int col = idx % nx;

            //Make sure it is active and is not already taken by a cluster.
            if (activeMask[idx] == 1 && takenMask[idx] == 0) {
                int sum = 0;
                //If the pixel to the right isn't beyond the edge of the image
                int idx1 = idx + 1;
                if (col < nx - 1)
                    sum += activeMask[idx1];

                //If the pixel to the left isn't beyond the edge of the image
                int idx2 = idx - 1;
                if (col > 0)
                    sum += activeMask[idx2];

                //If the pixel above isn't beyond the edge of the image.
                int idx3 = idx - nx;
                if (row > 0)
                    sum += activeMask[idx3];

                //If the pixel below isn't beyond the edge of the image.
                int idx4 = idx + nx;
                if (row < ny - 1)
                    sum += activeMask[idx4];

                if (sum != 0) {
                    std::vector<int> *initPart = new std::vector<int>();
                    initPart->push_back(idx);
                    partitions->push_back(initPart);
                    pixLoc = idx;
                    found = true;
                }
            }
        }


        if (found == true) {
            cls += 1;
            takenMask[pixLoc] = cls;
            return 0;
        } else
            return -1;
    }

};


#endif