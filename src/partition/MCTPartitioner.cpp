/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * @file MCTPartitioner.cpp
 *
 * @author Dustin Kempton, GSU DMLab
 * Contact: dkempton1@gsu.edu
 *
 * Created on June 19, 2020
 */

#ifndef MCT_PARTITIONER_CPP
#define MCT_PARTITIONER_CPP

#include <cstdio>
#include <cmath>
#include <list>

#include "../include/filters/IImageFilter.cpp"
#include "../include/partition/IImagePartitioner.cpp"
#include "../include/partition/ICompositeClusterProcessor.cpp"
#include "../include/partition/IClusterPartitioner.cpp"

/**
 * This class constructs the partitions of magnetic charges for the topology model.  It first partitions positive and
 * negative magnetic field clusters.  It then finds local maxima within each of the partitions and verifies that
 * all of the values are "downhill" from that point.  If not, then those points that are not "downhill" are split
 * into their own partitions and the downhill test is performed for them. Then, if any of these partitions are
 * entirely surrounded by another partition, they are swallowed back up by the surrounding partition. Then partitions
 * that are either too small physically or have too little flux are removed.  Finally the found partitions are returned.
 */
class MCTPartitioner : public IImagePartitioner {

    IImageFilter *averageFilter;

    IClusterPartitioner *clusterPartitioner;
    IClusterPartitioner *downhillClusterPartitioner;

    ICompositeClusterProcessor *islandSwallower;
    ICompositeClusterProcessor *clusterCorrector;
    ICompositeClusterProcessor *smallFluxStructureRemover;

    int minSize;


public:

    /**
     * The constructor
     * @param clusterPartitioner Object that performs initial partitioning of the image into positive and negative
     * magnetic flux partitions.
     *
     * @param downhillClusterPartitioner Object that verifies that all values within a partition are downhill from some
     * local maxima and splits partitions that don't conform.
     *
     * @param islandSwallower Object that checks to see if a partition is completely surrounded by another and
     * merges the two if this is the case
     *
     * @param smallFluxStructureRemover Object that removes the partitions that don't meet some minimum flux amount
     *
     * @param clusterCorrector Object that fixes partitions that were split into more than one piece by other operations
     *
     * @param averageFilter Object that performs average smoothing on the input image.
     *
     * @param minSize The minimum number of pixels that a partition must have to be considered valid
     */
    MCTPartitioner(IClusterPartitioner *clusterPartitioner, IClusterPartitioner *downhillClusterPartitioner,
                   ICompositeClusterProcessor *islandSwallower, ICompositeClusterProcessor *smallFluxStructureRemover,
                   ICompositeClusterProcessor *clusterCorrector, IImageFilter *averageFilter, int minSize) {
        this->clusterPartitioner = clusterPartitioner;
        this->downhillClusterPartitioner = downhillClusterPartitioner;
        this->islandSwallower = islandSwallower;
        this->smallFluxStructureRemover = smallFluxStructureRemover;
        this->clusterCorrector = clusterCorrector;
        this->averageFilter = averageFilter;
        this->minSize = minSize;

    }

    PartitionMap *process(double *img, int ncols, int nrows, double cdelt1) {

        /*
         * Start by getting clusters of positive and negative magnetic field values.
         */
        ClusterPartitionMap *clustMap = this->clusterPartitioner->process(img, ncols, nrows);
        std::vector<std::vector<int> *> *combinedPartitions = clustMap->paritions;
        delete clustMap;

        if (combinedPartitions->empty()) {
            PartitionMap *map = new PartitionMap();
            map->partitions = combinedPartitions;
            map->nrows = nrows;
            map->ncols = ncols;
            return map;
        }

        double *bzFiltered = new double[ncols * nrows];

        /*
         * Cleanup image by performing an average smoothing operation on the input image.
         */
        this->averageFilter->process(img, bzFiltered, ncols, nrows);


        /*
         * With the first-cut of partitions performed, we look within each partition to find possible sub-patterns
         * using the bz values
         */
        std::list<std::vector<int> *> workingList(combinedPartitions->begin(), combinedPartitions->end());
        combinedPartitions->clear();

        for (std::list<std::vector<int> *>::iterator it = workingList.begin(); it != workingList.end();) {
            /*
             * Extract the partition that is at the current location in the working list.
             */
            std::vector<int> *part = (*it);
            if (!part->empty()) {
                AbsPartMask *pm = this->absValMaskPartition(bzFiltered, part, ncols, nrows);;

                int pmNx = pm->nx;
                int pmNy = pm->ny;
                int pmMinx = pm->xmin;
                int pmMiny = pm->ymin;
                double *bzs = pm->absPart;
                delete pm;

                /*
                 * Find the sub-structures within the partition using the local maxima and values down hill from those
                 * maxima locations.
                 */
                ClusterPartitionMap *dhPartMask = this->downhillClusterPartitioner->process(bzs, pmNx, pmNy);

                std::vector<std::vector<int> *> *dhPartitions = dhPartMask->paritions;
                int dhPartNum = dhPartitions->size();
                delete dhPartMask;
                delete[] bzs;

                if (dhPartNum > 1) {
                    /*
                     * If some sub-structures were found, then we need to remove the original partition being processed this
                     * iteration, and insert the new sub-partitions in its place.
                     */
                    it = workingList.erase(it);
                    delete part;
                    for (std::vector<std::vector<int> *>::iterator it2 = dhPartitions->begin();
                         it2 != dhPartitions->end(); it2++) {

                        std::vector<int> *tmpPart = (*it2);
                        std::vector<int> *newPart = new std::vector<int>();
                        for (std::vector<int>::iterator it3 = tmpPart->begin(); it3 != tmpPart->end(); it3++) {
                            int idx = (*it3);
                            int xPart = (idx % pmNx) + pmMinx;
                            int yPart = (idx / pmNx) + pmMiny;
                            idx = yPart * ncols + xPart;
                            newPart->push_back(idx);
                        }
                        it = workingList.insert(it, newPart);
                        it++;
                        delete tmpPart;
                    }
                } else {
                    /*
                     * If only one structure was found, then we don't need to do anything as it was already in the original
                     * partition list. We just delete the new partition and move on.
                     */
                    for (std::vector<std::vector<int> *>::iterator it2 = dhPartitions->begin();
                         it2 != dhPartitions->end(); it2++) {
                        std::vector<int> *tmpPart = (*it2);
                        delete tmpPart;
                    }
                    it++;
                }
                dhPartitions->clear();
                delete dhPartitions;
            } else {
                it = workingList.erase(it);
                delete part;
            }
        }
        combinedPartitions->insert(combinedPartitions->begin(), workingList.begin(), workingList.end());

        /*
         * Manipulation now begins. The following corrections are applied:
         * (1) Eliminate "islands" of different labels fully contained by nonzero areas
         * (2) Attempt to fix clusters that may have been broken apart
         * (3) Throw away structures with flux smaller that the minimum allowed.
         * (4) Throw away structures with size smaller than the minimum allowed.
         */
        //(1) Island swollowing
        this->islandSwallower->process(bzFiltered, combinedPartitions, ncols, nrows, cdelt1);

        /*
         * (2) Perform correction for clusters that may have been split in to more than
         * one piece.
         */
        this->clusterCorrector->process(bzFiltered, combinedPartitions, ncols, nrows, cdelt1);

        /*
         * (3) Remove structures with magnetic flux less than minFlux
         */
        this->smallFluxStructureRemover->process(bzFiltered, combinedPartitions, ncols, nrows, cdelt1);
        delete[] bzFiltered;

        /*
         * (4) Remove structures with small physical size
         */
        for (std::vector<std::vector<int> *>::iterator it = combinedPartitions->begin();
             it != combinedPartitions->end();) {
            std::vector<int> *part = (*it);

            if (int(part->size()) < this->minSize) {
                it = combinedPartitions->erase(it);
                delete part;
            } else {
                it++;
            }
        }


        PartitionMap *map = new PartitionMap();
        map->partitions = combinedPartitions;
        map->nrows = nrows;
        map->ncols = ncols;
        return map;
    }

private:


    struct AbsPartMask {
        /**
         * @param absPart The the absolute values map of BZ values partitioned from the full BZ map.
         */
        double *absPart;

        /**
         * @param nx The size of the partition along the column axis
         */
        int nx;

        /**
         * @param ny The size of the partition along the row axis
         */
        int ny;

        /**
         * @param xmin The column starting position of the partition in the original full BZ map.
         */
        int xmin;

        /**
         * @param ymin The row starting position of the partition in the original full BZ map.
         */
        int ymin;
    };

    /**
     * Partition out a specific partition from bz using the partition as the
     * map of which values to include. The values partitioned out have had
     * the absolute value taken at each pixel so that the downhill gradient
     * function can be applied without needing to be concerned about sign.
     *
     * @param bz The input magnetic field data to partition from
     *
     * @param partition List of pixels that belong to the partition that is to be cut from the input magnetic field data
     *
     * @param nx The row size of bz
     *
     * @param ny The column size of bz
     *
     *
     * @return The partitioned out sub-region of interest, including variables for the size of each dimension
     */
    AbsPartMask *absValMaskPartition(double *bz, std::vector<int> *partition, int nx, int ny) {

        /**
         * First we want to identify the min and max index of the areas we are going to partition out.
         */
        int minCol = nx;
        int maxCol = 0;
        int minRow = ny;
        int maxRow = 0;

        for (std::vector<int>::iterator it = partition->begin(); it != partition->end(); it++) {
            int idx = (*it);
            int col = idx % nx;
            int row = idx / nx;

            if (minCol > col) {
                minCol = col;
            }

            if (maxCol < col) {
                maxCol = col;
            }

            if (minRow > row) {
                minRow = row;
            }

            if (maxRow < row) {
                maxRow = row;
            }
        }

        /**
         * Next we construct a map of the abs bz values within the partition we are processing, just large enough
         * to contain the partition.
         */
        int nxPart = maxCol - minCol + 1;
        int nyPart = maxRow - minRow + 1;
        double *part = new double[nxPart * nyPart]();
        for (std::vector<int>::iterator it = partition->begin(); it != partition->end(); it++) {
            int idx = (*it);
            int col = idx % nx;
            int row = idx / nx;
            int idx2 = (row - minRow) * nxPart + (col - minCol);
            part[idx2] = abs(bz[idx]);
        }

        AbsPartMask *result = new AbsPartMask();
        result->nx = nxPart;
        result->ny = nyPart;
        result->xmin = minCol;
        result->ymin = minRow;
        result->absPart = part;
        return result;
    }
};

#endif
