/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * @file SmallFluxStructureRemover.cpp
 *
 * @author Dustin Kempton, GSU DMLab
 * Contact: dkempton1@gsu.edu
 *
 * Created on March 19, 2021
 */

#ifndef SMALL_FLUX_STRUCTURE_REMOVER_CPP
#define SMALL_FLUX_STRUCTURE_REMOVER_CPP

#include <cmath>
#include <vector>

#include "../include/partition/ICompositeClusterProcessor.cpp"

/**
 * Class that identifies partitions that don't conform to a rule of having a minimum amount of flux included within them.
 */
class SmallFluxStructureRemover : public ICompositeClusterProcessor {

    double minFlux;
    double lenPerArcSec;

public:

    SmallFluxStructureRemover(double minFlux, double lenPerArcSec) {
        this->minFlux = minFlux;
        this->lenPerArcSec = lenPerArcSec;
    }

    void process(double *img, std::vector<std::vector<int> *> *partitions, int ncols, int nrows, double cdelt) {

        double lambda = this->lenPerArcSec * (cdelt / 3600);
        double lambda2 = pow(lambda, 2.0);
        /*
         * Loop over each partition and calculate the sum of the absolute value of magnetic field values within each
         * partition, deleting those that don't meet the threshold.
         */
        for (std::vector<std::vector<int> *>::iterator it = partitions->begin(); it != partitions->end();) {
            std::vector<int> *part = (*it);
            double flxSum = 0;
            for (std::vector<int>::iterator it2 = part->begin(); it2 != part->end(); it2++) {
                int idx = (*it2);
                flxSum += abs(img[idx]);
            }
            flxSum *= lambda2;

            if (flxSum < this->minFlux) {
                it = partitions->erase(it);
                delete part;
            } else {
                it++;
            }
        }
    }

};

#endif