/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * @file ClusterCorrector.cpp
 *
 * @author Dustin Kempton, GSU DMLab
 * Contact: dkempton1@gsu.edu
 *
 * Created on March 19, 2021
 */

#ifndef CLUSTER_CORRECTOR_CPP
#define CLUSTER_CORRECTOR_CPP

#include <list>
#include <cstdio>
#include <vector>

#include "../include/partition/ICompositeClusterProcessor.cpp"
#include "../include/partition/IClusterProcessor.cpp"

/**
 * Class that identifies partitions that have been split into more than one continuous partition
 * and either removes the small pieces or adds them as another cluster if they meet some threshold.
 */
class ClusterCorrector : public ICompositeClusterProcessor {

    ICompositeClusterProcessor *smallFluxStructureRemover;
    IClusterProcessor *clusterConnector;
    int minSize;


public:

    ClusterCorrector(IClusterProcessor *clusterConnector, ICompositeClusterProcessor *smallFluxStructureRemover,
                     int minSize) {
        this->clusterConnector = clusterConnector;
        this->smallFluxStructureRemover = smallFluxStructureRemover;
        this->minSize = minSize;
    }

    void process(double *img, std::vector<std::vector < int> *> *partitions, int ncols, int nrows,
                 double cdelt ) {

        bool changed = false;
        std::list<std::vector<int>*> workingList(partitions->begin(), partitions->end());
        for (std::list<std::vector<int>*>::iterator it = workingList.begin(); it != workingList.end();) {
            std::vector<int> *part = (*it);

            std::vector < std::vector < int > * > *tmpPartitions = new std::vector < std::vector < int > * > ();
            std::vector<int> *tmpPart = new std::vector<int>(part->begin(), part->end());
            tmpPartitions->push_back(tmpPart);

            this->clusterConnector->process(tmpPartitions, ncols, nrows);

            /*
             * If there are more than two clusters, then we process them to either:
             * 1) Add those beyond the original one as a new cluster(s) if it meets
             * minimum requirements for being a cluster
             * 2) Delete it all together if it doesn't meet the minimum requirements
             * for being a cluster
             */
            if (tmpPartitions->size() > 1) {
                /*
                 * First remove those partitions with too small of flux.
                 */
                this->smallFluxStructureRemover->process(img, tmpPartitions, ncols, nrows, cdelt);

                /*
                 * Then remove those partitions that don't meet a minimum size.
                 */
                for (std::vector < std::vector < int > * > ::iterator it2 = tmpPartitions->begin();
                        it2 != tmpPartitions->end();) {

                    std::vector<int> *part2 = (*it2);
                    if (int(part2->size()) < this->minSize) {
                        it2 = tmpPartitions->erase(it2);
                        //printf("%s \n", "Delete part too small.");
                        //No need to delete part2 erase destroys the element from the vector
                    } else {
                        it2++;
                    }
                }

                /*
                 * Remove the original partition from our list of partitions, and insert the sub-partitions
                 * into the partitions list.
                 */
                it = workingList.erase(it);
                delete part;

                if (!tmpPartitions->empty()) {
                    it = workingList.insert(it, tmpPartitions->begin(), tmpPartitions->end());
                    it = std::next(it, tmpPartitions->size());
                }
                changed = true;
            } else {
                for (std::vector < std::vector < int > * > ::iterator it2 = tmpPartitions->begin();
                        it2 != tmpPartitions->end(); it2++) {
                    std::vector<int> *part2 = (*it2);
                    delete part2;
                }
                it++;
            }
            delete tmpPartitions;
        }

        if (changed) {
            partitions->clear();
            partitions->insert(partitions->begin(), workingList.begin(), workingList.end());
            this->process(img, partitions, ncols, nrows, cdelt);
        }
    }

};

#endif