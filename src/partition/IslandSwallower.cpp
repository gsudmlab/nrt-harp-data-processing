/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * @file IslandSwallower.cpp
 *
 * @author Dustin Kempton, GSU DMLab
 * Contact: dkempton1@gsu.edu
 *
 * Created on March 5, 2021
 */

#ifndef ISLAND_SWALLOWER_CPP
#define ISLAND_SWALLOWER_CPP

#include <list>
#include <vector>
#include <algorithm>

#include "../include/partition/ICompositeClusterProcessor.cpp"

/**
 * Class that identifies partitions that are surrounded by other partitions (an island) and reassigns pixels from the
 * island partition to the surrounding partitions.
 */
class IslandSwallower : public ICompositeClusterProcessor {

public:
    IslandSwallower() {

    }

    void process(double *img, std::vector<std::vector<int> *> *partitions, int ncols, int nrows, double cdelt) {

        /*
         * Construct the part mask using the vector of partitions, where each pixel belonging to a partition
         * is labeled with an identifier for that partition.
         */
        int *posPartMask = new int[ncols * nrows]();
        int *negPartMask = new int[ncols * nrows]();
        int posPartNum = 1;
        int negPartNum = 1;
        std::vector<std::vector<int> *> *posParts = new std::vector<std::vector<int> *>();
        std::vector<std::vector<int> *> *negParts = new std::vector<std::vector<int> *>();
        for (std::vector<std::vector<int> *>::iterator it = partitions->begin(); it != partitions->end(); it++) {
            std::vector<int> *part = (*it);
            if (img[part->front()] > 0) {
                for (std::vector<int>::iterator it2 = part->begin(); it2 != part->end(); it2++) {
                    int idx = (*it2);
                    posPartMask[idx] = posPartNum;
                }
                posParts->push_back(part);
                posPartNum++;
            } else {
                for (std::vector<int>::iterator it2 = part->begin(); it2 != part->end(); it2++) {
                    int idx = (*it2);
                    negPartMask[idx] = negPartNum;
                }
                negParts->push_back(part);
                negPartNum++;
            }
        }

        this->redoIslands(img, posPartMask, posParts, ncols, nrows);
        this->redoIslands(img, negPartMask, negParts, ncols, nrows);
        this->deleteEmptyParts(partitions);

        delete[] posPartMask;
        delete[] negPartMask;
        delete posParts;
        delete negParts;
    }

private:

    void deleteEmptyParts(std::vector<std::vector<int> *> *partitions) {
        for (std::vector<std::vector<int> *>::iterator it = partitions->begin(); it != partitions->end();) {
            std::vector<int> *part = (*it);
            if (part->empty()) {
                it = partitions->erase(it);
                delete part;
            } else {
                it++;
            }
        }
    }

    /**
     * Method that identifies partitions that are surrounded by other partitions (an island), and recursively reassigns
     * the edge pixels of the partition to the surrounding partitions. It also enforces that the adjacent partition
     * pixel is non-zero in the original input image.
     *
     * @param img The original input image that the partitions were constructed from.
     *
     * @param partMask A mask that has each pixel within the partitions labeled as being part of the corresponding
     * partition.
     *
     * @param partitions A list of partitions, where the partitions are lists of pixel indexes within the original image
     * and the partMask. This list gets edited, moving pixels from one list to another. Lists may have zero size after
     * execution, so post processing to remove these should be done.
     *
     * @param ncols The number of columns in the original image
     *
     * @param nrows The number of rows in the original image
     */
    void redoIslands(double *img, int *partMask, std::vector<std::vector<int> *> *partitions, int ncols,
                     int nrows) {

        bool changesMade = false;
        for (std::vector<std::vector<int> *>::iterator it = partitions->begin(); it != partitions->end(); it++) {
            std::vector<int> *part = (*it);
            /*
             * A partition may have had all of its pixels removed by previous calls to this method. We don't remove the
             * partition from the list of partitions because we would have to redo the partMask. This is a faster way
             * to deal with that. We just make sure to remove the zero length partitions after this method is done.
             */
            if (!part->empty()) {
                IslandBoundMask *ism = this->findBoundaries(part, partMask, ncols, nrows);
                std::vector<int> *partitionEdgePixels = ism->partitionEdgePixels;
                std::vector<int> *edgeAdjacentPixels = ism->edgeAdjacentPixels;
                delete ism;

                /*
                 * Make sure the partition surrounded by other partitions.
                 */
                bool foundZero = false;
                for (std::vector<int>::iterator it2 = edgeAdjacentPixels->begin();
                it2 != edgeAdjacentPixels->end(); it2++) {
                    int idx = (*it2);
                    int val = partMask[idx];
                    if (val == 0) {
                        foundZero = true;
                        break;
                    }
                }

                if (!foundZero) {
                    /*
                     * If we didn't find that the partition was touching an unclaimed part of the image, then it means
                     * this partition is completely surrounded by other partitions. So, we want to have this partition
                     * swallowed by the surrounding partitions. This will be done slowly, by only doing the edge pixels
                     * on each pass.
                     */
                    do {
                        if (!partitionEdgePixels->empty() && !edgeAdjacentPixels->empty()) {
                            for (std::vector<int>::iterator it2 = partitionEdgePixels->begin();
                            it2 != partitionEdgePixels->end(); it2++) {
                                int idx = (*it2);
                                int label = partMask[idx];
                                int row = idx / ncols;
                                int col = idx % ncols;

                                if (row > 0) {
                                    int idx2 = idx - ncols;
                                    int val = partMask[idx2];
                                    if (val != label && val > 0 && (img[idx] * img[idx2] > 0.0)) {
                                        partMask[idx] = val;
                                        part->erase(std::remove(part->begin(), part->end(), idx), part->end());
                                        std::vector<int> *newPart = (*partitions)[val - 1];
                                        newPart->push_back(idx);
                                        changesMade = true;
                                        continue;
                                    }
                                }

                                if (row < nrows - 1) {
                                    int idx2 = idx + ncols;
                                    int val = partMask[idx2];
                                    if (val != label && val > 0 && (img[idx] * img[idx2] > 0.0)) {
                                        partMask[idx] = val;
                                        part->erase(std::remove(part->begin(), part->end(), idx), part->end());
                                        std::vector<int> *newPart = (*partitions)[val - 1];
                                        newPart->push_back(idx);
                                        changesMade = true;
                                        continue;
                                    }
                                }

                                if (col > 0) {
                                    int idx2 = idx - 1;
                                    int val = partMask[idx2];
                                    if (val != label && val > 0 && (img[idx] * img[idx2] > 0.0)) {
                                        partMask[idx] = val;
                                        part->erase(std::remove(part->begin(), part->end(), idx), part->end());
                                        std::vector<int> *newPart = (*partitions)[val - 1];
                                        newPart->push_back(idx);
                                        changesMade = true;
                                        continue;
                                    }
                                }

                                if (col < ncols - 1) {
                                    int idx2 = idx + 1;
                                    int val = partMask[idx2];
                                    if (val != label && val > 0 && (img[idx] * img[idx2] > 0.0)) {
                                        partMask[idx] = val;
                                        part->erase(std::remove(part->begin(), part->end(), idx), part->end());
                                        std::vector<int> *newPart = (*partitions)[val - 1];
                                        newPart->push_back(idx);
                                        changesMade = true;
                                        continue;
                                    }
                                }

                                /*
                                 * If none of the conditions above were met then we need to remove the index from the part
                                 * without assigning it to any other partition. This should not happen because only the
                                 * edge pixels should be processed in this loop, but just in case.
                                 */
                                part->erase(std::remove(part->begin(), part->end(), idx), part->end());
                            }
                        } else {
                            part->clear();
                        }

                        /*
                         * Reprocess the edges for the next do loop iteration.
                         */
                        if (!part->empty()) {
                            delete partitionEdgePixels;
                            delete edgeAdjacentPixels;
                            ism = this->findBoundaries(part, partMask, ncols, nrows);

                            partitionEdgePixels = ism->partitionEdgePixels;
                            edgeAdjacentPixels = ism->edgeAdjacentPixels;
                            delete ism;
                        }
                    } while (!part->empty());
                }

                delete partitionEdgePixels;
                delete edgeAdjacentPixels;
            }
            if (changesMade) break;
        }


        //If parts of any partition were swallowed by another then run again.
        if (changesMade) {
            changesMade = false;
            for (std::vector<std::vector<int> *>::iterator it = partitions->begin(); it != partitions->end();) {
                std::vector<int> *part = (*it);
                if (part->empty()) {
                    it = partitions->erase(it);
                    changesMade = true;
                } else {
                    it++;
                }
            }

            if (changesMade) {
                for (int i = 0; i < ncols * nrows; i++) {
                    partMask[i] = 0;
                }
                int partNum = 1;
                for (std::vector<std::vector<int> *>::iterator it = partitions->begin(); it != partitions->end(); it++) {
                    std::vector<int> *part = (*it);
                    for (std::vector<int>::iterator it2 = part->begin(); it2 != part->end(); it2++) {
                        int idx = (*it2);
                        partMask[idx] = partNum;
                    }
                    partNum++;
                }
            }

            this->redoIslands(img, partMask, partitions, ncols, nrows);
        }
    }

    struct IslandBoundMask {
        /**
         * @param partitionEdgePixels A list of index values for the boundary of a partition.
         */
        std::vector<int> *partitionEdgePixels;

        /**
         * @param edgeAdjacentPixels A list of index values for the pixels adjacent to the pixels in partitionEdgePixels.
         */
        std::vector<int> *edgeAdjacentPixels;
    };

    /**
     * A method that finds the edge pixels of a partition. It also identifies the pixels that are adjacent to the edge
     * pixels of the partition.
     *
     * @param partition List of points within a partition.
     *
     * @param partMask An array that maps the location of all the different partitions that are in consideration at
     * the time of this method having been called.
     *
     * @param ncols Number of columns in the partMask
     *
     * @param nrows Number of rows in the partMask
     *
     * @return A structure containing two vectors, 1.) The indexes of edge pixels of the input partition, 2.) The
     * indexes of pixels adjacent to the edge pixels of the input partition.
     */
    IslandBoundMask *findBoundaries(std::vector<int> *partition, int *partMask, int ncols, int nrows) {


        int count = partition->size();
        std::vector<int> *edgeAdjacentPixels = new std::vector<int>();
        std::vector<int> *partitionEdgePixels = new std::vector<int>();

        if (count > 0) {
            /*
             *  Since the partMask should be labeled with the pixels that fall within each partition, pulling the
             *  first pixel from this partition should give the label used for this partition.
             */
            int label = partMask[partition->front()];

            /*
             * If there are more than 4 pixels in the partition, then find the edge pixels of the area of interest.
             */
            if (count > 4) {
                for (std::vector<int>::iterator it = partition->begin(); it != partition->end(); it++) {
                    int idx = (*it);
                    int row = idx / ncols;
                    int col = idx % ncols;

                    if (row > 0) {
                        int val = partMask[idx - ncols];
                        if (val != label) {
                            partitionEdgePixels->push_back(idx);
                            continue;
                        }
                    }

                    if (row < nrows - 1) {
                        int val = partMask[idx + ncols];
                        if (val != label) {
                            partitionEdgePixels->push_back(idx);
                            continue;
                        }
                    }

                    if (col > 0) {
                        int val = partMask[idx - 1];
                        if (val != label) {
                            partitionEdgePixels->push_back(idx);
                            continue;
                        }
                    }

                    if (col < ncols - 1) {
                        int val = partMask[idx + 1];
                        if (val != label) {
                            partitionEdgePixels->push_back(idx);
                            continue;
                        }
                    }
                }
            } else {
                //Else just add all the partition pixels to the edge list.
                partitionEdgePixels->insert(partitionEdgePixels->end(), partition->begin(), partition->end());
            }



            /*
            * Construct a list of pixel indexes that are adjacent to the edge pixel of the partition, and are not part of
             * the partition.
             */

            //Temporary mask used to indicate when a pixel is already present in the boundary list.
            double *tmask = new double[ncols * nrows]();
            for (std::vector<int>::iterator it = partitionEdgePixels->begin(); it != partitionEdgePixels->end(); it++) {
                int idx = (*it);
                int row = idx / ncols;
                int col = idx % ncols;

                if (row > 0) {
                    int idx2 = idx - ncols;
                    int val = partMask[idx2];
                    if (val != label && tmask[idx2] == 0) {
                        edgeAdjacentPixels->push_back(idx2);
                        tmask[idx2] = 1;
                    }
                }

                if (row < nrows - 1) {
                    int idx2 = idx + ncols;
                    int val = partMask[idx2];
                    if (val != label && tmask[idx2] == 0) {
                        edgeAdjacentPixels->push_back(idx2);
                        tmask[idx2] = 1;
                    }
                }

                if (col > 0) {
                    int idx2 = idx - 1;
                    int val = partMask[idx2];
                    if (val != label && tmask[idx2] == 0) {
                        edgeAdjacentPixels->push_back(idx2);
                        tmask[idx2] = 1;
                    }
                }

                if (col < ncols - 1) {
                    int idx2 = idx + 1;
                    int val = partMask[idx2];
                    if (val != label && tmask[idx2] == 0) {
                        edgeAdjacentPixels->push_back(idx2);
                        tmask[idx2] = 1;
                    }
                }
            }
            delete[] tmask;

        }
        IslandBoundMask *maskResult = new IslandBoundMask();
        maskResult->partitionEdgePixels = partitionEdgePixels;
        maskResult->edgeAdjacentPixels = edgeAdjacentPixels;

        return maskResult;
    }
};

#endif