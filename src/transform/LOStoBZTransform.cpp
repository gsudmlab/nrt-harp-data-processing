/*
* SEP-Mag-Feature-Processor, a project at the Data Mining Lab
* (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
*
* Copyright (C) 2021 Georgia State University
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation version 3.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * File: LOSTransform.cpp
 * Author: Dustin Kempton
 *
 *
 * This class is meant to perform a line of sight conversion to radial magnetic field
 * for the LOS Magnetogram.
 *
 *
 * Created on October 04, 2018
 */

#ifndef LOSTRANS_OMP_CPP
#define LOSTRANS_OMP_CPP

#include <math.h>

class LOStoBZTransform {

private:
	static constexpr double RADS_IN_DEG =
			(3.14159265358979323846264338327950288) / 180.0;
	double rsun_ref = 6.96e8;
	static constexpr double RADS_2_DEG = 180.0
			/ (3.14159265358979323846264338327950288);
	static constexpr double mPI = (3.14159265358979323846264338327950288);
	static constexpr double mPI_2 = (3.14159265358979323846264338327950288)
			/ 2.0;
public:

	double* process(double *image, double crln_obs,
			 double cdelt1, double dsun_obs, int nx, int ny,
			 double crval1, double crval2) {


		double globalLon = (crval1 - crln_obs) * RADS_IN_DEG;
		double globalLat = crval2 * RADS_IN_DEG;

		double xscale0 = cdelt1 * RADS_IN_DEG; //resolution
		double yscale0 = cdelt1 * RADS_IN_DEG; // in rad

		double *correctedImg = new double[nx * ny];
#pragma omp parallel for
		for (int j = 0; j < ny; j++) {
			for (int i = 0; i < nx; i++) {

				double x = (i + 0.5 - nx / 2.0) * xscale0; // in rad
				double y = (j + 0.5 - ny / 2.0) * yscale0;

				double localLat, localLon;
				this->plane2sphere(x, y, globalLat, globalLon, &localLat,
						&localLon);

				//The compiler of openACC doesn't like nested function calls.
				//So, we will just break them apart here.
				double sinLocLat = sin(localLat);
				double powSinLocLat = pow(sinLocLat, 2.0);
				double cosLocLat = cos(localLat);
				double sinLocLon = sin(localLon);
				double powCosSinLatLon = pow((cosLocLat * sinLocLon), 2.0);
				double d = sqrt(powSinLocLat + powCosSinLatLon);
				double asinD = asin(d);
				double atanDRsun = atan(d * rsun_ref / dsun_obs);
				double losCorrection = cos(asinD + atanDRsun);

				//double losCorrection = cos(localLon) / cos(localLat);
				correctedImg[j * nx + i] = image[j * nx + i] / losCorrection;
			}
		}

		return correctedImg;
	}

private:

	int plane2sphere(double x, double y, double latc, double lonc, double *lat,
			double *lon) {
		/*
		 *  Perform the inverse mapping from rectangular coordinates x, y on a map
		 *    in a particular projection to heliographic (or geographic) coordinates
		 *    latitude and longitude (in radians).
		 *  The map coordinates are first transformed into arc and azimuth coordinates
		 *    relative to the center of the map according to the appropriate inverse
		 *    transformation for the projection, and thence to latitude and longitude
		 *    from the known heliographic coordinates of the map center (in radians).
		 *  The scale of the map coordinates is assumed to be in units of radians at
		 *    the map center (or other appropriate location of minimum distortion).
		 *
		 *  Arguments:
		 *      x }         Map coordinates, in units of radians at the scale
		 *      y }           appropriate to the map center
		 *      latc        Latitude of the map center (in radians)
		 *      lonc        Longitude of the map center (in radians)
		 *      *lat        Returned latitude (in radians)
		 *      *lon        Returned longitude (in radians)
		 *
		 *  The function returns -1 if the requested point on the map does not project
		 *    back to the sphere or is not a principal value, 1 if it projects to a
		 *    point on a hidden hemisphere (if that makes sense), 0 otherwise
		 */

		int status = 0;

		double latc0 = 0.0, sinlatc = 0.0, coslatc = 1.0;
		if (latc != latc0) {
			coslatc = cos(latc);
			sinlatc = sin(latc);
		}
		latc0 = latc;

		if (fabs(y) > 1.0) {
			y = copysign(1.0, y);
			status = -1;
		}

		double cosphi = sqrt(1.0 - y * y);
		*lat = asin((y * coslatc) + (cosphi * cos(x) * sinlatc));
		double test = (cos(*lat) == 0.0) ? 0.0 : cosphi * sin(x) / cos(*lat);
		*lon = asin(test) + lonc;
		if (fabs(x) > mPI_2) {
			status = 1;
			while (x > mPI_2) {
				*lon = mPI - *lon;
				x -= mPI;
			}
			while (x < -mPI_2) {
				*lon = -mPI - *lon;
				x += mPI;
			}
		}
		if (arc_distance(*lat, *lon, latc, lonc) > mPI_2)
			status = 1;
		return status;
	}

	static double arc_distance(double lat, double lon, double latc,
			double lonc) {
		double cosa = sin(lat) * sin(latc)
				+ cos(lat) * cos(latc) * cos(lon - lonc);
		return acos(cosa);
	}

};

#endif
