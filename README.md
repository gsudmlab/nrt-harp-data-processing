# NRT-HARP-Processor

* [INFO](#markdown-header-info)
* [TEST BUILD](#markdown-header-test-build)
* [CONFIGURATION](#markdown-header-configuration)

****
***

## INFO

The NRT-HARP-Processor project is one part of a larger microservice system. This process downloads and processes near
real time (NRT) HMI Active Region Patch (HARP) data from NASA's Joint Science Operations Center (JSOC) at Stanford.
Several different magnetic field parameters are calculated for each active region at each time step that data is
downloaded. When started, the process will ensure to download the data for each active region over the last 24 hours. 

The required libraries are listed in the `requirements.txt` file, but the setup process in
the [TEST_BUILD.md](TEST_BUILD.md) file should be followed for proper operation.

## TEST Build

The build section is only for building and running on an individual basis for testing. If deploying the entire system,
go to the main deployment process utilizing `docker-compose`, see the
main [deployment project](https://bitbucket.org/gsudmlab/isepcleardeploy).

If you wish to continue to run just the local project, continue to [TEST_BUILD.md](TEST_BUILD.md)

***

## CONFIGURATION

The default config file for this process is config.ini. It is copied into the image at build time.

A full listing of the lines lines in the config file and what they control are listed in the [CONFIG.md](CONFIG.md)
file.

***
***

## Acknowledgment

This work was supported in part by NASA Grant Award No. NNH14ZDA001N, NASA/SRAG Direct Contract and two NSF Grant
Awards: No. AC1443061 and AC1931555.

***

This software is distributed using the [GNU General Public License, Version 3](./LICENSE.txt)

![GPLv3](./images/gplv3-88x31.png)

***

© 2023 Dustin Kempton, Berkay Aydin, Rafal Angryk

[Data Mining Lab](https://dmlab.cs.gsu.edu/)

[Georgia State University](https://www.gsu.edu/)