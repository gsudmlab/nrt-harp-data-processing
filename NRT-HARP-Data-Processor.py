"""
 * NRT-HARP-Data-Processor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2023 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""

import os
import sys
import time
import sched
import argparse
import traceback
from threading import Thread
from datetime import timedelta

import py_src.configuration.ConfigReader as conf
import py_src.databases.RESTFulDBAccessor as rest
from py_src.processing.DownloadProcessor import DownloadProcessor
from py_src.processing.ParamProcessor import ParamProcessor

this = sys.modules[__name__]

this.DOWNLOADER_THREAD = None
this.PROCESSOR_THREAD = None


def do_start(config_dir, config_file, sc: sched):
    """
    Function that is used to initialize all the objects that are going to be utilized by this process and schedule the
    various sub-processes that will be run at some cadence. If a failure occurs while initalizing this function will be
    scheduled to try again at some point in the future.

    :param config_dir: The directory where the configuration file is located
    :param config_file: The name of the configuration file
    :param sc: A scheduling object used to run the sub-processes
    :return: None
    """
    try:
        config_dir = os.path.abspath(config_dir)
        config = conf.ConfigReader(config_dir, config_file)
        logger = config.get_logger()

        accessor = rest.RESTFulDBAccessor(config.get_api_address(), config.get_api_user(), config.get_api_password(),
                                          config.get_logger())

        cadence_hours = config.get_cadence()
        download_processor = DownloadProcessor(accessor, timedelta(hours=cadence_hours),
                                               config.get_notification_email(), logger)

        calc_processor = ParamProcessor(accessor, logger, config.get_lib_dir(), config.get_lib_name(),
                                        config.get_batch_size())

        cadence_sec = int(cadence_hours * 60 * 60)

        sc.enter(3, 1, sched_downloading, (download_processor, logger, cadence_sec, sc,))
        sc.enter(6, 1, sched_processing, (calc_processor, logger, (cadence_sec / 4) + 1, sc,))

    except Exception as e:
        print('do_start Failed with: %s', str(e))
        print('do_start Traceback: %s', traceback.format_exc())
        sc.enter(60, 1, do_start, (config_dir, config_file, sc,))


def sched_downloading(download_processor, logger, cadence_sec: int, sc: sched):
    """
    Function for scheduling the download process execution every N seconds.  It will skip scheduling another run if the
    previous run has not completed yet.

    :param download_processor: The download processor object
    :param logger:  The logging object used to catch and log errors
    :param cadence_sec: The number of seconds before this function should be called again
    :param sc: The scheduler used to call this function again
    :return: None
    """
    try:
        if this.DOWNLOADER_THREAD is not None:
            if not this.DOWNLOADER_THREAD.is_alive():
                t = Thread(target=do_downloading, args=(download_processor, logger,))
                t.start()
                this.DOWNLOADER_THREAD = t
        else:
            t = Thread(target=do_downloading, args=(download_processor, logger,))
            t.start()
            this.DOWNLOADER_THREAD = t

    except Exception as e:
        logger.error('sched_downloading Failed with: %s', str(e))
        logger.debug('sched_downloading Traceback: %s', traceback.format_exc())
    sc.enter(cadence_sec, 2, sched_downloading, (download_processor, logger, cadence_sec, sc,))


def do_downloading(download_processor, logger):
    """
    Function that runs the download process and catches any errors that might occur.

    :param download_processor: The download processor object
    :param logger: The logging object used to catch and log errors
    :return: None
    """
    try:
        download_processor.run()
    except Exception as e:
        logger.error('do_downloading Failed with: %s', str(e))
        logger.debug('do_downloading Traceback: %s', traceback.format_exc())


def sched_processing(calc_processor, logger, cadence_sec: int, sc: sched):
    """
    Function for scheduling the parameter calculation process execution every N seconds. It will skip scheduling another
    run if the previous run has not completed yet.

    :param calc_processor: The parameter calculation processor
    :param logger: The logging object used to catch and log errors
    :param cadence_sec: The number of seconds before this function should be called again
    :param sc: the scheduler used to call this function again
    :return: None
    """
    try:
        if this.PROCESSOR_THREAD is not None:
            if not this.PROCESSOR_THREAD.is_alive():
                t = Thread(target=do_processing, args=(calc_processor, logger,))
                t.start()
                this.PROCESSOR_THREAD = t
        else:
            t = Thread(target=do_processing, args=(calc_processor, logger,))
            t.start()
            this.PROCESSOR_THREAD = t

    except Exception as e:
        logger.error('sched_processing Failed with: %s', str(e))
        logger.debug('sched_processing Traceback: %s', traceback.format_exc())
    sc.enter(cadence_sec, 1, sched_processing, (calc_processor, logger, cadence_sec, sc,))


def do_processing(calc_processor, logger):
    """
    Function that runs the parameter calculation process and catches any errors that might occur.

    :param calc_processor: The parameter calculation processor object
    :param logger: The logging object used to catch and log errors
    :return: None
    """
    try:
        calc_processor.run()
    except Exception as e:
        logger.error('do_processing Failed with: %s', str(e))
        logger.debug('do_processing Traceback: %s', traceback.format_exc())


def run_main(config_dir, config_file):
    sc = sched.scheduler(time.time, time.sleep)
    sc.enter(5, 1, do_start, (config_dir, config_file, sc,))
    sc.run()


example_text = """example:
    
       python3 NRT-HARP-Data-Processor.py -d config_dir
       python3 NRT-HARP-Data-Processor.py -d config_dir -f configuration_file.ini
       python3 NRT-HARP-Data-Processor.py -f configuration_file.ini
       """
parser = argparse.ArgumentParser(prog='NRT-HARP-Data-Processor', description='NRT HARP Data Parameter Processing.',
                                 epilog=example_text,
                                 formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('-d', '--directory', help='Location of the config file.', type=str,
                    default=os.path.join(os.getcwd(), 'config'))
parser.add_argument('-f', '--file', help='Name of the config file.', type=str, default='config.ini')
args = parser.parse_args()
run_main(args.directory, args.file)
