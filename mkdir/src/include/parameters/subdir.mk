# Set source directory for this subdirectory
SRC = $(PWD)/src/include/parameters

#List of files contributed by this directory
FILES = IParamCalculator.cpp IPIR_ParamCalculator.cpp

#Add the files to the source, object, and dependencies lists
OBJS += $(patsubst %.cpp,$(BUILDDIR)/%.o,$(FILES))
CPP_DEPS += $(patsubst %.cpp,$(BUILDDIR)/%.d,$(FILES))

# Each subdirectory must supply rules for building sources it contributes
$(BUILDDIR)/%.o: $(SRC)/%.cpp
	@echo 'Building object for file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	$(CXX) $(CFLAGS) -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '