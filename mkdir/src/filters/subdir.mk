# Set source directory for this subdirectory
SRC = $(PWD)/src/filters

#List of files contributed by this directory
FILES = AverageFilter.cpp AverageDownSampleFilter.cpp AverageCleaningFilter.cpp GaussianSmoothingFilter.cpp \
LanczosDownSampleFilter.cpp PIL_Filter.cpp PIL_Filter_W_Gauss.cpp

#Add the files to the source, object, and dependencies lists
OBJS += $(patsubst %.cpp,$(BUILDDIR)/%.o,$(FILES))
CPP_DEPS += $(patsubst %.cpp,$(BUILDDIR)/%.d,$(FILES))

# Each subdirectory must supply rules for building sources it contributes
$(BUILDDIR)/%.o: $(SRC)/%.cpp
	@echo 'Building object for file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	$(CXX) $(CFLAGS) -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '
