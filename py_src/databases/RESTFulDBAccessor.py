"""
 * NRT-HARP-Data-Processor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2022 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
import os
import json
import aiohttp
import aiofiles
import tempfile
import requests
import traceback
from enum import Enum

from typing import List, BinaryIO
from logging import Logger

import sunpy
import numpy as np
from sunpy.io.header import FileHeader
from sunpy.map import GenericMap
from sunpy.util import MetaDict

from datetime import datetime

from ..models.HARPParams import HARPParams
from ..models.HARPTimeStepRecord import HARPTimeStepRecord


class RESTFulDBAccessor:
    class FileType(Enum):
        magnetogram = 1
        conf_disambig = 2
        bitmap = 3

    def __init__(self, base_uri: str, user: str, password: str, logger: Logger):
        self._base_uri = base_uri
        self._unprocessed_uri = self._base_uri + 'harpdata/unprocessed/'
        self._img_download = self._base_uri + 'harpdata/harps/{0}/file/download/'
        self._harp_detail = self._base_uri + 'harpdata/harps/{0}/'
        self._img_upload = self._base_uri + 'harpdata/harps/{0}/file/upload/{1}'
        self._file_detail = self._base_uri + 'harpdata/files/{0}/'
        self._file_info = self._base_uri + 'harpdata/files/'
        self._params_uri = self._base_uri + 'harpdata/parameters/'
        self._token_uri = self._base_uri + 'generate-auth-token/'
        self._tmp_file = tempfile.TemporaryDirectory()
        self._logger = logger
        self._user = user
        self._pass = password
        self._session = None
        self._async_session = None

    def __del__(self):
        self._tmp_file.cleanup()

    def __get_session(self):
        if self._session is not None:
            return self._session

        try:
            self._session = requests.Session()
            self._session.auth = (self._user, self._pass)
            data = {"username": self._user, "password": self._pass}
            r = self._session.post(self._token_uri, json=data)
            if r.status_code == 200:
                token_json = json.loads(r.content)
                token = token_json['token']
                token_header = {'HTTP_AUTHORIZATION': 'Token ' + token}
                self._session.headers.update(token_header)
                return self._session
        except Exception as e:
            self._logger.error('RESTFulDBAccessor.get__session Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.get__session Traceback: %s', traceback.format_exc())
            self._session = None
        return None

    def __reset_session(self):
        try:
            if self._session is not None:
                self._session.close()
                self._session = None
        except Exception as e:
            self._logger.error('RESTFulDBAccessor.reset_session Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.reset_session Traceback: %s', traceback.format_exc())
            self._async_session = None

    async def __get_async_session(self):
        if self._async_session is not None:
            if self._async_session.closed is False and not self._async_session.loop.is_closed():
                return self._async_session
            else:
                self._async_session = None

        try:
            self._logger.info('RESTFulDBAccessor.get__async_session initializing session.')
            auth = aiohttp.BasicAuth(self._user, self._pass)
            conn = aiohttp.TCPConnector(limit=50, force_close=True)
            self._async_session = aiohttp.ClientSession(auth=auth, connector=conn)
            data = {"username": self._user, "password": self._pass}
            r = await self._async_session.post(self._token_uri, json=data)
            async with r:
                if r.status == 200:
                    token_json = await r.json()
                    token = token_json['token']
                    token_header = {'HTTP_AUTHORIZATION': 'Token ' + token}
                    self._async_session.headers.update(token_header)
                    return self._async_session
        except Exception as e:
            self._logger.error('RESTFulDBAccessor.get__async_session Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.get__async_session Traceback: %s', traceback.format_exc())
            self._async_session = None
        return None

    async def __reset_async_session(self):
        try:
            if self._async_session is not None:
                await self._async_session.close()
                self._async_session = None
        except Exception as e:
            self._logger.error('RESTFulDBAccessor.reset_async_session Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.reset_async_session Traceback: %s', traceback.format_exc())
            self._async_session = None

    def get_unprocessed(self) -> List[HARPTimeStepRecord]:

        try:
            session = self.__get_session()
            if session is None:
                return None

            r = session.get(self._unprocessed_uri)

            if r.status_code == 200:
                results = []
                for obj in r.json():
                    harp_num = int(obj['harpnumber'])
                    obs_start_str = obj['obsstart']
                    obs_start = datetime.strptime(obs_start_str, '%Y-%m-%dT%H:%M:%S')
                    conf = int(obj['confdownloaded'])
                    bit = int(obj['bitmapdownloaded'])
                    mag = int(obj['magnetogramdownloaded'])
                    harp_rec = HARPTimeStepRecord(harp_num, obs_start, bit, conf, mag)
                    results.append(harp_rec)
                return results
            else:
                self._logger.error('RESTFulDBAccessor.get_unprocessed returned unexpected status: %s',
                                   str(r.status_code))
                return None
        except requests.exceptions.RequestException as e:
            self.__reset_session()
            self._logger.error('RESTFulDBAccessor.get_unprocessed Failed because of request exception with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.get_unprocessed Traceback: %s', traceback.format_exc())
        except Exception as e:
            self.__reset_session()
            self._logger.error('RESTFulDBAccessor.get_unprocessed Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.get_unprocessed Traceback: %s', traceback.format_exc())
            return None

    def get_times_with_data(self, harp_num: int) -> List[datetime]:
        try:
            session = self.__get_session()
            if session is None:
                return None

            data_uri = self._harp_detail.format(harp_num)
            r = session.get(data_uri)
            if r.status_code == 200:
                results = []
                result_obj = r.json()
                file_list = result_obj['files']

                for f in file_list:
                    if f['bitmapdownloaded'] == 1 and f['confdownloaded'] == 1 and f['magnetogramdownloaded'] == 1:
                        obs_start_str = f['obsstart']
                        obs_start = datetime.strptime(obs_start_str, '%Y-%m-%dT%H:%M:%S')
                        results.append(obs_start)
                return results
            elif r.status_code == 404:
                return []
            else:
                self._logger.error('RESTFulDBAccessor.get_times_with_data returned unexpected status: %s',
                                   str(r.status_code))
                return None
        except requests.exceptions.RequestException as e:
            self.__reset_session()
            self._logger.error('RESTFulDBAccessor.get_times_with_data Failed because of request exception with: %s',
                               str(e))
            self._logger.debug('RESTFulDBAccessor.get_times_with_data Traceback: %s', traceback.format_exc())
        except Exception as e:
            self.__reset_session()
            self._logger.error('RESTFulDBAccessor.get_times_with_data Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.get_times_with_data Traceback: %s', traceback.format_exc())

    def get_times_with_bad_data(self, harp_num: int) -> List[datetime]:
        try:
            session = self.__get_session()
            if session is None:
                return None

            data_uri = self._harp_detail.format(harp_num)
            r = session.get(data_uri)
            if r.status_code == 200:
                results = []
                result_obj = r.json()
                file_list = result_obj['files']

                for f in file_list:
                    if f['bitmapdownloaded'] == -1 or f['confdownloaded'] == -1 or f['magnetogramdownloaded'] == -1:
                        obs_start_str = f['obsstart']
                        obs_start = datetime.strptime(obs_start_str, '%Y-%m-%dT%H:%M:%S')
                        results.append(obs_start)
                return results
            elif r.status_code == 404:
                return []
            else:
                self._logger.error('RESTFulDBAccessor.get_times_with_data returned unexpected status: %s',
                                   str(r.status_code))
                return None

        except Exception as e:
            self.__reset_session()
            self._logger.error('RESTFulDBAccessor.get_times_with_bad_data Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.get_times_with_bad_data Traceback: %s', traceback.format_exc())

    def save_bad_img_indicator(self, harp_num: int, obs_time: datetime, file_type: FileType) -> bool:

        try:
            session = self.__get_session()
            if session is None:
                return None

            qury_time = obs_time.strftime('%Y-%m-%dT%H:%M:%S')
            qury = ["query_time={}".format(qury_time), "harp_number={}".format(harp_num)]

            complete_url = "{}?{}".format(self._file_info, '&'.join(qury))
            r = session.get(complete_url)

            if r.status_code == 200:
                files = r.json()
                if len(files) > 0:
                    file = files[0]
                    if file_type == self.FileType.magnetogram:
                        file['magnetogramdownloaded'] = -1
                    elif file_type == self.FileType.conf_disambig:
                        file['confdownloaded'] = -1
                    else:
                        file['bitmapdownloaded'] = -1
                    post_url = self._file_detail.format(self._file_detail.format(file['id']))
                    r = session.put(post_url, json=file)
                else:
                    file = {'harpnumber': harp_num, 'obsstart': qury_time, 'bitmapdownloaded': 0, 'confdownloaded': 0,
                            'magnetogramdownloaded': 0}
                    post_url = self._file_info
                    if file_type == self.FileType.magnetogram:
                        file['magnetogramdownloaded'] = -1
                    elif file_type == self.FileType.conf_disambig:
                        file['confdownloaded'] = -1
                    else:
                        file['bitmapdownloaded'] = -1
                    r = session.post(post_url, json=file)

                if r.status_code == 200:
                    return True
                else:
                    self._logger.error('RESTFulDBAccessor.save_bad_img_indicator unexpected status for post: %s',
                                       str(r.status_code))
            else:
                self._logger.error('RESTFulDBAccessor.save_bad_img_indicator unexpected status for query: %s',
                                   str(r.status_code))

        except Exception as e:
            self.__reset_session()
            self._logger.error('RESTFulDBAccessor.save_bad_img_indicator Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.save_bad_img_indicator Traceback: %s', traceback.format_exc())

    async def save_bad_img_indicator_async(self, harp_num: int, obs_time: datetime, file_type: FileType) -> bool:

        try:
            session = await self.__get_async_session()
            if session is None:
                return None

            qury_time = obs_time.strftime('%Y-%m-%dT%H:%M:%S')
            qury = ["query_time={}".format(qury_time), "harp_number={}".format(harp_num)]

            complete_url = "{}?{}".format(self._file_info, '&'.join(qury))
            r = await session.get(complete_url)

            if r.status_code == 200:
                files = r.json()
                if len(files) > 0:
                    file = files[0]
                    if file_type == self.FileType.magnetogram:
                        file['magnetogramdownloaded'] = -1
                    elif file_type == self.FileType.conf_disambig:
                        file['confdownloaded'] = -1
                    else:
                        file['bitmapdownloaded'] = -1
                    post_url = self._file_detail.format(self._file_detail.format(file['id']))
                    r = await session.put(post_url, json=file)
                else:
                    file = {'harpnumber': harp_num, 'obsstart': qury_time, 'bitmapdownloaded': 0, 'confdownloaded': 0,
                            'magnetogramdownloaded': 0}
                    post_url = self._file_info
                    if file_type == self.FileType.magnetogram:
                        file['magnetogramdownloaded'] = -1
                    elif file_type == self.FileType.conf_disambig:
                        file['confdownloaded'] = -1
                    else:
                        file['bitmapdownloaded'] = -1
                    r = await session.post(post_url, json=file)

                if r.status_code == 200:
                    return True
                else:
                    self._logger.error('RESTFulDBAccessor.save_bad_img_indicator_async unexpected status for post: %s',
                                       str(r.status_code))
            else:
                self._logger.error('RESTFulDBAccessor.save_bad_img_indicator_async unexpected status for query: %s',
                                   str(r.status_code))

        except Exception as e:
            self.__reset_session()
            self._logger.error('RESTFulDBAccessor.save_bad_img_indicator_async Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.save_bad_img_indicator_async Traceback: %s', traceback.format_exc())

    def save_img_data(self, harp_num: int, file_name: str, data: BinaryIO) -> bool:
        try:
            session = self.__get_session()
            if session is None:
                return False

            upload_uri = self._img_upload.format(harp_num, file_name)
            files = {'name': (None, file_name), 'attachment': (file_name, data, 'application/octet-stream')}
            r = session.post(upload_uri, files=files)

            if r.status_code == 201:
                return True
            else:
                self._logger.error('RESTFulDBAccessor.save_img_data returned unexpected status: %s', str(r.status_code))
        except requests.exceptions.RequestException as e:
            self.__reset_session()
            self._logger.error('RESTFulDBAccessor.save_img_data Failed because of request exception with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.save_img_data Traceback: %s', traceback.format_exc())
        except Exception as e:
            self.__reset_session()
            self._logger.error('RESTFulDBAccessor.save_img_data Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.save_img_data Traceback: %s', traceback.format_exc())
        return False

    async def get_img_data_async(self, harp_num: int, obs_start: datetime, file_type: FileType) -> GenericMap:

        try:
            session = await self.__get_async_session()
            if session is None:
                return False

            dl_uri = self._img_download.format(harp_num)
            obsstart = obs_start.strftime('%Y-%m-%dT%H:%M:%S')
            if file_type == self.FileType.magnetogram:
                filetype_str = 'magnetogram'
            elif file_type == self.FileType.conf_disambig:
                filetype_str = 'conf_disambig'
            else:
                filetype_str = 'bitmap'

            args = {'obsstart': obsstart, 'filetype': filetype_str}
            filename = 'harp_{0}_{1}_{2}.fits'.format(str(harp_num), obsstart, filetype_str)
            filename = os.path.join(self._tmp_file.name, filename)

            async with session.get(dl_uri, params=args) as r:
                async with aiofiles.open(filename, 'wb') as fd:
                    async for chunk in r.content.iter_chunked(1024):
                        await fd.write(chunk)

            if os.path.isfile(filename) and os.path.getsize(filename) > 0:
                img_map = self.__get_map(filename)
                if img_map is None:
                    await self.save_bad_img_indicator_async(harp_num, obs_start, file_type)
            else:
                img_map = None
                await self.save_bad_img_indicator_async(harp_num, obs_start, file_type)
            os.remove(filename)
            return img_map
        except Exception as e:
            await self.__reset_async_session()
            self._logger.error('RESTFulDBAccessor.get_img_data_async Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.get_img_data_async Traceback: %s', traceback.format_exc())
            return None

    async def save_calc_values_async(self, calc_vals: HARPParams) -> bool:
        """

        :param calc_vals:
        :return:
        """
        try:
            session = await self.__get_async_session()
            if session is None:
                return False

            params = calc_vals.Params
            obsstart_str = calc_vals.ObsStart.strftime('%Y-%m-%dT%H:%M:%S')
            content_dict = {'harp_number': calc_vals.HarpNumber, 'obsstart': obsstart_str,
                            'usflux': params.at[0, 'USFLUX'],
                            'head_usflux': params.at[0, 'HEAD_USFLUX'],
                            'meangbz': params.at[0, 'MEANGBZ'],
                            'r_value': params.at[0, 'R_VALUE'],
                            'pil_len': params.at[0, 'PIL_LEN'],
                            'fdim': params.at[0, 'FDIM'],
                            'B_EFF': params.at[0, 'BEFF'],
                            'HEAD_MEANGAM': params.at[0, 'HEAD_MEANGAM'],
                            'HEAD_MEANGBT': params.at[0, 'HEAD_MEANGBT'],
                            'HEAD_MEANGBZ': params.at[0, 'HEAD_MEANGBZ'],
                            'HEAD_MEANGBH': params.at[0, 'HEAD_MEANGBH'],
                            'HEAD_MEANJZD': params.at[0, 'HEAD_MEANJZD'],
                            'HEAD_TOTUSJZ': params.at[0, 'HEAD_TOTUSJZ'],
                            'HEAD_MEANALP': params.at[0, 'HEAD_MEANALP'],
                            'HEAD_MEANJZH': params.at[0, 'HEAD_MEANJZH'],
                            'HEAD_TOTUSJH': params.at[0, 'HEAD_TOTUSJH'],
                            'HEAD_ABSNJZH': params.at[0, 'HEAD_ABSNJZH'],
                            'HEAD_SAVNCPP': params.at[0, 'HEAD_SAVNCPP'],
                            'HEAD_MEANPOT': params.at[0, 'HEAD_MEANPOT'],
                            'HEAD_TOTPOT': params.at[0, 'HEAD_TOTPOT'],
                            'HEAD_MEANSHR': params.at[0, 'HEAD_MEANSHR'],
                            'HEAD_SHRGT45': params.at[0, 'HEAD_SHRGT45'],
                            'HEAD_R_VALUE': params.at[0, 'HEAD_R_VALUE'],
                            'LAT_MIN': calc_vals.LAT_MIN, 'LON_MIN': calc_vals.LON_MIN,
                            'LAT_MAX': calc_vals.LAT_MAX, 'LON_MAX': calc_vals.LON_MAX,
                            'CRVAL1': calc_vals.CRVAL1, 'CRVAL2': calc_vals.CRVAL2,
                            'CRLN_OBS': calc_vals.CRLN_OBS, 'CRLT_OBS': calc_vals.CRLT_OBS,
                            'GWILL': calc_vals.GWILL, 'QUALITY': calc_vals.QUALITY}

            r = await session.post(self._params_uri, json=content_dict)
            if r.status == 201:
                return True
            else:
                self._logger.error('RESTFulDBAccessor.save_calc_values_async returned unexpected status: %3',
                                   str(r.status_code))
        except requests.exceptions.RequestException as e:
            await self.__reset_async_session()
            self._logger.error('RESTFulDBAccessor.save_calc_values_async Failed because of request exception with: %s',
                               str(e))
            self._logger.debug('RESTFulDBAccessor.save_calc_values_async Traceback: %s', traceback.format_exc())
        except Exception as e:
            await self.__reset_async_session()
            self._logger.error('RESTFulDBAccessor.save_calc_values_async Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.save_calc_values_async Traceback: %s', traceback.format_exc())
            return False

    def __get_map(self, file_name) -> GenericMap:

        map_data = None
        try:
            pairs = sunpy.io.read_file(file_name, filetype='fits', ignore_missing_simple=True)
            new_pairs = []
            for pair in pairs:
                file_data, file_meta = pair
                assert isinstance(file_meta, FileHeader)

                if len(np.shape(file_data)) > 1:
                    data = file_data
                    meta = MetaDict(file_meta)
                    new_pairs.append((data, meta))

                if len(new_pairs) > 0:
                    data, header = new_pairs[0]
                    meta = MetaDict(header)
                    map_data = GenericMap(data, meta)

        except Exception as e:
            self._logger.error('RESTFulDBAccessor.__getMap Failed with: %s for file %s', str(e), file_name)
            self._logger.debug('RESTFulDBAccessor.__getMap Traceback: %s', traceback.format_exc())

        return map_data
