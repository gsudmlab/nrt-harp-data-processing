"""
 * NRT-HARP-Data-Processor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2020 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
from datetime import datetime
from pandas import DataFrame
import numpy as np


class HARPParams():

    def __init__(self, harp_number: int, obs_start: datetime, params: DataFrame,
                 LAT_MIN: float, LON_MIN: float, LAT_MAX: float, LON_MAX: float, CRVAL1: float, CRVAL2: float,
                 CRLN_OBS: float, CRLT_OBS: float, GWILL: float, QUALITY: int):
        self.HarpNumber = harp_number
        self.ObsStart = obs_start
        self.Params = params
        self.LAT_MIN = float(LAT_MIN)
        self.LON_MIN = float(LON_MIN)
        self.LAT_MAX = float(LAT_MAX)
        self.LON_MAX = float(LON_MAX)
        self.CRVAL1 = float(CRVAL1)
        self.CRVAL2 = float(CRVAL2)
        self.CRLN_OBS = float(CRLN_OBS)
        self.CRLT_OBS = float(CRLT_OBS)
        self.GWILL = float(GWILL) if GWILL is not None else float(0)
        self.QUALITY = int(QUALITY)

    def __str__(self):
        var_dict = self.__dict__
        values = ['\t({0}: {1})\n'.format(k, str(var_dict[k])) for k in var_dict.keys()]
        out_str = 'HARPParams: \n' + ''.join(values)
        return out_str
