"""
 * NRT-HARP-Data-Processor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2020 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
from datetime import datetime


class HARPTracking():
    def __init__(self, local_tracker: int, harp_number: int, obs_start: datetime, bitmap_downloaded: bool,
                 conf_downloaded: bool, magnetogram_downloaded: bool):
        self.LocalTracker = local_tracker
        self.HarpNumber = harp_number
        self.ObsStart = obs_start
        self.BitmapDownloaded = bitmap_downloaded
        self.ConfDownloaded = conf_downloaded
        self.MagnetogramDownloaded = magnetogram_downloaded
