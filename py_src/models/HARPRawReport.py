"""
 * NRT-HARP-Data-Processor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2020 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""

import numpy as np
from sunpy.util import MetaDict


class HARPRawReport:

    def __init__(self, mag_data: np.ndarray, bit_data: np.ndarray, conf_data: np.ndarray, nx, ny, header: MetaDict):
        self.mag_data = mag_data
        self.bit_data = bit_data
        self.conf_data = conf_data
        self.header = header
        self.nx = nx
        self.ny = ny
