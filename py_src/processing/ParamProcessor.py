"""
 * NRT-HARP-Data-Processor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2020 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
import os
import asyncio
import traceback
from typing import List
import threading

import numpy
from pandas import DataFrame
from datetime import datetime
from logging import Logger

import numpy as np
import ctypes as ct

from py_src.models.HARPParams import HARPParams
from ..databases.RESTFulDBAccessor import RESTFulDBAccessor
from ..models.HARPRawReport import HARPRawReport
from ..models.HARPTimeStepRecord import HARPTimeStepRecord

NUM_SOLAR_FEATURES_PER_TS = 16
NON_COMPUTED_SOLAR_FEATURES = 2

import cProfile, pstats, io


class ParamProcessor:

    def __init__(self, accessor: RESTFulDBAccessor, logger: Logger, lib_dir: str, lib_name: str, batch_size: int):
        self._accessor = accessor
        self._logger = logger
        self._batch_size = batch_size
        dir_path = os.getcwd()
        self._lib_path = os.path.join(dir_path, lib_dir, lib_name)
        self._init_done = False
        self._async_lock = None
        self._thread_lock = threading.Lock()

    def __init_lib(self):
        if not self._init_done:
            lib = ct.cdll.LoadLibrary(self._lib_path)
            lib.init_lib(None)

            self._computeSolarFeatures = lib.computeSolarFeatures
            self._computeSolarFeatures.argtypes = [ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),
                                                   ct.POINTER(ct.c_double),
                                                   ct.c_double, ct.c_double, ct.c_double,
                                                   ct.c_double, ct.c_double, ct.c_double,
                                                   ct.c_double, ct.c_double, ct.c_double,
                                                   ct.c_uint, ct.c_uint, ct.POINTER(ct.c_double)]

            self._init_done = True

    def run(self):
        """
           Retrieves all the records of files in the database that have not yet been processed and then processes the
           files to produce the parameters. The parameters are then inserted into the database.

           :return: None
           """
        with self._thread_lock:
            self.__init_lib()
            asyncio.run(self.__run_async())

    async def __run_async(self):
        """
        Retrieves all the records of files in the database that have not yet been processed and then processes the
        files to produce the parameters. The parameters are then inserted into the database.

        :return: None
        """
        self._async_lock = asyncio.Lock()
        try:
            self._logger.info("ParamProcessor initiated at %s", datetime.now())
            try:
                records = self._accessor.get_unprocessed()
                #pr = cProfile.Profile()
                for chunk_num in range(0, int(len(records) / self._batch_size) + 1):
                    #pr.enable()
                    chunk_start = chunk_num * self._batch_size
                    self._logger.info("Processing batch starting at: %s", str(chunk_start))
                    futures = None
                    if (chunk_start + self._batch_size) < len(records):
                        futures = [self.__run_gen_async(records[x + chunk_start]) for x in range(0, self._batch_size)]
                    else:
                        futures = [self.__run_gen_async(records[x]) for x in range(chunk_start, len(records))]

                    await asyncio.gather(*futures)
                    #pr.disable()
                    #s = io.StringIO()
                    #sortby = 'cumulative'
                    #ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
                    #ps.print_stats()
                    #print(s.getvalue())
            except Exception as e:
                self._logger.error('ParamProcessor.run Failed to get records with: %s', str(e))
                self._logger.debug('ParamProcessor.run Traceback: %s', traceback.format_exc())

            time = datetime.now()
            self._logger.info("ParamProcessor completed at %s", time)
        finally:
            self._async_lock = None

    async def __run_gen_async(self, record: HARPTimeStepRecord):
        if record.ConfDownloaded and record.BitmapDownloaded and record.MagnetogramDownloaded:
            try:
                data = await self.__get_img_data_for_rec_async(record.HarpNumber, record.ObsStart)
                if data is not None:
                    async with self._async_lock:
                        harp_params = self.__get_params_obj(record.HarpNumber, record.ObsStart, data)
                    await self._accessor.save_calc_values_async(harp_params)
            except Exception as e:
                self._logger.error('ParamProcessor.run_gen_async Failed to process HARP %s record %s with: %s',
                                   str(record.HarpNumber), str(record.ObsStart), str(e))
                self._logger.debug('ParamProcessor.run_gen_async Traceback: %s', traceback.format_exc())

    @staticmethod
    def __clean_nan(val):
        try:
            val = float(val) if (val is not None) and (not np.isnan(float(val))) and np.isfinite(val) else None
            return val
        except:
            return None

    def __get_params_obj(self, harp_number: int, timestep: datetime, data: HARPRawReport) -> HARPParams:
        header = data.header
        cdelt1 = header['cdelt1']
        dsun_obs = header['dsun_obs']
        rsun_obs = header['rsun_obs']
        rsun_ref = header['rsun_ref']
        crval1 = header['crval1']
        crval2 = header['crval2']
        crln_obs = header['crln_obs']
        crpix1 = header['crpix1']
        crpix2 = header['crpix2']
        lat_min = header['lat_min']
        lon_min = header['lon_min']
        lat_max = header['lat_max']
        lon_max = header['lon_max']
        CRVAL1 = self.__clean_nan(header['CRVAL1'.lower()])
        CRVAL2 = self.__clean_nan(header['CRVAL2'.lower()])
        CRLN_OBS = self.__clean_nan(header['CRLN_OBS'.lower()])
        CRLT_OBS = self.__clean_nan(header['CRLT_OBS'.lower()])
        HEAD_USFLUX = self.__clean_nan(header['USFLUX'.lower()])
        HEAD_MEANGAM = self.__clean_nan(header['MEANGAM'.lower()])
        HEAD_MEANGBT = self.__clean_nan(header['MEANGBT'.lower()])
        HEAD_MEANGBZ = self.__clean_nan(header['MEANGBZ'.lower()])
        HEAD_MEANGBH = self.__clean_nan(header['MEANGBH'.lower()])
        HEAD_MEANJZD = self.__clean_nan(header['MEANJZD'.lower()])
        HEAD_TOTUSJZ = self.__clean_nan(header['TOTUSJZ'.lower()])
        HEAD_MEANALP = self.__clean_nan(header['MEANALP'.lower()])
        HEAD_MEANJZH = self.__clean_nan(header['MEANJZH'.lower()])
        HEAD_TOTUSJH = self.__clean_nan(header['TOTUSJH'.lower()])
        HEAD_ABSNJZH = self.__clean_nan(header['ABSNJZH'.lower()])
        HEAD_SAVNCPP = self.__clean_nan(header['SAVNCPP'.lower()])
        HEAD_MEANPOT = self.__clean_nan(header['MEANPOT'.lower()])
        HEAD_TOTPOT = self.__clean_nan(header['TOTPOT'.lower()])
        HEAD_MEANSHR = self.__clean_nan(header['MEANSHR'.lower()])
        HEAD_SHRGT45 = self.__clean_nan(header['SHRGT45'.lower()])
        HEAD_R_VALUE = self.__clean_nan(header['R_VALUE'.lower()])
        if header['GWILL'.lower()] == 'nan':
            GWILL = None
        else:
            GWILL = header['GWILL'.lower()]

        QUALITY = self.__clean_nan(header['QUALITY'.lower()])

        feats = self.__generate_solar_features(data.mag_data, data.bit_data, data.conf_data, cdelt1,
                                               crpix1, crpix2, crval1, crval2, rsun_ref, rsun_obs,
                                               dsun_obs, crln_obs, data.nx, data.ny)

        USFLUX = feats[0]
        # FLUXIMB = feats[1]
        MEANGBZ = feats[2]
        FDIM = feats[3]
        R_VALUE = feats[4]
        B_EFF = feats[5]
        # BEFF_MAX = feats[6]
        # sepLen = feats[7]
        # meanFlux = feats[8]
        # errorFlux = feats[9]
        PIL_LEN = feats[10]
        # PIR_USFLUX = feats[11]
        # PIR_FLUXIMB = feats[12]
        # PIR_MEANGBZ = feats[13]

        param_names = ['USFLUX', 'HEAD_USFLUX', 'MEANGBZ', 'R_VALUE', 'PIL_LEN', 'FDIM', 'BEFF',
                       'HEAD_MEANGAM', 'HEAD_MEANGBT', 'HEAD_MEANGBZ', 'HEAD_MEANGBH',
                       'HEAD_MEANJZD', 'HEAD_TOTUSJZ', 'HEAD_MEANALP', 'HEAD_MEANJZH',
                       'HEAD_TOTUSJH', 'HEAD_ABSNJZH', 'HEAD_SAVNCPP', 'HEAD_MEANPOT',
                       'HEAD_TOTPOT', 'HEAD_MEANSHR', 'HEAD_SHRGT45', 'HEAD_R_VALUE']

        params = [USFLUX, HEAD_USFLUX, MEANGBZ, R_VALUE, PIL_LEN, FDIM, B_EFF, HEAD_MEANGAM,
                  HEAD_MEANGBT, HEAD_MEANGBZ, HEAD_MEANGBH, HEAD_MEANJZD, HEAD_TOTUSJZ,
                  HEAD_MEANALP, HEAD_MEANJZH, HEAD_TOTUSJH, HEAD_ABSNJZH, HEAD_SAVNCPP,
                  HEAD_MEANPOT, HEAD_TOTPOT, HEAD_MEANSHR, HEAD_SHRGT45, HEAD_R_VALUE]
        out_df = DataFrame([params], columns=param_names)

        harp_params = HARPParams(harp_number, timestep, out_df, lat_min, lon_min, lat_max, lon_max, CRVAL1, CRVAL2,
                                 CRLN_OBS, CRLT_OBS, GWILL, QUALITY)
        return harp_params

    async def __get_img_data_for_rec_async(self, harp_number: int, timestep: datetime) -> HARPRawReport:

        mask = await self._accessor.get_img_data_async(harp_number, timestep, RESTFulDBAccessor.FileType.conf_disambig)
        if mask is not None:
            mask_array = np.asarray(mask.data, dtype=float).flatten()
        else:
            return None

        bitmap = await self._accessor.get_img_data_async(harp_number, timestep, RESTFulDBAccessor.FileType.bitmap)
        if bitmap is not None:
            bitmap_array = np.asarray(bitmap.data, dtype=float).flatten()
        else:
            return None

        magnetogram = await self._accessor.get_img_data_async(harp_number, timestep,
                                                              RESTFulDBAccessor.FileType.magnetogram)
        if magnetogram is not None:
            magnetogram_array = np.asarray(magnetogram.data, dtype=float).flatten()
        else:
            return None
        nx = magnetogram.data.shape[1]
        ny = magnetogram.data.shape[0]
        header = magnetogram.meta

        report = HARPRawReport(magnetogram_array, bitmap_array, mask_array, nx, ny, header)
        return report

    def __generate_solar_features(self, mag_arr, bit_arr, mask_arr, cdelt1, crpix1,
                                  crpix2, crval1, crval2, rsun_ref, rsun_obs, dsun_obs, crln_obs, nx,
                                  ny) -> numpy.ndarray:
        """
        Calls the parameter calculation library and returns the results from that library call.

        :param mag_arr: A flattened array that contains the magnetogram data
        :param bit_arr: A flattened array that contains the bitmap data
        :param mask_arr: A flattened array that contains the mask data
        :param cdelt1: The degrees per pixel
        :param crpix1: The central pixel location along the x
        :param crpix2: The central pixel location along the y
        :param crval1:
        :param crval2:
        :param rsun_ref:
        :param rsun_obs:
        :param dsun_obs:
        :param crln_obs:
        :param nx: The number of pixels on the x axis that the arrays contain
        :param ny: The number of pixels on the y axis that the arrays contain
        :return: Array with parameters in order [USFLUX, MEANGBZ, R_VALUE, PIL_LEN, FDIM]
        """
        out_solar_features = np.zeros((NUM_SOLAR_FEATURES_PER_TS - NON_COMPUTED_SOLAR_FEATURES), float)

        self._computeSolarFeatures(mask_arr.ctypes.data_as(ct.POINTER(ct.c_double)),
                                   bit_arr.ctypes.data_as(ct.POINTER(ct.c_double)),
                                   mag_arr.ctypes.data_as(ct.POINTER(ct.c_double)),
                                   cdelt1, crpix1, crpix2, crval1, crval2, rsun_ref,
                                   rsun_obs, dsun_obs, crln_obs, nx, ny,
                                   out_solar_features.ctypes.data_as(ct.POINTER(ct.c_double)))

        for i in range(NUM_SOLAR_FEATURES_PER_TS - NON_COMPUTED_SOLAR_FEATURES):
            out_solar_features[i] = self.__clean_nan(float(out_solar_features[i]))

        return out_solar_features
