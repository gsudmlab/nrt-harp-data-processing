"""
 * NRT-HARP-Data-Processor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2020 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
import os
import re
import drms
import glob
import time
import shutil
import socket
import tarfile
import tempfile
import requests
import traceback
import urllib.request
from typing import List, Dict
from datetime import timedelta, datetime
from logging import Logger

from ..databases.RESTFulDBAccessor import RESTFulDBAccessor


class DownloadProcessor:

    def __init__(self, accessor: RESTFulDBAccessor, runtime_cadence: timedelta, drms_client_email: str, logger: Logger):
        self._accessor = accessor
        self._runtime_cadence = runtime_cadence
        self._drms_client_name = drms_client_email
        self._logger = logger

        socket.setdefaulttimeout(60)  # Needed for DRMS and it's assumptions about timeout on the socket level.
        self._tmp_file = tempfile.TemporaryDirectory()

    def run(self):
        """
        Method that checks to see if data for all the harps active over the last 24 times the runtime cadence is present
        on disk.  If new data is present, it downloads and then inserts the information about the new data into the
        database that keeps track of what data is available and has been processed.

        :return: None, all information is logged and the calling routine assumes the process completed successfully
        """

        self._logger.info("DownloaderProcessor initiated at %s", datetime.now())
        try:
            start_time = datetime.now() - (self._runtime_cadence * 24)
            end_time = datetime.now()
            processed_harps = self.__download_records_for_range(start_time, end_time)
            self.__insert_downloaded(processed_harps)
        except Exception as e:
            self._logger.error('DownloaderProcessor.run Failed with: %s', str(e))
            self._logger.debug('DownloaderProcessor.run Traceback: %s', traceback.format_exc())

        self._logger.info("DownloaderProcessor completed at %s", datetime.now())

    def __download_records_for_range(self, start_time: datetime, end_time: datetime) -> List[int]:
        """
        Downloads harp data between the start and end time provided. If the data already exists in system storage, then
        nothing is done.

        :param start_time: The start time to begin downloading data from

        :param end_time: The end time to end downloading data from

        :return: A list of harp numbers that had data downloaded
        """
        online_files = self.__get_list_of_online(start_time, end_time)
        online_files = self.__filter_online(online_files)
        return self.__download_online_records(online_files)

    def __filter_online(self, harp_records) -> Dict:
        """
        Cleans the list of online records to remove those that have already been downloaded. It is expected that each
        HARP number in the dictionary has a list of online records to download into a directory that is named after
        the HARP number being processed.

        :param harp_records: Dictionary of HARP numbers to process with each entry being a list of file times to process

        :return: Cleaned dictionary of online records for each harp number
        """

        answer = {}
        for sNum, recs in harp_records.items():
            save_dir = os.path.join(self._tmp_file.name, str(sNum))

            if not os.path.exists(save_dir):
                answer[sNum] = recs
                self.__filter_from_restful_db(sNum, recs)
            else:
                self.__check_for_tar(save_dir)
                files = []
                files.extend(glob.glob(save_dir + "/*magnetogram.fits"))
                for file in files:
                    dots = list(self.__find_all(file, '.'))
                    if len(dots) > 2:
                        yr_pos = dots[2] + 1
                        yr = file[yr_pos:yr_pos + 4]
                        mo_pos = yr_pos + 4
                        mo = file[mo_pos:mo_pos + 2]
                        dy_pos = mo_pos + 2
                        dy = file[dy_pos:dy_pos + 2]
                        hr_pos = dy_pos + 3
                        hr = file[hr_pos:hr_pos + 2]
                        mi_pos = hr_pos + 2
                        mi = file[mi_pos:mi_pos + 2]
                        sec_pos = mi_pos + 2
                        sec = file[sec_pos:sec_pos + 2]
                        rec_time = "{0}.{1}.{2}_{3}:{4}:{5}_TAI".format(yr, mo, dy, hr, mi, sec)
                        if rec_time in recs:
                            recs.remove(rec_time)
                if len(recs) > 0:
                    answer[sNum] = recs

        return answer

    def __filter_from_restful_db(self, harpnum, recs):
        times = self._accessor.get_times_with_data(harpnum)
        if times is not None:
            for t in times:
                time_str = t.strftime('%Y.%m.%d_%H:%M:%S_TAI')
                if time_str in recs:
                    recs.remove(time_str)
        times = self._accessor.get_times_with_bad_data(harpnum)
        if times is not None:
            for t in times:
                time_str = t.strftime('%Y.%m.%d_%H:%M:%S_TAI')
                if time_str in recs:
                    recs.remove(time_str)
        return recs

    def __download_online_records(self, harp_records) -> List[int]:
        """
        Downloads the records for each HARP in the dictionary that is passed. It is expected that each HARP number
        in the dictionary has a list of online records to download into a directory that is named after the HARP
        number being processed.

        :param harp_records: Dictionary of HARP numbers to process with each entry being a list of file times to process
        :return: A list of HARP numbers that the download process successfully downloaded data for
        """
        processed_harp_nums = []
        part_size = 150
        for sNum, recs in harp_records.items():
            self._logger.info("SharpNum {0}, Records {1}".format(sNum, len(recs)))

            save_dir = os.path.join(self._tmp_file.name, str(sNum))
            has_complete = False
            attempt_count = 0
            if not os.path.exists(save_dir):
                os.makedirs(save_dir)

            while attempt_count < 5 and not has_complete:
                try:
                    self._logger.info('Total records found: %s - HARP: %s', len(recs), sNum)
                    if len(recs) > 0:
                        c = drms.Client()
                        num_partitions = len(recs) // part_size

                        for i in range(num_partitions + 1):
                            ts_tmp = ''
                            if i < num_partitions:
                                for j in range(0, part_size):
                                    ts_tmp += '{0}, '.format(recs[j + i * part_size])
                            else:
                                j = i * part_size
                                while j < len(recs):
                                    ts_tmp += '{0}, '.format(recs[j])
                                    j = j + 1

                            q = "hmi.sharp_cea_720s_nrt[%s][%s]" % (sNum, ts_tmp)

                            k = c.query(q, key='HARPNUM, T_REC')
                            self._logger.debug("Found %s records for query on HARP:%s", len(k), sNum)
                            max_req = 25000

                            self._logger.debug("Starting Download Harp:%s, Partition:%s", sNum, i)

                            r = c.export(
                                q + '{Bitmap,Conf_disambig,magnetogram}',
                                method='url-tar', protocol='fits', n=max_req, email=self._drms_client_name)

                            self._logger.debug('Starting download...')
                            time.sleep(10)

                            try_cnt = 0
                            done = False
                            while try_cnt < 10 and not done:
                                while (not r.has_finished()) and (not r.has_failed()):
                                    r.wait(sleep=30, timeout=120)

                                tarurl = r.urls['url'][0]
                                filename = r.urls['filename'][0]
                                save_loc = os.path.join(save_dir, filename)

                                try:
                                    # make request and set timeout for no data to 120 seconds and enable streaming
                                    with urllib.request.urlopen(tarurl, timeout=120) as resp:
                                        # Open output file and make sure we write in binary mode
                                        with open(save_loc + '.part', 'wb') as fh:
                                            # walk through the request response in chunks of 1MiB
                                            while True:
                                                chunk = resp.read(1024 * 1024)
                                                if not chunk:
                                                    break
                                                # Write the chunk to the file
                                                fh.write(chunk)

                                    # Rename the file
                                    os.rename(save_loc + '.part', save_loc)

                                    done = True
                                    self._logger.info("Data Chunk Downloaded - HARP:%s, Partition:%s", sNum, i)
                                    if self.__check_for_tar(save_dir):
                                        processed_harp_nums.append(sNum)
                                except Exception as e:
                                    self._logger.error('Attempt:%s. - Download Chunk Error: %s - HARP:%s',
                                                       attempt_count, str(e), sNum)
                                    self._logger.debug('Download Chunk Error Traceback: %s - HARP:%s',
                                                       traceback.format_exc(), sNum)
                                    time.sleep(10)
                                    try_cnt += 1
                        has_complete = True
                    else:
                        self._logger.info("No records found - HARP:%s", sNum)
                    has_complete = True
                except Exception as e:
                    self._logger.error('Attempt:%s - Download Chunk Error: %s - HARP:%s', attempt_count, str(e), sNum)
                    self._logger.debug('Download Chunk Error Traceback: %s - HARP:%s', traceback.format_exc(), sNum)
                    time.sleep(10)
                    attempt_count += 1
        return processed_harp_nums

    def __check_for_tar(self, save_dir) -> bool:
        """
        Method checks for and unzips tar files within a specified directory.  It will also remove some of the additional
        files that are sometimes included in the tar file as well as partial downloads and the tar file it self.

        :param save_dir: The directory to search for tar files to unzip

        :return: True if a tar file was found to unzip, false otherwise.
        """

        found_files = False
        files = []
        files.extend(glob.glob(save_dir + "/*.tar"))

        for fname in files:
            if fname.endswith("tar"):
                try:
                    tar = tarfile.open(fname, "r:")
                    tar.extractall(save_dir)
                    tar.close()
                    found_files = True
                except Exception as e:
                    self._logger.error('Check for Tar error: %s', str(e))
                    self._logger.debug('Traceback: %s', traceback.format_exc())
                os.remove(fname)

        types = ('*.txt', '*.html', '*.json', '*.drmsrun', '*.qsub', '*.tar.*')
        files = []
        for ftype in types:
            files.extend(glob.glob(save_dir + "/" + ftype))

        for f in files:
            os.remove(f)

        return found_files

    def __insert_downloaded(self, harp_nums: List[int]):
        """
        Inserts the records that were downloaded. It is expected that each
        HARP number in the dictionary has a list of records that were downloaded into a directory that is named after
        the HARP number being processed.

        :param harp_nums: List of HARP numbers to process

        :return: None
        """

        for harp_num in harp_nums:
            save_dir = os.path.join(self._tmp_file.name, str(harp_num))
            self._logger.info("Inserting downloaded data for HARP: %s", harp_num)

            if os.path.exists(save_dir):
                try:
                    files = [f for f in os.listdir(save_dir) if
                             os.path.isfile(os.path.join(save_dir, f))]
                    for file_name in files:
                        if "magnetogram.fits" in file_name or "bitmap.fits" in file_name or "conf_disambig.fits" in file_name:
                            file_path = os.path.join(save_dir, file_name)
                            if os.path.isfile(file_path) and os.path.getsize(file_path) > 0:
                                success = False
                                with open(file_path, 'rb') as fh:
                                    success = self._accessor.save_img_data(harp_num, file_name, fh)
                                    # Should add some retry if not successful.
                                if success:
                                    os.remove(file_path)
                            else:
                                if "magnetogram" in file_name:
                                    filetype = RESTFulDBAccessor.FileType.magnetogram
                                elif "bitmap" in file_name:
                                    filetype = RESTFulDBAccessor.FileType.bitmap
                                else:
                                    filetype = RESTFulDBAccessor.FileType.conf_disambig

                                filetime = self.__get_datetime_from_filename(file_name)
                                self._accessor.save_bad_img_indicator(harp_num, filetime, filetype)
                                os.remove(file_path)
                    shutil.rmtree(save_dir)
                except Exception as e:
                    self._logger.error('DownloaderProcessor.__insert_downloaded Failed with: %s', str(e))
                    self._logger.debug('DownloaderProcessor.__insert_downloaded Traceback: %s', traceback.format_exc())

            self._logger.info("Done inserting downloaded data for HARP: %s", harp_num)

    @staticmethod
    def __get_datetime_from_filename(self, filename: str) -> datetime:
        dots = list(self.__find_all(filename, '.'))
        if len(dots) > 2:
            yr_pos = dots[2] + 1
            yr = filename[yr_pos:yr_pos + 4]
            mo_pos = yr_pos + 4
            mo = filename[mo_pos:mo_pos + 2]
            dy_pos = mo_pos + 2
            dy = filename[dy_pos:dy_pos + 2]
            hr_pos = dy_pos + 3
            hr = filename[hr_pos:hr_pos + 2]
            mi_pos = hr_pos + 2
            mi = filename[mi_pos:mi_pos + 2]
            sec_pos = mi_pos + 2
            sec = filename[sec_pos:sec_pos + 2]
            time = datetime(int(yr), int(mo), int(dy), int(hr), int(mi), int(sec))
            return time
        return None

    @staticmethod
    def __find_all(a_str: str, sub: str):
        start = 0
        while True:
            start = a_str.find(sub, start)
            if start == -1: return
            yield start
            start += len(sub)

    def __get_list_of_online(self, start_time: datetime, end_time: datetime):
        """
        Gets a json object that contains a dictionary of all the online records for the NRT SHARP dataset. The
        dictionary contains an entry for each HARP that is found in the range, and each entry is a list of files
        available for that harp number in the given date range.

        :param start_time: The start time of the range
        :param end_time: The end time of the range
        :return: Dictionary with a list of online file times to download from JSOC for each HARP number found in range.
        """

        url = 'http://jsoc.stanford.edu/cgi-bin/ajax/jsoc_info'

        start_time_format = start_time.strftime("%Y.%m.%d_%H:%M:%S")
        end_time_format = end_time.strftime("%Y.%m.%d_%H:%M:%S")

        answer = {}

        ds_param = 'hmi.sharp_cea_720s_nrt[][{0}-{1}]'.format(start_time_format, end_time_format)

        params = dict(
            ds=ds_param,
            op='rs_list',
            key='*online*,*spec*',
            f=1
        )

        try_count = 0
        done = False
        while try_count < 5 and not done:
            try:
                resp = requests.get(url=url, params=params, timeout=60)
                if resp.status_code == 200:
                    data = resp.json()

                    for key, values in data.items():
                        if key == "recset":
                            for val in values:
                                if val["online"] == "Y":
                                    t_rec_pattern = "hmi.sharp_cea_720s_nrt\[(.*?)\]\[(.*?)\]"
                                    t_record = re.search(t_rec_pattern, val["spec"])
                                    if t_record.group(1) in answer:
                                        answer[t_record.group(1)].append(t_record.group(2))
                                    else:
                                        answer[t_record.group(1)] = [t_record.group(2)]

                    self._logger.info("Online Records JSON Downloaded - Time Range:%s-%s", start_time_format,
                                      end_time_format)
                    done = True
                else:
                    self._logger.critical("Online Records HTTP request Failed: %s - Time Range:%s-%s", resp.status_code,
                                          start_time_format, end_time_format)
                    done = False
                    try_count += 1

            except Exception as e:
                self._logger.critical("Online Records JSON Download Failed. Error:%s - Time Range:%s-%s", str(e),
                                      start_time_format, end_time_format)
                try_count += 1

        if done:
            return answer
        else:
            return None
