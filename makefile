RM := rm -rf
MKDIR_P = mkdir -p

BUILDDIR=$(PWD)/build

alib = libsepmvts4swa.so

makeDir = $(PWD)/mkdir
libDir = $(PWD)/lib

# Default target executed when no arguments are given to make.
default_target: all

.PHONY:clean default_target

all: fromDir = $(makeDir)
all:
	$(MAKE) -C $(makeDir) all
	$(MAKE) copy_lib

clean:
	$(MAKE) -C $(makeDir) clean
	-$(RM) $(libDir)

copy_lib:
	$(MKDIR_P) $(libDir)/bin
	cp $(BUILDDIR)/$(alib) $(libDir)/bin/$(alib)
	