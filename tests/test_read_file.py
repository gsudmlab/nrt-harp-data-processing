import unittest

import sunpy
import numpy as np
from sunpy.io.header import FileHeader
from sunpy.map import GenericMap
from sunpy.util import MetaDict


class TestFitsFileRead(unittest.TestCase):

    def test_file_read(self):
        file_name = "/data/sep-pred/sharps/7808/hmi.sharp_cea_720s_nrt.7808.20230126_194800_TAI.bitmap.fits"
        pairs = sunpy.io.read_file(file_name, filetype='fits', ignore_missing_simple=True)

        for pair in pairs:
            file_data, file_meta = pair
            print(file_meta)
            assert isinstance(file_meta, FileHeader)

        file_name2 = "/data/sep-pred/sharps/7808/hmi.sharp_cea_720s_nrt.7808.20230126_194800_TAI.conf_disambig.fits"
        pairs2 = sunpy.io.read_file(file_name2, filetype='fits', ignore_missing_simple=True)

        for pair in pairs2:
            file_data, file_meta = pair
            print(file_meta)
            assert isinstance(file_meta, FileHeader)

        file_name3 = "/data/sep-pred/sharps/7833/harp_7833_2023-01-27T13:00:00_TAI.magnetogram.fits"
        pairs3 = sunpy.io.read_file(file_name3, filetype='fits', ignore_missing_simple=True)

        for pair in pairs3:
            file_data, file_meta = pair
            print(file_meta)
            assert isinstance(file_meta, FileHeader)