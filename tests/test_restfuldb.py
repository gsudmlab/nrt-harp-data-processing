import unittest
from py_src.databases.RESTFulDBAccessor import RESTFulDBAccessor
from py_src.models.HARPTimeStepRecord import HARPTimeStepRecord
from . import *


class TestRestulDBAccessor(unittest.TestCase):

    def test_get_unprocessed(self):
        base_uri = RESTFULAPIURI
        accessor = RESTFulDBAccessor(base_uri, None)
        results = accessor.get_unprocessed()
        if results is not None and len(results) > 0:
            obj = results[0]
            self.assertTrue(type(obj) is HARPTimeStepRecord)
        else:
            self.assertTrue(False)

    def test_file_download(self):
        base_uri = RESTFULAPIURI
        accessor = RESTFulDBAccessor(base_uri, None)
        results = accessor.get_unprocessed()
        if results is not None and len(results) > 0:
            obj = results[0]
            harp_num = obj.HarpNumber
            obs_start = obj.ObsStart
            results = accessor.get_img_data(harp_num, obs_start, None)


    def test_harp_detail(self):
        base_uri = RESTFULAPIURI
        accessor = RESTFulDBAccessor(base_uri, None)
        harp_num = 6768
        results = accessor.get_times_with_data(harp_num)
