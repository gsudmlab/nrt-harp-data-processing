# NRT-HARP-Processor Test Build

If you wish to run this project independently on your local host for test purposes, you will need to setup several
things first.

1. The required libraries listed in the `requirements.txt` file, you should setup a conda environment with the required
   versions of the libraries by first:

       conda create -n processor
   Then using the `processor` environment by

       conda activate processor
   You can then install the needed libraries using:

       conda install -c conda-forge astropy~=5.0.1 numpy~=1.22.4 sunpy~=3.1.3 requests~=2.28.1 aiofiles~=22.1.0 aiohttp~=3.8.3


3. Before you can run this process, you must build a built in C++ library that is used for computation of parameters.
   You will need to ensure that GCC and Make are installed on your system before compiling using the following command:
   
       make all

4. You should now be able to run the project using:

       python3 NRT-HARP-Data-Processor.py

5. When done you should deactivate the environment:

       conda deactivate

***

## Runtime

1. To run this project, access to the RESTFul API that stores the data used by and reported by this process is
   required. In order to run the API see
   the [sep-predicition-restful-db](https://bitbucket.org/gsudmlab/sep-predicition-restful-db/) project for
   instructions on setup for and running the API.

***
***

In cases where doing integration testing, the RESTFul API has been started, and the test config is set to its location.

   python -m unittest discover

***

[Return to README](./README.md)

***
***

## Acknowledgment

This work was supported in part by NASA Grant Award No. NNH14ZDA001N, NASA/SRAG Direct Contract and two NSF Grant
Awards: No. AC1443061 and AC1931555.

***

This software is distributed using the [GNU General Public License, Version 3](./LICENSE.txt)

![GPLv3](./images/gplv3-88x31.png)

***

© 2023 Dustin Kempton, Berkay Aydin, Rafal Angryk

[Data Mining Lab](https://dmlab.cs.gsu.edu/)

[Georgia State University](https://www.gsu.edu/)